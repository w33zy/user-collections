#### TODOS
* create a shortcode to display how many collectors have a post in their collection or in a collection.
* show how many posts a user has in their collection in the WP admin users list.
* create a "collections" page on plugin activation
* create a "welcome" page to create and configure initial settings
* add a spinner to the "Register" button
* deleting an entire collection on the frontend should also update post meta counts
* Implement the pagination of collections and collection items via AJAX
* Deleting a user should delete all their collections and collection items

#### BUGS
* The `collector` role is not able to trigger async requests
* everytime a user sign's in from a new browser or incognito mode it creates a new collection

#### REFERENCES
* ##### PHPUnit examples
  * https://make.wordpress.org/core/handbook/testing/automated-testing/writing-phpunit-tests/
  * https://wpbrowser.wptestkit.dev/modules/wploader
  * PHPUNIT: https://phpunit.readtivestack.com/
  * WP_UnitTestCase: https://codex.wordpress.org/Unit_Tests_for_WordPress_Plugins
  * https://github.com/xwp/wp-customize-snapshots/tree/develop/tests/
  * https://github.com/WordPress/wordpress-develop/tree/trunk/tests/phpunit/tests/
  * https://github.com/eventespresso/event-espresso-core/tree/master/tests/testcases/core/libraries/rest_api/controllers
  * https://github.com/wp-graphql/wp-graphql/tree/develop/tests/
