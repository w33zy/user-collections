<?php

namespace User_Collections\Inc\Admin;

use User_Collections as NS;
use User_Collections\Inc\Common\Entities\Metadata;
use User_Collections\Inc\Common\Entities\Collection;
use User_Collections\Inc\Core\Options;

/**
 * The "collection" functionality of the plugin.
 *
 * @link       http://wzymedia.com
 * @since      1.0.0
 *
 * @author    wzy
 */
class Collections { // @TODO: This class may need breaking up

	use NS\Inc\Common\Traits\Utilities;

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since       1.0.0
	 * @param       string $plugin_name        The name of this plugin.
	 * @param       string $version            The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version     = $version;
	}

	/**
	 * Save submitted post to collection
	 *
	 * @since 1.0.0
	 *
	 * @return void
	 */
	public function save_post_to_collection() {

		$post_ID          = (int) $_POST['post_ID'];
		$current_user     = (int) $_POST['user_ID'];
		$sent_post        = isset( $_POST['current_post'] ) ? $_POST['current_post'] : null;
		$collection_ID    = isset( $_POST['collection_ID'] ) ? sanitize_text_field( $_POST['collection_ID'] ) : '';
		$collection_title = isset( $_POST['collection_title'] ) ? sanitize_text_field( $_POST['collection_title'] ) : '';

		check_ajax_referer( 'collection-list', 'security_nonce' );

		// If the user posting this is not the current user, reject this.
		if ( $current_user !== get_current_user_id() ) {

			wp_send_json_error( ['unauthorized_action' => 'Fail user authentication or unauthorized action'], 401 );
		}

		// We have a user that is not logged
		if ( 0 === $current_user && 0 === get_current_user_id() ) {

			do_action( 'user_collection_update', $post_ID, 0, 'anonymous' );

			$meta = get_post_meta( $post_ID, 'uc_post_meta', true ) ?: new Metadata();

			++ $meta->collection_count;

			$res = update_post_meta( $post_ID, 'uc_post_meta', $meta );

			if ( $res ) {
				wp_send_json_error( ['anonymous_user' => 'An anonymous user saved a post.'], 401 );
			}

			wp_send_json_error( ['item_unsaved' => 'Unable to save item ' . $post_ID . ' for anonymous user'], 500 );
		}

		$collection = get_user_meta( $current_user, 'uc_collection', true ) ?: new Collection();
		$keys       = [ 'postID', 'url', 'name', 'description', 'datePublished', 'dateModified', 'image', 'aggregateRating',
						'recipeCategory', 'recipeCuisine', 'dateCollected', 'itemReviewed', 'thumbnailUrl' ];

		if ( ! property_exists( $collection, $collection_ID ) ) {
			$collection->$collection_ID = [];
		}

		// Add item.
		if ( isset( $_POST['operation'] ) && 'add' === $_POST['operation'] ) {

			$new_item = null;

			if ( ! array_key_exists( $collection_ID, $collection->titles ) ) {
				$collection->titles[ $collection_ID ] = $collection_title;
			}

			// Build an array of values we will be storing.
			foreach ( $sent_post as $k => $v ) {
				if ( in_array( $k, $keys, true ) ) {
					$new_item[$k] = is_array( $v )
						? $this->array_walker( 'sanitize_text_field', $v )
						: ( 'postID' === $k  ? (int) $v : sanitize_text_field( $v ) );
				}
			}

			// Prevent saving to that same collection twice.
			foreach ( $collection->$collection_ID as $item ) {
				if ( $item['postID'] === $post_ID ) {
					wp_send_json_error( ['item_conflict' => 'Item ' . $post_ID . ' already saved to collection.'], 409 );
				}
			}

			$collection->$collection_ID[] = $new_item;

			$collection->count   = $this->get_collection_count( $collection );
			$collection->hash    = md5( json_encode( $collection ) );

			$result = update_user_meta( $current_user, 'uc_collection' , $collection );

			if ( ! $result ) {
				wp_send_json_error( ['item_unsaved' => 'Unable to save item ' . $post_ID . ' to collection'], 500 );
			}

			// do_action( 'user_collection_update', $post_ID, $current_user, 'add' );
			$this->update_post_meta_collection( $post_ID, $current_user, 'add' );

			wp_send_json_success(
				array(
					'data' => $new_item,
					'hash' => $collection->hash,
				)
			);
		}

		// Remove item.
		if ( isset( $_POST['operation'] ) && 'remove' === $_POST['operation'] ) {

			$items  = null;
			$result = null;

			$post_in_collection = array_search( $post_ID, array_column( $collection->$collection_ID, 'postID'), true );

			if ( false === $post_in_collection ) {
				wp_send_json_error( ['not_in_collection' => 'Item ' . $post_ID .' is not in collection'], 500 );
			}

			// Search through array and remove any item with a matching postID.
			$filtered_items = array_filter( $collection->$collection_ID, static function ( $item ) use ( $post_ID ) {
				return $item['postID'] !== $post_ID;
			});

			// We need to re-index the array after removing an item with `unset()`.
			foreach ( $filtered_items as $item ) {
				$items[] = $item;
			}

			$collection->$collection_ID = $items;

			// If collection array is empty, remove it from the object.
			if ( empty( $collection->$collection_ID ) ) { // TODO: I don't think this works.
				$collection->$collection_ID = [];
			}

			$collection->count = $this->get_collection_count( $collection );
			$collection->hash  = md5( json_encode( $collection ) );

			$result = update_user_meta( $current_user, 'uc_collection', $collection );

			if ( ! $result ) {
				wp_send_json_error( ['item_unremoved' => 'Unable to remove item ' . $post_ID .' removed from collection'], 500 );
			}

			// do_action( 'user_collection_update', $post_ID, $current_user, 'remove' );
			$this->update_post_meta_collection( $post_ID, $current_user, 'remove' );

			wp_send_json_success(
				array(
					'data' => array( 'item_removed' => 'Item ' . $post_ID . ' removed from collection' ),
					'hash' => $collection->hash,
				)
			);
		}

		wp_send_json_error( ['bad_request' => 'Cannot process the request due to a client error'], 400 );
	}

	/**
	 * Get all our saved collections
	 *
	 * @since 1.0.0
	 *
	 * @return void
	 */
	public function get_collection() {

		$result         = null;
		$user_ID        = isset( $_GET['user_ID'] ) ? (int) $_GET['user_ID'] : 0;
		$pub_collection = isset( $_GET['pub_collection'] ) ? wp_strip_all_tags( $_GET['pub_collection'] ) : '';
		$pub_collector  = isset( $_GET['pub_collector'] ) ? (int) $_GET['pub_collector'] : 0;

		check_ajax_referer( 'collection-list', 'security_nonce' );

		if ( $pub_collector && $pub_collection ) {
			$result = get_user_meta( $pub_collector, 'uc_collection', true );

			if ( $result && $result->ID === $pub_collection ) {
				wp_send_json_success( $result );
			}
		}

		// If we get a user ID and it does not match the current users' ID, then no access
		if ( $user_ID !== get_current_user_id() ) {
			wp_send_json_error( ['unauthorized_action' => 'Sorry, you do not have access to this collection'], 401 );
		}

		$result = get_user_meta( $user_ID, 'uc_collection', true );

		if ( ! $result ) {
			wp_send_json_error();
		}

		if ( $result ) {
			unset( $result->ID ); // We are keeping the ID property semi-private.
		}

		wp_send_json_success( $result );
	}

	/**
	 * Delete an entire collection
	 *
	 * @since 1.0.0
	 *
	 * @return void
	 */
	public function delete_collection() {

		$user_ID       = isset( $_GET['user_ID'] ) ? (int) $_GET['user_ID'] : 0;
		$collection_ID = isset( $_GET['collection_ID'] ) ? sanitize_key( $_GET['collection_ID'] ) : 'A';

		check_ajax_referer( 'collection-list', 'security_nonce' );

		// If the user posting this is not the current user, reject this.
		if ( $user_ID !== get_current_user_id() ) {
			wp_send_json_error( ['user_mismatch' => 'Unable to delete to collection'] );
		}

		$collection = get_user_meta( $user_ID, 'uc_collection', true );

		if ( $collection ) {
			unset( $collection->$collection_ID, $collection->titles[ $collection_ID ] );
		}

		$collection->count = $this->get_collection_count( $collection );
		$collection->hash  = md5( json_encode( $collection ) );

		$result = update_user_meta( $user_ID, 'uc_collection' , $collection );

		if ( ! $result ) {
			wp_send_json_error( ['collection_unremoved' => 'Unable to remove collection ' . $collection_ID .' removed from collection'] );
		}

		do_action( 'user_collection_delete', $collection_ID, $user_ID );

		wp_send_json_success(
			array(
				'data' => true,
				'hash' => $collection->hash
			)
		);
	}

	/**
	 * Get all our saved collections
	 *
	 * @since 1.0.0
	 *
	 * @var Collection $collection
	 *
	 * @return void
	 */
	public function create_collection() {

		$user_ID          = isset( $_POST['user_ID'] ) ? (int) $_POST['user_ID'] : 0;
		$collection_ID    = isset( $_POST['collection_ID'] ) ? wp_strip_all_tags( $_POST['collection_ID'] ) : '';
		$collection_title = isset( $_POST['collection_title'] ) ? wp_strip_all_tags( $_POST['collection_title'] ) : '';

		check_ajax_referer( 'collection-list', 'security_nonce' );

		// If the user posting this is not the current user, reject this.
		if ( $user_ID !== get_current_user_id() ) {
			wp_send_json_error( ['user_mismatch' => 'Unable to create to collection'], 500 );
		}

		$collection = get_user_meta( $user_ID, 'uc_collection', true ) ?: new Collection();

		if ( $collection ) {
			if ( ! array_key_exists( $collection_ID, $collection->titles ) ) {
				$collection->titles[ $collection_ID ] = $collection_title;
			}

			if ( ! property_exists( $collection, $collection_ID ) ) {
				$collection->$collection_ID = [];
			}
		}

		$collection->count = $this->get_collection_count( $collection );
		$collection->hash  = md5( json_encode( $collection ) );

		$result = update_user_meta( $user_ID, 'uc_collection' , $collection );

		if ( ! $result ) {
			wp_send_json_error( ['collection_not_created' => 'Unable to create collection ' . $collection_ID] );
		}

		do_action( 'user_collection_create', $collection_ID, $user_ID );

		wp_send_json_success(
			array(
				'data' => $collection->titles,
				'hash' => $collection->hash
			)
		);
	}

	/**
	 * Count the contents of all our collection members
	 *
	 * @param object $collection
	 *
	 * @return int
	 */
	private function get_collection_count( $collection ) {

		$count = 0;

		foreach ( $this->filter_collection( $collection ) as $subcollection ) {
			if ( is_array( $subcollection ) ) {
				$count += count( $subcollection );
			}
		}

		return $count;
	}

	/**
	 * Handles the AJAX functions related to the Svelte
	 * user signup form
	 *
	 * @since 1.0.0
	 *
	 * @uses \check_ajax_referer()
	 * @uses \register_new_user()
	 * @uses \wp_generate_password()
	 * @uses \wp_insert_user()
	 * @uses \wp_send_new_user_notifications()
	 * @uses \get_user_by()
	 * @uses \get_password_reset_key()
	 *
	 * @return void
	 */
	public function signup_form() {

		check_ajax_referer( 'signup-nonce', 'signup_nonce' );

		$username = isset( $_POST['username'] ) ? sanitize_user( $_POST['username'] ) : null;
		$email    = isset( $_POST['email'] ) ? sanitize_email( $_POST['email'] ) : null;
		$token    = isset( $_POST['token'] ) ? sanitize_text_field( $_POST['token'] ) : null;

		if ( ! $this->verify_recaptcha( $token ) ) {
			wp_send_json_error( [ 'signed_up' => false, 'error' => __( 'reCaptcha verification has failed.', 'user-collections' ) ], 403 );
		}

		if ( ! $username ) {
			wp_send_json_error( [ 'signed_up' => false, 'error' => __( 'The username field was empty or invalid.', 'user-collections' ) ], 403 );
		}

		if ( ! $email ) {
			wp_send_json_error( [ 'signed_up' => false, 'error' => __( 'The email field was empty or invalid.', 'user-collections' ) ], 403 );
		}

		if ( username_exists( $email ) || email_exists( $email ) ) {
			wp_send_json_error( ['signed_up' => false, 'error' => __( 'A account with those details already exists', 'user-collections' )], 403 );
		}

		// Generate the hashed password, the new subscriber still has to check email to create their own password.
		$password = wp_generate_password( 12, false );

		$user_data = array(
			'user_login'           => $username,
			'user_email'           => $email,
			'user_pass'            => $password,
			'role'                 => 'collector',
			'show_admin_bar_front' => 'false', // This has to be a string 'true' | 'false'
		);

		$user_id = wp_insert_user( $user_data );

		if ( is_wp_error( $user_id ) ) {
			wp_send_json_error( ['signed_up' => false, 'error' => __( 'We were unable to create an account, please try again.', 'user-collections' )], 403 );
		} else {
			// Send the default new user email to the admin only.
			wp_send_new_user_notifications( $user_id, 'admin' );

			// Create and send our own password create email to the user.
			$user       = get_user_by( 'id', $user_id );
			$user_login = $user->user_login;
			$user_email = $user->user_email;
			$site_name  = is_multisite() ? get_network()->site_name : wp_specialchars_decode( get_option( 'blogname' ), ENT_QUOTES );
			$key        = get_password_reset_key( $user );
			$url        = sprintf( '%1$s?intent=register&key=%2$s&login=%3$s', 'favorites', $key, rawurlencode( $user_login ) ); // TODO: Replace 'favorites' with admin selected setting

			if ( is_wp_error( $key ) ) {
				wp_send_json_error( ['signed_up' => false, 'error' => __( 'We were unable to create an account, please try again.', 'user-collections' )], 403 );
			}

			$message = __( 'Thank your for creating a new account.', 'user-collections' ) . "\r\n\r\n";
			/* translators: %s: Site name. */
			$message .= sprintf( __( 'Site Name: %s', 'user-collections' ), $site_name ) . "\r\n\r\n";
			/* translators: %s: User login. */
			$message .= sprintf( __( 'Username: %s', 'user-collections' ), $user_login ) . "\r\n\r\n";
			// $message .= __( 'If this was a mistake, just ignore this email and nothing will happen.', 'user-collections' ) . "\r\n\r\n";
			$message .= __( 'To create a password for your new account, please visit the following address:', 'user-collections' ) . "\r\n\r\n";
			$message .= '<' . network_site_url( $url, 'login' ) . ">\r\n";

			/* translators: Password reset notification email subject. %s: Site title. */
			$title = sprintf( __( '[%s] New Account', 'user-collections' ), $site_name );

			$email_sent = wp_mail( $user_email, wp_specialchars_decode( $title ), $message );

			if ( $email_sent ) {
				wp_send_json_success( ['signed_up' => true] );
			}

			wp_send_json_error( ['signed_up' => false, 'error' => __( 'We were unable to create an account, please try again.', 'user-collections' )], 403 );
		}

		wp_send_json_error( ['signed_up' => false, 'error' => __( 'We were unable to create an account, please try again.', 'user-collections' )], 403 );
	}

	/**
	 * Handles the AJAX functions related to the Svelte
	 * user login form
	 *
	 * @since 1.0.0
	 *
	 * @uses \check_ajax_referer()
	 * @uses \wp_signon()
	 *
	 * @return void
	 */
	public function login_form() {

		check_ajax_referer( 'login-nonce', 'login_nonce' );

		$data                  = [];
		$data['user_login']    = isset( $_POST['username'] ) ? sanitize_user( $_POST['username'] ) : null;
		$data['user_password'] = isset( $_POST['password'] ) ? sanitize_text_field( $_POST['password'] ) : null;
		$data['remember']      = isset( $_POST['remember'] ) && wp_validate_boolean( $_POST['remember'] );
		$token                 = isset( $_POST['token'] ) ? sanitize_text_field( $_POST['token'] ) : null;

		if ( ! $this->verify_recaptcha( $token ) ) {
			wp_send_json_error( [ 'logged_in' => false, 'error' => __( 'reCaptcha verification has failed.', 'user-collections' ) ], 403 );
		}

		if ( ! $data['user_login'] ) {
			wp_send_json_error( [ 'logged_in' => false, 'error' => __( 'The username field was empty.', 'user-collections' ) ], 403 );
		}

		if ( ! $data['user_password'] ) {
			wp_send_json_error( [ 'logged_in' => false, 'error' => __( 'The password field was empty.', 'user-collections' ) ], 403 );
		}

		$login = wp_signon( $data, is_ssl() );

		if ( is_wp_error( $login ) ) {
			wp_send_json_error( [ 'logged_in' => false, 'error' => __( 'Unknown username. Check again or try your email address.', 'user-collections' ) ], 403 );
		} else {
			wp_send_json_success( ['logged_in' => true] );
		}

		wp_send_json_error( [ 'logged_in' => false, 'error' => __( 'Unable to login, please try again later.', 'user-collections' ) ], 403 );
	}

	/**
	 * Handles the AJAX functions related to the Svelte
	 * forgot password form
	 *
	 * @since 1.0.0
	 *
	 * @uses \check_ajax_referer()
	 * @uses \get_password_reset_key()
	 * @uses \wp_mail()
	 *
	 * @return void
	 */
	public function forgot_form() {

		check_ajax_referer( 'forgot-nonce', 'forgot_nonce' );

		$user_login = isset( $_POST['user_login'] ) ? sanitize_email( $_POST['user_login'] ) : null;
		$token      = isset( $_POST['token'] ) ? sanitize_text_field( $_POST['token'] ) : null;
		$user_data  = null;

		if ( ! $this->verify_recaptcha( $token ) ) {
			wp_send_json_error( [ 'email_sent' => false, 'error' => __( 'reCaptcha verification has failed.', 'user-collections' ) ], 403 );
		}

		if ( ! $user_login ) {
			wp_send_json_error( [ 'email_sent' => false, 'error' => __( 'Invalid email address provided.', 'user-collections' ) ], 403 );
		}

		if ( strpos( $user_login, '@' ) ) {
			$user_data = get_user_by( 'email', $user_login );
		} else {
			$user_data = get_user_by( 'login', $user_login );
		}

		if ( $user_data ) {
			// Redefining user_login ensures we return the right case in the email.
			$user_login = $user_data->user_login;
			$user_email = $user_data->user_email;
			$faves_page = Options::get_option( 'collection_page', 'favorites' );
 			$site_name  = is_multisite() ? get_network()->site_name : wp_specialchars_decode( get_option( 'blogname' ), ENT_QUOTES );
			$key        = get_password_reset_key( $user_data );
			$url        = sprintf( '%1$s?intent=reset&key=%2$s&login=%3$s', $faves_page, $key, rawurlencode( $user_login ) );

			if ( is_wp_error( $key ) ) {
				wp_send_json_error( [ 'email_sent' => false, 'error' => __( 'We were unable to reset you password, please try again.', 'user-collections' ) ], 403 );
			}

			$message = __( 'Someone has requested a password reset for the following account:', 'user-collections' ) . "\r\n\r\n";
			/* translators: %s: Site name. */
			$message .= sprintf( __( 'Site Name: %s', 'user-collections' ), $site_name ) . "\r\n\r\n";
			/* translators: %s: User login. */
			$message .= sprintf( __( 'Username: %s', 'user-collections' ), $user_login ) . "\r\n\r\n";
			$message .= __( 'If this was a mistake, just ignore this email and nothing will happen.', 'user-collections' ) . "\r\n\r\n";
			$message .= __( 'To reset your password, visit the following address:', 'user-collections' ) . "\r\n\r\n";
			$message .= '<' . network_site_url( $url, 'login' ) . ">\r\n";

			/* translators: Password reset notification email subject. %s: Site title. */
			$title = sprintf( __( '[%s] Password Reset', 'user-collections' ), $site_name );

			$email_sent = wp_mail( $user_email, wp_specialchars_decode( $title ), $message );

			if ( $email_sent ) {
				wp_send_json_success( ['email_sent' => true] );
			}
		}

		wp_send_json_error( [ 'email_sent' => false, 'error' => __( 'We were unable to reset you password, please try again.', 'user-collections' ) ], 403 );
	}

	/**
	 * Handles the AJAX functions related to the
	 * password reset form
	 *
	 * @since 1.0.0
	 *
	 * @uses \check_ajax_referer()
	 * @uses \get_password_reset_key()
	 * @uses \wp_mail()
	 *
	 * @return void
	 */
	public function reset_form() {

		check_ajax_referer( 'reset-nonce', 'reset_nonce' );

		$user_pass_1 = isset( $_POST['user_pass_1'] ) ? sanitize_text_field( $_POST['user_pass_1'] ) : null;
		$user_pass_2 = isset( $_POST['user_pass_2'] ) ? sanitize_text_field( $_POST['user_pass_2'] ) : null;
		$user_login  = isset( $_POST['login'] ) ? sanitize_text_field( $_POST['login'] ) : null;
		$user_intent = isset( $_POST['intent'] ) ? sanitize_text_field( $_POST['intent'] ) : null;
		$reset_key   = isset( $_POST['key'] ) ? sanitize_text_field( $_POST['key'] ) : null;
		$token       = isset( $_POST['token'] ) ? sanitize_text_field( $_POST['token'] ) : null;

		if ( ! $this->verify_recaptcha( $token ) ) {
			wp_send_json_error( [ 'password_reset' => false, 'error' => __( 'reCaptcha verification has failed.', 'user-collections' ) ], 403 );
		}

		if ( ! $user_pass_1 || ! $user_pass_2 ) {
			wp_send_json_error( [ 'password_reset' => false, 'error' => __( 'A password field is empty.', 'user-collections' ) ], 400 );
		}

		if ( $user_pass_1 !== $user_pass_2 ) {
			wp_send_json_error( [ 'password_reset' => false, 'error' => __( 'Both passwords should be the same.', 'user-collections' ) ], 400 );
		}

		$user = check_password_reset_key( $reset_key, $user_login );

		if ( ! $user || is_wp_error( $user ) ) {
			if ( $user && 'expired_key' === $user->get_error_code() ) {
				wp_send_json_error( [ 'password_reset' => false, 'error' => __( 'The password reset key has expired, please request a new password.', 'user-collections' ) ], 403 );
			} else {
				wp_send_json_error( [ 'password_reset' => false, 'error' => __( 'The password reset key is invalid, please request a new password.', 'user-collections' ) ], 403 );
			}
		}

		// Parameter checks OK, reset password
		reset_password( $user, $user_pass_1 );

		wp_send_json_success( ['password_reset' => true], 200 );
	}

	/**
	 * Checks that the reCAPTCHA parameter sent with the registration
	 * request is valid.
	 *
	 * @since 1.0.0
	 *
	 * @uses \wp_remote_post()
	 *
	 * @param  null  $token
	 *
	 * @return bool True if the CAPTCHA is OK, otherwise false.
	 */
	private function verify_recaptcha( $token = null ) {

		// If reCaptcha is disabled, short-circuit this function by returning `true`.
		if ( ! Options::get_option( 'enable_recaptcha' ) ) {
			return true;
		}

		if ( $token ) {
			$captcha_response = $token;
		} else {
			return false;
		}

		// Verify the captcha response from Google.
		$response = wp_remote_post(
			'https://www.google.com/recaptcha/api/siteverify',
			array(
				'body' => array(
					'secret'   => Options::get_option( 'recaptcha_secret_key', '' ),
					'response' => $captcha_response
				)
			)
		);

		$success = false;

		if ( $response && is_array( $response ) ) {
			$decoded_response = json_decode( $response['body'], false );
			$success = $decoded_response->success;
		}

		return $success;
	}

	/**
	 * Update the post meta containing a list of user IDs
	 * that hve added the post to a collection
	 *
	 * @since 1.0.0
	 *
	 * @param int    $post_id  The post ID we are updating
	 * @param int    $user_id  The user ID
	 * @param string $task     The task we are performing
	 *
	 * @return bool|int
	 */
	public function update_post_meta_collection( $post_id, $user_id, $task ) {

		$meta = get_post_meta( $post_id, 'uc_post_meta', true ) ?: new Metadata();

		if ( ( 'add' === $task ) && ! in_array( $user_id, $meta->collected_by, true ) ) {
			$meta->collected_by[] = $user_id;
		}

		// If post is in at least 1 of the user's collections, don't remove their ID
		if ( 'remove' === $task && ! $this->post_in_user_collection( $post_id, $user_id ) && in_array( $user_id, $meta->collected_by, true ) ) {
			$meta->collected_by =array_values( // Re-index array
				array_filter( $meta->collected_by, static function ( $i ) use ( $user_id ) {
						return $i !== $user_id;
					}
				)
			);
		}

		if ( 'add' === $task ) {
			++ $meta->collection_count;
		}

		if ( ( 'remove' === $task ) && $meta->collection_count > 0 ) {
			-- $meta->collection_count;
		}

		return update_post_meta( $post_id, 'uc_post_meta', $meta );
	}

}
