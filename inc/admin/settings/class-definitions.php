<?php

namespace User_Collections\Inc\Admin\Settings;

use User_Collections as NS;

/**
 *
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    User_Collections
 * @subpackage User_Collections/includes
 */

/**
 * The Settings definition of the plugin.
 *
 *
 * @package    User_Collections
 * @subpackage User_Collections/includes
 * @author     wzy Media <email@example.com>
 */
class Definitions {

	use NS\Inc\Common\Traits\Utilities;

	/**
	 * [apply_tab_slug_filters description]
	 *
	 * @param  array $default_settings [description]
	 *
	 * @return array                   [description]
	 */
	private static function apply_tab_slug_filters( $default_settings ) {

		$extended_settings[] = array();
		$extended_tabs       = self::get_tabs();

		foreach ( $extended_tabs as $tab_slug => $tab_desc ) {

			$options                        = isset( $default_settings[ $tab_slug ] ) ? $default_settings[ $tab_slug ] : array();
			$extended_settings[ $tab_slug ] = apply_filters( 'user_collections_settings_' . $tab_slug, $options );
		}

		return $extended_settings;
	}

	/**
	 * [get_default_tab_slug description]
	 * @return string
	 */
	public static function get_default_tab_slug() {

		return key( self::get_tabs() );
	}

	/**
	 * Retrieve settings tabs
	 *
	 * @since    1.0.0
	 * @return    array    $tabs    Settings tabs
	 */
	public static function get_tabs() {

		$tabs                = array();
		$tabs['default_tab'] = __( 'General', 'user-collections' );
		$tabs['advanced_tab']  = __( 'Advanced', 'user-collections' );

		return apply_filters( 'user_collections_settings_tabs', $tabs );
	}

	/**
	 * 'Whitelisted' User_Collections settings, filters are provided for each settings
	 * section to allow extensions and other plugins to add their own settings
	 *
	 *
	 * @since    1.0.0
	 * @return    mixed    $value    Value saved / $default if key if not exist
	 */
	public static function get_settings() {

		$settings[] = array();

		$settings = array(
			'default_tab' => array(
				'default_tab_settings'       => array(
					'name' => '<strong>' . __( 'Header', 'user-collections' ) . '</strong>',
					'type' => 'header'
				),
				'primary_collection_name'              => array(
					'name' => __( 'Primary collection', 'user-collections' ),
					'desc' => __( 'The name of the default user collection.', 'user-collections' ),
					'std'  => __( 'Favorites', 'user-collections' ),
					'type' => 'text'
				),
				'anon_collection'                   => array(
					'name' => __( 'Anonymous collections', 'user-collections' ),
					'desc' => __( 'Allow logged out users to create collections', 'user-collections' ),
					'type' => 'checkbox'
				),
				'post_saved_message'              => array(
					'name' => __( 'Post saved message', 'user-collections' ),
					'desc' => __( 'The message to show when a post is saved to a collection', 'user-collections' ),
					'std'  => __( 'Post saved', 'user-collections' ),
					'type' => 'text'
				),
				'already_saved_message'              => array(
					'name' => __( 'Post already saved message', 'user-collections' ),
					'desc' => __( 'The message to show when a post is already saved to a collection', 'user-collections' ),
					'std'  => __( 'Post already saved', 'user-collections' ),
					'type' => 'text'
				),
				'collection_page'                     => array(
					'name'    => __( 'Collection page', 'user-collections' ),
					'desc'    => __( 'The page on this website to display user collections', 'user-collections' ),
					'options' => ( new self() )->list_wp_pages(),
					'type'    => 'select'
				),
				'collection_button_type'                      => array(
					'name'    => __( 'Button Type', 'user-collections' ),
					'desc'    => __( 'Which collection button type to use', 'user-collections' ),
					'options' => array(
						'regular-button'   => __( 'Regular Button', 'user-collections' ),
						'expanding-button'   => __( 'Expanding Button', 'user-collections' ),
						'heart-button'   => __( 'Heart Button', 'user-collections' ),
					),
					'type'    => 'radio'
				),
				/*'missing_callback'           => array(
					'name' => '<strong>' . __( 'Missing Callback', 'user-collections' ) . '</strong>',
					'type' => 'non-exist'
				),
				'checkbox'                   => array(
					'name' => __( 'Checkbox', 'user-collections' ),
					'desc' => __( 'Checkbox', 'user-collections' ),
					'type' => 'checkbox'
				),
				'multicheck'                 => array(
					'name'    => __( 'Multicheck', 'user-collections' ),
					'desc'    => __( 'Multicheck with 3 options', 'user-collections' ),
					'options' => array(
						'wp-human'   => __( "I read the <a href='https://wphuman.com/blog/'>WP Human Blog</a>", 'user-collections' ),
						'tang-rufus' => __( "<a href='http://tangrufus.com/'>Tang Rufus' Blog</a> looks great", 'user-collections' ),
						'Filter'     => __( 'You can apply filters on this option!', 'user-collections' )
					),
					'type'    => 'multicheck'
				),
				'multicheck_without_options' => array(
					'name' => __( 'Multicheck', 'user-collections' ),
					'desc' => __( 'Multicheck without options', 'user-collections' ),
					'type' => 'multicheck'
				),
				'radio'                      => array(
					'name'    => __( 'Radio', 'user-collections' ),
					'desc'    => __( 'Radio with 3 options', 'user-collections' ),
					'options' => array(
						'wp-human'   => __( "I read the <a href='https://wphuman.com/blog/'>WP Human Blog</a>", 'user-collections' ),
						'tang-rufus' => __( "<a href='http://tangrufus.com/'>Tang Rufus' Blog</a> looks great", 'user-collections' ),
						'Filter'     => __( 'You can apply filters on this option!', 'user-collections' )
					),
					'type'    => 'radio'
				),
				'radio_without_options'      => array(
					'name' => __( 'Radio', 'user-collections' ),
					'desc' => __( 'Radio without options', 'user-collections' ),
					'type' => 'radio'
				),
				'text'                       => array(
					'name' => __( 'Text', 'user-collections' ),
					'desc' => __( 'Text', 'user-collections' ),
					'type' => 'text'
				),
				'text_with_std'              => array(
					'name' => __( 'Text with std', 'user-collections' ),
					'desc' => __( 'Text with std', 'user-collections' ),
					'std'  => __( 'std will be saved!', 'user-collections' ),
					'type' => 'text'
				),
				'email'                      => array(
					'name' => __( 'Email', 'user-collections' ),
					'desc' => __( 'Email', 'user-collections' ),
					'type' => 'email'
				),
				'url'                        => array(
					'name' => __( 'URL', 'user-collections' ),
					'desc' => __( 'By default, only http & https are allowed', 'user-collections' ),
					'type' => 'url'
				),
				'password'                   => array(
					'name' => __( 'Password', 'user-collections' ),
					'desc' => __( 'Password', 'user-collections' ),
					'type' => 'password'
				),
				'number'                     => array(
					'name' => __( 'Number', 'user-collections' ),
					'desc' => __( 'Number', 'user-collections' ),
					'type' => 'number'
				),
				'number_with_attributes'     => array(
					'name' => __( 'Number', 'user-collections' ),
					'desc' => __( 'Max: 1000, Min: 20, Step: 30', 'user-collections' ),
					'max'  => 1000,
					'min'  => 20,
					'step' => 30,
					'type' => 'number'
				),
				'textarea'                   => array(
					'name' => __( 'Textarea', 'user-collections' ),
					'desc' => __( 'Textarea', 'user-collections' ),
					'type' => 'textarea'
				),
				'textarea_with_std'          => array(
					'name' => __( 'Textarea with std', 'user-collections' ),
					'desc' => __( 'Textarea with std', 'user-collections' ),
					'std'  => __( 'std will be saved!', 'user-collections' ),
					'type' => 'textarea'
				),
				'select'                     => array(
					'name'    => __( 'Select', 'user-collections' ),
					'desc'    => __( 'Select with 3 options', 'user-collections' ),
					'options' => array(
						'wp-human'   => __( "I read the <a href='https://wphuman.com/blog/'>WP Human Blog</a>", 'user-collections' ),
						'tang-rufus' => __( "<a href='http://tangrufus.com/'>Tang Rufus' Blog</a> looks great", 'user-collections' ),
						'Filter'     => __( 'You can apply filters on this option!', 'user-collections' )
					),
					'type'    => 'select'
				),
				'rich_editor'                => array(
					'name' => __( 'Rich Editor', 'user-collections' ),
					'desc' => __( 'Rich Editor save as HTML markups', 'user-collections' ),
					'type' => 'rich_editor'
				),*/
			),
			'advanced_tab'  => array(
				'enable_recaptcha'                   => array(
					'name' => __( 'Enable Google reCaptcha', 'user-collections' ),
					'desc' => __( 'Protect the signup forms from automated spam bots', 'user-collections' ),
					'type' => 'checkbox'
				),
				'recaptcha_site_key'              => array(
					'name' => __( 'reCaptcha Site Key', 'user-collections' ),
					'desc' => __( 'The name of the default user collection.', 'user-collections' ),
					'std'  => '',
					'type' => 'text'
				),
				'recaptcha_secret_key'              => array(
					'name' => __( 'reCaptcha Secret Key', 'user-collections' ),
					'desc' => __( 'The name of the default user collection.', 'user-collections' ),
					'std'  => '',
					'type' => 'text'
				),
			)
		);

		return self::apply_tab_slug_filters( $settings );
	}
}
