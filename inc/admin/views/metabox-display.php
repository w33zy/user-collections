<?php
/**
 * Provide a meta box view for the settings page
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    User_Collections
 * @subpackage User_Collections/admin/partials
 */

/**
 * Meta Box
 *
 * Renders a single meta box.
 *
 * @since       1.0.0
*/
?>

<form action="options.php" method="POST">
	<?php settings_fields( 'user_collections_settings' ); ?>
	<?php do_settings_sections( 'user_collections_settings_' . $active_tab ); ?>
	<?php submit_button(); ?>
</form>
<br class="clear" />
