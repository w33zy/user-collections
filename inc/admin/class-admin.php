<?php

namespace User_Collections\Inc\Admin;

use User_Collections\Inc\Admin\Settings\Definitions;
use User_Collections\Inc\Common\Entities\Collection;
use User_Collections\Inc\Core\Options;

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @author    Your Name or Your Company
 */
class Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * The text domain of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_text_domain    The text domain of this plugin.
	 */
	private $plugin_text_domain;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since       1.0.0
	 *
	 * @param       string $plugin_name        The name of this plugin.
	 * @param       string $version            The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name        = $plugin_name;
		$this->version            = $version;
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/user-collections-admin.css', array(), $this->version, 'all' );
	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
		/*
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/user-collections-admin.js', array( 'jquery' ), $this->version, false );
	}

	/**
	 * Register the administration menu for this plugin into the WordPress Dashboard menu.
	 *
	 * @since       1.0.0
	 *
	 * @return      void
	 */
	public function add_plugin_admin_menu() {

		add_menu_page(
			__( 'User Collections', 'user-collections' ),
			__( 'User Collections', 'user-collections' ),
			'manage_options',
			$this->plugin_name,
			array( $this, 'display_plugin_admin_page' )
		);

		add_submenu_page(
			'options-general.php',
			__( 'User Collections', 'user-collections' ),
			__( 'User Collections', 'user-collections' ),
			'manage_options',
			$this->plugin_name,
			array( $this, 'display_plugin_admin_page' )
		);

		remove_menu_page( $this->plugin_name );
	}

	/**
	 * Render the settings page for this plugin.
	 *
	 * @since    1.0.0
	 *
	 * @return void
	 */
	public function display_plugin_admin_page(): void {

		$tabs        = Definitions::get_tabs();
		$default_tab = Definitions::get_default_tab_slug();
		$active_tab  = isset( $_GET['tab'] ) && array_key_exists( $_GET['tab'], $tabs ) ? $_GET['tab'] : $default_tab;

		include_once __DIR__ . '/views/admin-display.php';
	}

	/**
	 * Add settings action link to the plugins page.
	 *
	 * @since 1.0.0
	 *
	 * @param  array  $links  The links below the entry on the plugin list field.
	 *
	 * @return array $links
	 */
	public function add_action_links( array $links ) {

		$links['settings'] = '<a href="' . admin_url( 'admin.php?page=' . $this->plugin_name ) . '">' . __( 'Settings', 'user-collections' ) . '</a>';

		return $links;
	}

	/**
	 * Redirects collector to collection page
	 *
	 * @since 1.0.0
	 *
	 * @param  string    $redirect_to
	 * @param  string    $request
	 * @param  \WP_User|\WP_Error
	 *  $user
	 *
	 * @return string|void
	 */
	public function collector_redirect( string $redirect_to, string $request, $user ) {

		global $user;

		if ( isset( $user->roles ) && is_array( $user->roles ) ) {
			if ( in_array( 'collector', $user->roles, true ) ) {
				return home_url( Options::get_option( 'collection_page' ) ); // TODO: Set this to an option.
			}

			return $redirect_to;
		}

		return $redirect_to;
	}

	/**
	 * Prevent subscribers from accessing WP admin area.
	 *
	 * @since 1.0.0
	 *
	 * @uses \wp_get_current_user()
	 * @uses \wp_safe_redirect()
	 *
	 * @return void
	 */
	public function restrict_admin_access() {

		if ( ! defined('DOING_AJAX') || ! DOING_AJAX ) {

			$user = wp_get_current_user();

			if ( $user && isset( $user->roles ) && is_array( $user->roles ) && in_array( 'collector', $user->roles, true ) ) {
				wp_safe_redirect( home_url( Options::get_option( 'collection_page' ) ) );
				exit;
			}
		}
	}

	public function collection_query( $qvars ) {

		$qvars[] = 'id';
		$qvars[] = 'collections';

		return $qvars;
	}

	/**
	 * Add a new 'Collections' column to the WP admin user's column
	 *
	 * @since 1.0.0
	 *
	 * @param array $column The array of the user column titles
	 *
	 * @return array
	 */
	public function new_modify_user_table( $column ) {

		$column['uc_collections'] = __( 'Collections', 'user-collections' );
		$column['uc_items'] = __( 'Items', 'user-collections' );

		return $column;
	}

	/**
	 * Adds content to the 'Collections' column to the WP admin user's column
	 *
	 * @since 1.0.0
	 *
	 * @param  string  $val          The value of the column entry
	 * @param  string  $column_name  The column name
	 * @param  int     $user_id      ID of the currently-listed user
	 *
	 * @return string
	 */
	public function new_modify_user_table_row( string $val, string $column_name, int $user_id ) {
		global $wpdb;

		$collection_count = $wpdb->get_var(
			$wpdb->prepare(
				"SELECT COUNT(*) FROM `{$wpdb->prefix}uc_collections` WHERE `{$wpdb->prefix}uc_collections`.`collection_user` = %d", $user_id
			)
		);

		$item_count = $wpdb->get_var(
			$wpdb->prepare(
				"SELECT COUNT(*) FROM `{$wpdb->prefix}uc_collections`, `{$wpdb->prefix}uc_items` WHERE `{$wpdb->prefix}uc_collections`.`ID` = `{$wpdb->prefix}uc_items`.`collection_id` AND `{$wpdb->prefix}uc_collections`.`collection_user` = %d", $user_id
			)
		);

		if ( 'uc_collections' === $column_name ) {
			return $collection_count;
		}

		if ( 'uc_items' === $column_name ) {
			return $item_count;
		}

		return $val;
	}

}
