<?php

namespace User_Collections\Inc\Rest;

use User_Collections as NS;
use User_Collections\Inc\Core\Options;


/**
 * The "collection" functionality of the plugin.
 *
 * @link       http://wzymedia.com
 * @since      1.0.0
 *
 * @author    wzy
 */
class Collections {

	/**
	 * Register the routes for the objects of the controller.
	 */
	public function register_routes(): void {

		$version = '1';
		$namespace = 'uc/v' . $version;
		$base = 'collections';

		register_rest_route(
			$namespace,
			'/' . $base,
			array(
				array(
					'methods'             => \WP_REST_Server::READABLE,
					'callback'            => array( $this, 'get_collections' ),
					'permission_callback' => array( $this, 'get_collections_permissions_check' ),
					'args'                => array(),
				),
				array(
					'methods'             => \WP_REST_Server::CREATABLE,
					'callback'            => array( $this, 'create_collection' ),
					'permission_callback' => array( $this, 'create_collection_permissions_check' ),
					'args'                => array(),
				),
				array(
					'methods'             => \WP_REST_Server::DELETABLE,
					'callback'            => array( $this, 'delete_collection' ),
					'permission_callback' => array( $this, 'delete_collection_permissions_check' ),
					'args'                => array(),
				),
			)
		);

		register_rest_route(
			$namespace,
			'/' . $base . '/(?P<collection>[a-zA-Z0-9-]+)',
			array(
				array(
					'methods'             => \WP_REST_Server::READABLE,
					'callback'            => array( $this, 'get_items' ),
					'permission_callback' => array( $this, 'get_collection_items_permissions_check' ),
					'args'                => array(),
				),
				array(
					'methods'             => \WP_REST_Server::CREATABLE,
					'callback'            => array( $this, 'add_item' ),
					'permission_callback' => array( $this, 'add_collection_item_permissions_check' ),
					'args'                => array(),
				),
//				array(
//					'methods'             => \WP_REST_Server::EDITABLE,
//					'callback'            => array( $this, 'update_item' ),
//					'permission_callback' => array( $this, 'add_item_permissions_check' ),
//					'args'                => array(),
//				),
				array(
					'methods'             => \WP_REST_Server::DELETABLE,
					'callback'            => array( $this, 'delete_item' ),
					'permission_callback' => array( $this, 'delete_collection_item_permissions_check' ),
					'args'                => array(),
				),
			)
		);

	}

	/**
	 * Get all collections and related items
	 * for current logged-in user
	 *
	 * @since 1.0.0
	 *
	 * @param  \WP_REST_Request  $request Full data about the request.
	 *
	 * @return \WP_Error|\WP_REST_Response
	 */
	public function get_collections( \WP_REST_Request $request ) {
		global $wpdb;

		$user_id = wp_get_current_user()->ID ?: 0;

		// if not logged in send user a generic collection
		if ( ! $user_id ) {

			return new \WP_REST_Response(
				array(
					'data' => array(
						'titles' => array(
							'uc-favorites' => __( 'Favorites', 'user-collections' ),
						),
						'counts' => array(
							'uc-favorites' => 0,
						),
						'uc-favorites' => array(),
					),
					'success' => true,
				),
				200
			);
		}

		// Search for all collections belonging to a user
		$collections = $wpdb->get_results(
			$wpdb->prepare(
				"SELECT * FROM `{$wpdb->prefix}uc_collections` WHERE `collection_user` = %d ORDER BY `{$wpdb->prefix}uc_collections`.`collection_updated` DESC",
				$user_id
			)
		);

		// If user is logged in and has no collections, create a default one
		if ( empty( $collections ) ) {
			$collection_title = Options::get_option( 'primary_collection_name', __( 'Favorites', 'user-collections' ) );
			$current_user     = wp_get_current_user()->ID ?: 0;
			$collection_key   = 'uc-' . strtolower( wp_generate_password( 12, false ) );
			$insert           = $wpdb->insert(
				$wpdb->prefix . 'uc_collections',
				array(
					'collection_key'     => $collection_key,
					'collection_user'    => $current_user,
					'collection_title'   => $collection_title,
					'collection_public'  => 1,
				),
				array(
					'%s',
					'%d',
					'%s',
					'%d',
				)
			);

			if ( false === $insert ) {
				return new \WP_Error( 'collection_not_created', __( 'Unable to create collection.', 'user-collections' ), array( 'status' => 404 ) );
			}

			// Use our the newly created default collection for the logged-in user
			$collections = $wpdb->get_results(
				$wpdb->prepare(
					"SELECT * FROM `{$wpdb->prefix}uc_collections` WHERE `collection_user` = %d ORDER BY `{$wpdb->prefix}uc_collections`.`collection_updated` DESC",
					$user_id
				)
			);
		}

		// Gather all the IDs of collections that the user owns, as a comma seperated string
		$ids = [];
		foreach( $collections as $collection ) {
			$ids[] = (int) $collection->ID;
		}
		$collection_ids = implode( ',', $ids );

		// Get all the items that belong to the user's collections
		$items = [];
		$all_items        = $wpdb->get_results(
			$wpdb->prepare(
				"SELECT * FROM `{$wpdb->prefix}uc_items` WHERE `collection_id` IN ({$collection_ids}) ORDER BY `{$wpdb->prefix}uc_items`.`item_collected` DESC",
				$user_id
			)
		);

		foreach( $all_items as $item ) {
			$items[ $item->collection_id ][] = $item;
		}

		// If user has collections, build the response
		$data = $this->prepare_collections_and_items_for_response( $collections, $items, $request );

		return new \WP_REST_Response(
			array(
				'data' => $data,
				'success' => true,
			),
			200
		);
	}

	/**
	 * Get one item from the collection
	 *
	 * @param  \WP_REST_Request  $request Full data about the request.
	 *
	 * @return \WP_Error|\WP_REST_Response
	 */
	public function create_collection( \WP_REST_Request $request ) {
		global $wpdb;

		$collection_title = $request->get_param( 'collection_title' );

		if ( empty( $collection_title ) ) {
			return new \WP_Error( 'collection_title_required', __( 'A collection title is required.', 'user-collections' ) );
		}

		if ( strlen( $collection_title ) < 3 || strlen( $collection_title ) > 255 ) {
			return new \WP_Error( 'collection_title_length', __( 'The collection title length is incorrect', 'user-collections' ) );
		}

		$current_user   = wp_get_current_user()->ID ?: 0;
		$collection_key = 'uc-' . strtolower( wp_generate_password( 12, false ) );
		$insert         = $wpdb->insert(
			$wpdb->prefix . 'uc_collections',
			array(
				'collection_key'     => $collection_key,
				'collection_user'    => $current_user,
				'collection_title'   => $collection_title,
				'collection_public'  => 1,
			),
			array(
				'%s',
				'%d',
				'%s',
				'%d',
			)
		);

		if ( false === $insert ) {
			return new \WP_Error( 'collection_not_created', __( 'Unable to create collection.', 'user-collections' ) );
		}

		return new \WP_REST_Response(
			array(
				'data' => array(
					'collection_title' => $collection_title,
					'collection_key'   => $collection_key,
				),
				'success' => true,
			),
			201
		);
	}

	/**
	 * Delete an entire collection
	 *
	 * @since 1.0.0
	 *
	 * @param  \WP_REST_Request  $request Full data about the request.
	 *
	 * @return \WP_Error|\WP_REST_Response
	 */
	public function delete_collection( \WP_REST_Request $request ) {
		global $wpdb;

		$collection_key  = $request->get_param( 'collection_key' );
		$collection_user = $request->get_param( 'collection_user' );

		if ( empty( $collection_key ) ) {
			return new \WP_Error( 'collection_ID_required', __( 'A valid collection key is required.', 'user-collections' ), array( 'status' => 400 ) );
		}

		// This `DELETE` action cascades to the items table
		$delete_collection = $wpdb->query(
			$wpdb->prepare(
				"DELETE FROM `{$wpdb->prefix}uc_collections` WHERE `collection_key` = %s AND `collection_user` = %d", $collection_key, $collection_user
			)
		);

		if ( 0 === $delete_collection || false === $delete_collection ) {
			return new \WP_Error( 'collection_not_exists', __( 'Collection does not exists.', 'user-collections' ), array( 'status' => 404 ) );
		}

		return new \WP_REST_Response(
			array(
				'data' => array(
					'collection_key'      => $collection_key,
					'collections_deleted' => $delete_collection,
				),
				'success' => true,
			),
			201
		);
	}

	/**
	 * Get one item from the collection
	 *
	 * @param  \WP_REST_Request  $request Full data about the request.
	 *
	 * @return \WP_Error|\WP_REST_Response
	 */
	public function get_items( \WP_REST_Request $request ) {
		global $wpdb;

		$current_user   = wp_get_current_user()->ID ?: 0;
		$collection_key = $request->get_param( 'collection' );
		$collection = $wpdb->get_results(
			$wpdb->prepare(
				"SELECT * FROM `{$wpdb->prefix}uc_collections` LEFT JOIN `{$wpdb->prefix}uc_items` ON (`{$wpdb->prefix}uc_collections`.`ID` = `{$wpdb->prefix}uc_items`.`collection_id`) WHERE `collection_key` = %s AND `collection_user` = %d",
				$collection_key,
				$current_user
			)
		);

		if ( empty( $collection ) ) {
			return new \WP_Error( 'collection_not_found', __( 'No collection found.', 'user-collections' ), array( 'status' => 404 ) );
		}

		$data = $this->prepare_collection_items_for_response( $collection, $request );

		return new \WP_REST_Response(
			array(
				'data' => $data,
				'success' => true,
			),
			200
		);
	}

	/**
	 * Add an item the collection
	 *
	 * @since 1.0.0
	 *
	 * @param  \WP_REST_Request  $request Full data about the request.
	 *
	 * @return \WP_Error|\WP_REST_Response
	 */
	public function add_item( \WP_REST_Request $request ) {
		global $wpdb;

		$current_user     = wp_get_current_user()->ID ?: 0;
		$collection_key   = $request->get_param( 'collection' );
		$item_id          = $request->get_param( 'item_id' ) ?: 0;
		$item_title       = $request->get_param( 'item_title' ) ?: '';
		$item_description = $request->get_param( 'item_description' ) ?: '';
		$item_url         = $request->get_param( 'item_url' ) ?: '';
		$item_category    = $request->get_param( 'item_category' ) ?: '';
		$item_author      = $request->get_param( 'item_author' ) ?: '';
		$item_thumbnail   = $request->get_param( 'item_thumbnail' ) ?: '';
		$item_published   = $request->get_param( 'item_published' ) ?: '';
		$item_modified    = $request->get_param( 'item_modified' ) ?: '';

		if ( empty( $collection_key ) ) {
			return new \WP_Error( 'collection_id_required', __( 'Collection ID is required.', 'user-collections' ), array( 'status' => 409 ) );
		}

		$collection = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM `{$wpdb->prefix}uc_collections` WHERE `collection_key` = %s", $collection_key ) );

		if ( empty( $collection ) ) {
			return new \WP_Error( 'collection_does_not_exist', __( 'That collection does not exists.', 'user-collections' ), array( 'status' => 409 ) );
		}

		if ( (int) $collection->collection_user !== $current_user ) {
			return new \WP_Error( 'collection_none_access', __( 'No access to collection.', 'user-collections' ), array( 'status' => 403 ) );
		}

		$previous_item = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM `{$wpdb->prefix}uc_items` WHERE `item_id` = %d AND `collection_id` = %d", $item_id, $collection->ID ) );

		if ( ! empty( $previous_item ) && ( (int) $previous_item->item_id === $item_id) ) {
			return new \WP_Error( 'collection_item_already_added', __( 'Item already added to collection.', 'user-collections' ), array( 'status' => 409 ) );
		}

		$item_key = strtolower( wp_generate_password( 15, false ) );
		$insert   = $wpdb->insert(
			$wpdb->prefix . 'uc_items',
			array(
				'item_id'          => $item_id,
				'item_title'       => $item_title,
				'item_description' => $item_description,
				'item_url'         => $item_url,
				'item_author'      => $item_author,
				'item_category'    => $item_category,
				'item_thumbnail'   => $item_thumbnail,
				'item_published'   => $item_published,
				'item_modified'    => $item_modified,
				'item_public'      => 1,
				'item_key'         => $item_key,
				'collection_id'    => $collection->ID,
			)
		);

		if ( false === $insert ) {
			return new \WP_Error( 'item_not_inserted', __( 'Unable to add item to collection.', 'user-collections' ), array( 'status' => 404 ) );
		}

		$wpdb->query(
			$wpdb->prepare(
				"UPDATE `{$wpdb->prefix}uc_collections` SET `{$wpdb->prefix}uc_collections`.`collection_count` = `{$wpdb->prefix}uc_collections`.`collection_count` + 1 WHERE `collection_key` = %s", $collection_key
			)
		);

		return new \WP_REST_Response(
			array(
				'data' => array(
					'item_key'       => $item_key,
					'collection_key' => $collection_key,
				),
				'success' => true,
			),
			200
		);
	}

	/**
	 * Delete an item from the collection
	 *
	 * @since 1.0.0
	 *
	 * @param  \WP_REST_Request  $request Full data about the request.
	 *
	 * @return \WP_Error|\WP_REST_Response
	 */
	public function delete_item( \WP_REST_Request $request ) {
		global $wpdb;

		$collection_key = $request->get_param( 'collection' );
		$item_id        = $request->get_param( 'item_id' );

		if ( empty( $item_id ) ) {
			return new \WP_Error( 'item_id_required', __( 'Item ID is required.', 'user-collections' ), array( 'status' => 409 ) );
		}

		$delete = $wpdb->delete(
			$wpdb->prefix . 'uc_items',
			array(
				'item_id'  => $item_id,
			),
			array(
				'%d',
			)
		);

		if ( false === $delete || 0 === $delete ) {
			return new \WP_Error( 'item_not_deleted', __( 'Unable to delete item from collection.', 'user-collections' ), array( 'status' => 404 ) );
		}

		$wpdb->query(
			$wpdb->prepare(
				"UPDATE `{$wpdb->prefix}uc_collections` SET `{$wpdb->prefix}uc_collections`.`collection_count` = `{$wpdb->prefix}uc_collections`.`collection_count` - 1 WHERE `collection_key` = %s", $collection_key
			)
		);

		return new \WP_REST_Response(
			array(
				'data' => array(
					'collection_key' => $collection_key,
					'item_id'        => $item_id,
				),
				'success' => true,
			),
			200
		);
	}


	/**
	 * Check if a given request has access to view a collection
	 *
	 * @since 1.0.0
	 *
	 * @param \WP_REST_Request $request Full data about the request.
	 * @return \WP_Error|bool
	 */
	public function get_collections_permissions_check( \WP_REST_Request $request ): bool {
		$current_user = wp_get_current_user();

		return $current_user
		       && ( $current_user->has_cap( 'collector' ) || $current_user->has_cap( 'manage_options' ) );
	}

	/**
	 * Check if a given request has access to create collections
	 *
	 * @since 1.0.0
	 *
	 * @param \WP_REST_Request $request Full data about the request.
	 * @return \WP_Error|bool
	 */
	public function create_collection_permissions_check( \WP_REST_Request $request ): bool {
		$current_user = wp_get_current_user();

		return $current_user
		       && ( $current_user->has_cap( 'create_collection' ) || $current_user->has_cap( 'manage_options' ) );
	}

	/**
	 * Check if a given request has access to create collections
	 *
	 * @since 1.0.0
	 *
	 * @param \WP_REST_Request $request Full data about the request.
	 * @return \WP_Error|bool
	 */
	public function delete_collection_permissions_check( \WP_REST_Request $request ): bool {
		$current_user    = wp_get_current_user();
		$collection_user = (int) $request->get_param( 'collection_user' );

		return $current_user
		       && ( $current_user->has_cap( 'delete_collection' ) || $current_user->has_cap( 'manage_options' ) )
		       && ( $current_user->ID === $collection_user );
	}

	/**
	 * Check if a given request has access to get a specific collections
	 *
	 * @since 1.0.0
	 *
	 * @param  \WP_REST_Request  $request
	 *
	 * @return bool
	 */
	public function get_collection_items_permissions_check( \WP_REST_Request $request): bool {
		$current_user = wp_get_current_user();

		return $current_user
		       && ( $current_user->has_cap( 'collector' ) || $current_user->has_cap( 'manage_options' ) );
	}

	/**
	 * Check if a given request has access to create collections
	 *
	 * @since 1.0.0
	 *
	 * @param \WP_REST_Request $request Full data about the request.
	 *
	 * @return bool
	 */
	public function add_collection_item_permissions_check( \WP_REST_Request $request ): bool {
		$current_user = wp_get_current_user();
		$collection_user = (int) $request->get_param( 'collection_user' );

		return $current_user
		       && ( $current_user->has_cap( 'create_item' ) || $current_user->has_cap( 'manage_options' ) )
		       && ( $current_user->ID === $collection_user );
	}

	/**
	 * Delete an item from a collection
	 *
	 * @since 1.0.0
	 *
	 * @param \WP_REST_Request $request Full data about the request.
	 *
	 * @return bool
	 */
	public function delete_collection_item_permissions_check( \WP_REST_Request $request ): bool {
		$current_user    = wp_get_current_user();
		$collection_user = (int) $request->get_param( 'collection_user' );

		return $current_user
		       && ( $current_user->has_cap( 'delete_item' ) || $current_user->has_cap( 'manage_options' ) )
		       && ( $current_user->ID === $collection_user );
	}

	/**
	 * @param  array             $collection
	 * @param  \WP_REST_Request  $request
	 *
	 * @return array
	 */
	private function prepare_collection_items_for_response( array $collection, \WP_REST_Request $request ): array {
		$data = array();

		foreach ( $collection as $item ) {
			$data['collection_count'] = (int) $item->collection_count;
			$data[ $item->collection_title ][] = array(
				'collection_key'   => $item->collection_key,
				'item_id'          => (int) $item->item_id,
				'item_key'         => $item->item_key,
				'item_title'       => $item->item_title,
				'item_url'         => $item->item_url,
				'item_thumbnail'   => $item->item_thumbnail,
				'item_published'   => $item->item_published,
				'item_modified'    => $item->item_modified,
				'item_description' => $item->item_description,
				'item_public'      => (bool) $item->item_public,
				'item_collected'   => $item->item_collected,
			);
		}

		return $data;
	}

	/**
	 * Organize the collection data for the response
	 *
	 * @since 1.0.0
	 *
	 * @param  array             $collections
	 * @param                    $items
	 * @param  \WP_REST_Request  $request
	 *
	 * @return array
	 */
	private function prepare_collections_and_items_for_response( array $collections, $items, \WP_REST_Request $request ): array {
		$data = array();

		foreach ( $collections as $collection ) {
			$data['titles'][ $collection->collection_key ] =  $collection->collection_title;
			$data['counts'][ $collection->collection_key ] = (int) $collection->collection_count;

			if ( empty( $items[ $collection->ID ] ) ) {
				$data[ $collection->collection_key ] = [];
				continue;
			}

			foreach ( $items[ $collection->ID ] as $item ) {
				if ( isset( $item->item_id ) ) {
					$data[ $collection->collection_key ][] = array(
						'item_key'         => $item->item_key,
						'item_id'          => (int) $item->item_id,
						'item_author'      => $item->item_author,
						'item_title'       => $item->item_title,
						'item_description' => $item->item_description,
						'item_url'         => $item->item_url,
						'item_thumbnail'   => $item->item_thumbnail,
						'item_published'   => $item->item_published,
						'item_modified'    => $item->item_modified,
						'item_public'      => (bool) $item->item_public,
						'item_collected'   => $item->item_collected,
					);
				} else {
					$data[ $collection->collection_key ] = [];
				}
			}

		}

		return $data;
	}

}