<?php

namespace User_Collections\Inc\Rest;

use User_Collections as NS;
use User_Collections\Inc\Core\Options;

/**
 * Handles user authentication via the REST API
 *
 * @link       http://wzymedia.com
 * @since      1.0.0
 *
 * @author    wzy
 */
class Authentication {

	/**
	 * Register the routes for the objects of the controller.
	 */
	public function register_routes(): void {

		$version = '1';
		$namespace = 'uc/v' . $version;
		$base = 'authentication';

		register_rest_route(
			$namespace,
			'/' . $base . '/register',
			array(
				array(
					'methods'             => \WP_REST_Server::CREATABLE,
					'callback'            => array( $this, 'register_user' ),
					'permission_callback' => array( $this, 'login_permissions_check' ),
					'args'                => array(),
				),
			)
		);

		register_rest_route(
			$namespace,
			'/' . $base . '/login',
			array(
				array(
					'methods'             => \WP_REST_Server::CREATABLE,
					'callback'            => array( $this, 'login_user' ),
					'permission_callback' => array( $this, 'login_permissions_check' ),
					'args'                => array(),
				),
			)
		);

		register_rest_route(
			$namespace,
			'/' . $base . '/forgot',
			array(
				array(
					'methods'             => \WP_REST_Server::CREATABLE,
					'callback'            => array( $this, 'forgot_user' ),
					'permission_callback' => array( $this, 'login_permissions_check' ),
					'args'                => array(),
				),
			)
		);

		register_rest_route(
			$namespace,
			'/' . $base . '/reset',
			array(
				array(
					'methods'             => \WP_REST_Server::CREATABLE,
					'callback'            => array( $this, 'reset_user' ),
					'permission_callback' => array( $this, 'login_permissions_check' ),
					'args'                => array(),
				),
			)
		);

		register_rest_route(
			$namespace,
			'/' . $base . '/logout',
			array(
				array(
					'methods'             => \WP_REST_Server::DELETABLE,
					'callback'            => array( $this, 'logout_user' ),
					'permission_callback' => array( $this, 'login_permissions_check' ),
					'args'                => array(),
				),
			)
		);

	}

	/**
	 * Register a new user.
	 *
	 * @since 1.0.0
	 *
	 * @uses \wp_generate_password()
	 * @uses \wp_insert_user()
	 * @uses \wp_send_new_user_notifications()
	 * @uses \get_user_by()
	 * @uses \get_password_reset_key()
	 *
	 * @param  \WP_REST_Request  $request Full data about the request.
	 *
	 * @return \WP_Error|\WP_REST_Response
	 */
	public function register_user( \WP_REST_Request $request ) {

		$user_login = $request->get_param( 'user_login' );
		$user_email = $request->get_param( 'user_email' );
		$token      = $request->get_param( 'token' );

		if ( ! $this->verify_recaptcha( $token ) ) {
			return new \WP_Error( 'captcha_error', __( 'reCaptcha verification has failed.' ) );
		}

		if ( ! is_email( $user_email ) ) {
			return new \WP_Error( 'email_error', __( 'Missing email address or invalid email format' ) );
		}

		// Generate the hashed password, the new subscriber still has to check email to create their own password.
		$password = wp_generate_password( 12, false );

		$user_data = array(
			'user_login'           => $user_login,
			'user_email'           => $user_email,
			'user_pass'            => $password,
			'role'                 => 'collector', // Set the new user's role as a `collector`
			'show_admin_bar_front' => 'false',     // This has to be a string 'true' | 'false'
		);

		$user_id = wp_insert_user( $user_data );

		if ( is_wp_error( $user_id ) ) {
			return $user_id;
		}

		// Send the default new user email to the admin only.
		wp_send_new_user_notifications( $user_id, 'admin' );

		// Create and send our own password create email to the user.
		$user       = get_user_by( 'id', $user_id );
		$user_login = $user->user_login;
		$user_email = $user->user_email;
		$site_name  = is_multisite() ? get_network()->site_name : wp_specialchars_decode( get_option( 'blogname' ), ENT_QUOTES );
		$key        = get_password_reset_key( $user );
		$url        = network_site_url( sprintf( '%1$s?intent=register&key=%2$s&login=%3$s', Options::get_option( 'collection_page' ), $key, rawurlencode( $user_login ) ) );

		if ( is_wp_error( $key ) ) {
			return $key;
		}

		$message = __( 'Thank your for creating a new account.', 'user-collections' ) . "\r\n\r\n";
		/* translators: %s: Site name. */
		$message .= sprintf( __( 'Site Name: %s', 'user-collections' ), $site_name ) . "\r\n\r\n";
		/* translators: %s: User login. */
		$message .= sprintf( __( 'Username: %s', 'user-collections' ), $user_login ) . "\r\n\r\n";
		// $message .= __( 'If this was a mistake, just ignore this email and nothing will happen.', 'user-collections' ) . "\r\n\r\n";
		$message .= __( 'To create a password for your new account, please visit the following address:', 'user-collections' ) . "\r\n\r\n";
		$message .= '<' . $url . ">\r\n";

		/* translators: Password reset notification email subject. %s: Site title. */
		$title = sprintf( __( '[%s] New Account', 'user-collections' ), $site_name );

		$email_sent = wp_mail( $user_email, wp_specialchars_decode( $title ), $message );

		if ( ! $email_sent ) {
			return new \WP_Error( 'email_error', __( 'We were unable to send your account details, please try again.' ), array( 'status' => 400 ) );
		}

		return new \WP_REST_Response(
			array(
				'data' => array(
					'user_id' => $user_id,
				),
				'success' => true,
			),
			201
		);
	}

	/**
	 * Login a user
	 *
	 * @since 1.0.0
	 *
	 * @uses \check_ajax_referer()
	 * @uses \wp_signon()
	 *
	 * @return \WP_Error | \WP_REST_Response
	 */
	public function login_user( \WP_REST_Request $request) {

		$token                 = $request->get_param( 'token' );
		$data['user_login']    = $request->get_param( 'user_login' );
		$data['user_password'] = $request->get_param( 'user_password' );
		$data['remember']      = $request->get_param( 'remember' );

		if ( ! $this->verify_recaptcha( $token ) ) {
			return new \WP_Error( 'login_error', __( 'reCaptcha verification has failed.' ) );
		}

		if ( ! $data['user_login'] || ! $data['user_password'] ) {
			return new \WP_Error( 'login_error', __( 'Invalid or missing request parameters.' ) );
		}

		$login = wp_signon( $data, is_ssl() );

		if ( is_wp_error( $login ) ) {
			return $login;
		}

		wp_set_current_user( $login->ID );

		return new \WP_REST_Response(
			array(
				'data' => array(
					'user_id' => $login->ID,
				),
				'success' => true,
			),
			200
		);
	}

	/**
	 * Forgot a user password
	 *
	 * Handles the forgot password process
	 *
	 * @since 1.0.0
	 *
	 * @todo Replace this with \retrieve_password()
	 *
	 * @uses \check_ajax_referer()
	 * @uses \wp_signon()
	 *
	 * @return \WP_Error | \WP_REST_Response
	 */
	public function forgot_user( \WP_REST_Request $request) {

		$data       = array();
		$token      = $request->get_param( 'token' );
		$user_login = $request->get_param( 'user_login' );

		if ( ! $this->verify_recaptcha( $token ) ) {
			return new \WP_Error( 'login_error', __( 'reCaptcha verification has failed.' ) );
		}

		if ( ! $user_login ) {
			return new \WP_Error( 'login_error', __( 'Invalid or missing request parameters.' ) );
		}

		if ( strpos( $user_login, '@' ) ) {
			$data = get_user_by( 'email', $user_login );
		} else {
			$data = get_user_by( 'login', $user_login );
		}

		if ( ! $data ) {
			return new \WP_Error( 'unknown_user', __( 'Unable to find user.' ) );
		}

		// Redefining user_login ensures we return the right case in the email.
		$user_login = $data->user_login;
		$user_email = $data->user_email;
		$site_name  = is_multisite() ? get_network()->site_name : wp_specialchars_decode( get_option( 'blogname' ), ENT_QUOTES );
		$key        = get_password_reset_key( $data );
		$url        = network_site_url( sprintf( '%1$s?intent=reset&key=%2$s&login=%3$s', Options::get_option( 'collection_page', 'favorites' ), $key, rawurlencode( $user_login ) ) );

		$message = __( 'Someone has requested a password reset for the following account:', 'user-collections' ) . "\r\n\r\n";
		/* translators: %s: Site name. */
		$message .= sprintf( __( 'Site Name: %s', 'user-collections' ), $site_name ) . "\r\n\r\n";
		/* translators: %s: User login. */
		$message .= sprintf( __( 'Username: %s', 'user-collections' ), $user_login ) . "\r\n\r\n";
		$message .= __( 'If this was a mistake, just ignore this email and nothing will happen.', 'user-collections' ) . "\r\n\r\n";
		$message .= __( 'To reset your password, visit the following address:', 'user-collections' ) . "\r\n\r\n";
		$message .= '<' . $url . ">\r\n";

		/* translators: Password reset notification email subject. %s: Site title. */
		$title = sprintf( __( '[%s] Password Reset', 'user-collections' ), $site_name );

		$email_sent = wp_mail( $user_email, wp_specialchars_decode( $title ), $message );

		if ( ! $email_sent ) {
			return new \WP_Error( 'reset_error', __( 'We were unable to send your account details, please try again.' ), array( 'status' => 400 ) );
		}

		return new \WP_REST_Response(
			array(
				'data' => array(
					'reset_url' => $url,
					'email_sent' => true,
				),
				'success' => true,
			),
			200
		);
	}

	/**
	 * Reset a user
	 *
	 * Handles the functionality of resetting a user's password
	 * via the password reset form.
	 *
	 * @since 1.0.0
	 *
	 * @uses \check_password_reset_key()
	 * @uses \reset_password()
	 *
	 * @return \WP_Error | \WP_REST_Response
	 */
	public function reset_user( \WP_REST_Request $request) {

		$token       = $request->get_param( 'token' );
		$new_pass_1  = $request->get_param( 'new_pass_1' );
		$new_pass_2  = $request->get_param( 'new_pass_2' );
		$user_login  = $request->get_param( 'user_login' );
		$user_intent = $request->get_param( 'intent' );
		$reset_key   = $request->get_param( 'key' );

		if ( ! $this->verify_recaptcha( $token ) ) {
			return new \WP_Error( 'reset_error', __( 'reCaptcha verification has failed.' ) );
		}

		if ( ! $new_pass_1 || ! $new_pass_2 ) {
			return new \WP_Error( 'reset_error', __( 'A password field is empty or missing.' ) );
		}

		if ( $new_pass_1 !== $new_pass_2 ) {
			return new \WP_Error( 'mismatch_error', __( 'Both passwords should be the same.' ) );
		}

		$user = check_password_reset_key( $reset_key, $user_login );

		if ( is_wp_error( $user ) ) {

			return $user;
		}

		// Parameter checks OK, reset password
		reset_password( $user, $new_pass_1 );

		return new \WP_REST_Response(
			array(
				'data' => array(
					'password_reset' => true
				),
				'success' => true,
			),
			200
		);
	}

	/**
	 * Logout a user
	 *
	 * @since 1.0.0
	 *
	 * @uses  \wp_logout()
	 *
	 * @param  \WP_REST_Request  $request
	 *
	 * @return \WP_REST_Response
	 */
	public function logout_user( \WP_REST_Request $request ): \WP_REST_Response {

		wp_logout();

		return new \WP_REST_Response(
			array(
				'data' => true,
				'success' => true,
			),
			200
		);
	}

	/**
	 * Check if a given request has access to view a collection
	 *
	 * @since 1.0.0
	 *
	 * @param \WP_REST_Request $request Full data about the request.
	 * @return \WP_Error|bool
	 */
	public function login_permissions_check( \WP_REST_Request $request ): bool {
		return true;
	}

	/**
	 * Checks that the reCAPTCHA parameter sent with the registration
	 * request is valid.
	 *
	 * @since 1.0.0
	 *
	 * @uses \wp_remote_post()
	 *
	 * @codeCoverageIgnore
	 *
	 * @param  null  $token
	 *
	 * @return bool True if the CAPTCHA is OK, otherwise false.
	 */
	private function verify_recaptcha( $token = null ): bool {

		// If reCaptcha is disabled, short-circuit this function by returning `true`.
		if ( ! Options::get_option( 'enable_recaptcha' ) ) {
			return true;
		}

		if ( ! $token ) {
			return false;
		}

		// Verify the captcha response from Google.
		$response = wp_remote_post(
			'https://www.google.com/recaptcha/api/siteverify',
			array(
				'body' => array(
					'secret'   => Options::get_option( 'recaptcha_secret_key', '' ),
					'response' => $token
				)
			)
		);

		if ( ! is_wp_error( $response ) ) {

			return json_decode( $response['body'], false )->success;
		}

		return false;
	}

}