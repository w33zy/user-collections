<?php

namespace User_Collections\Inc\Frontend\Shortcodes;

use User_Collections as NS;
use User_Collections\Inc\Core\Options;
use User_Collections\Inc\Common\Abstracts\Shortcode;

/**
 * Class CollectionList
 *
 * @package User_Collections\Inc\Frontend\Shortcodes
 */
class Collection_List extends Shortcode {

	use NS\Inc\Common\Traits\Utilities;

	/**
	 * CollectionList constructor.
	 *
	 * @since    1.0.0
	 *
	 * @param string $plugin_name The name of the plugin.
	 * @param string $version The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version     = $version;

		parent::__construct( $this->plugin_name, $this->version );
	}

	/**
	 * Creates the shortcode for use by WordPress
	 *
	 * @since 1.0.0
	 *
	 * @uses  \do_shortcode()
	 *
	 * @param array $options The shortcode options.
	 *
	 * @return mixed
	 *
	 */
	public function the_shortcode( $options ) {

		// Set default values for options not set explicitly.
		$options = shortcode_atts(
			array(
				'id' => '',
			),
			$options
		);

		$output = $this->render_shortcode( $options );

		wp_enqueue_style( $this->plugin_name . '-add-button' );
		wp_enqueue_style( $this->plugin_name . '-collection-list' );
		wp_enqueue_style( $this->plugin_name . '-authentication-form' );

		wp_enqueue_script( $this->plugin_name . '-collection-list' );
		wp_enqueue_script( $this->plugin_name . '-authentication-form' );

		return do_shortcode( $output );
	}

	/**
	 * Renders the markup of our shortcode content
	 *
	 * @since 1.0.0
	 *
	 * @uses \register_new_user()
	 * @uses \wp_send_new_user_notifications
	 *
	 * @param array $options The shortcode options.
	 *
	 * @return mixed
	 */
	public function render_shortcode( $options ) {

		$out             = '';
		$collector       = isset( $_GET['collector'] ) ? (int) $_GET['collector'] : 0;
		$collection      = isset( $_GET['collection'] ) ? esc_attr( wp_strip_all_tags( $_GET['collection'] ) ) : '';
		$anon_collection = Options::get_option( 'anon_collection' );

		if ( ! $collector && ! $anon_collection && ! is_user_logged_in() ) {
			// Our Svelte anchor.
			$out .= '<div id="collections-authentication"></div>';
		} else {
			// Our Svelte anchor.
			$out .= sprintf(
				'<div id="collections-container" %s %s></div>',
				$this->data_attribute( 'collection', $collection ),
				$this->data_attribute( 'collector', $collector )
			);
		}

		return $out;
	}

}
