<?php

namespace User_Collections\Inc\Frontend;

use User_Collections\Inc\Core\Options;

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @author    Your Name or Your Company
 */
class Frontend {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * The text domain of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_text_domain    The text domain of this plugin.
	 */
	private $plugin_text_domain;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since       1.0.0
	 * @param       string $plugin_name        The name of this plugin.
	 * @param       string $version            The version of this plugin.
	 */
	public function __construct( $plugin_name, $version) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/user-collections-frontend.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name . '-add-button', plugin_dir_url( __FILE__ ) . 'svelte/dist/add-button.css', array(), $this->version, 'all' );
		wp_register_style( $this->plugin_name . '-collection-list', plugin_dir_url( __FILE__ ) . 'svelte/dist/collection-list.css', array(), $this->version, 'all' );
		wp_register_style( $this->plugin_name . '-authentication-form', plugin_dir_url( __FILE__ ) . 'svelte/dist/user-authentication.css', array(), $this->version, 'all' );
	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts(): void {

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/user-collections-frontend.js', array( 'jquery' ), $this->version, true );
		wp_localize_script(
			$this->plugin_name,
			'svelteData',
			array(
				'post_ID'       => get_the_ID(),
				'user_ID'       => get_current_user_id() ?: null,
				'ajax_url'      => admin_url( 'admin-ajax.php' ),
				'nonce'         => wp_create_nonce( 'collection-list' ),
				'signup_nonce'  => wp_create_nonce( 'signup-nonce' ),
				'login_nonce'   => wp_create_nonce( 'login-nonce' ),
				'forgot_nonce'  => wp_create_nonce( 'forgot-nonce' ),
				'reset_nonce'   => wp_create_nonce( 'reset-nonce' ),
				'lost_password' => wp_lostpassword_url(),
				'rest_nonce'    => wp_create_nonce( 'wp_rest' ),
				'logout_url'    => wp_logout_url( home_url() ),
				'post_meta'     => ( get_post() && isset( get_post()->uc_post_meta ) ) ? get_post()->uc_post_meta : null,
			)
		);
		wp_localize_script(
			$this->plugin_name,
			'ucOptions',
			array(
				'site_icon'               => get_site_icon_url(),
				'collections_page'        => esc_url( home_url( Options::get_option( 'collection_page', 'favorites' ) ) ),
				'primary_collection_name' => Options::get_option( 'primary_collection_name', 'Favorites' ),
				'post_saved_message'      => Options::get_option( 'post_saved_message', 'Post saved' ),
				'already_saved_message'   => Options::get_option( 'already_saved_message', 'Post already saved' ),
				'enable_recaptcha'        => Options::get_option( 'enable_recaptcha' ),
				'recaptcha_site_key'      => Options::get_option( 'recaptcha_site_key', '' ),
				'button_type'             => Options::get_option( 'collection_button_type', 'regular-button' ),
				'anon_collection'         => Options::get_option( 'anon_collection' ),
			)
		);

		if ( is_singular() ) {
			wp_register_script( $this->plugin_name . '-add-button', plugin_dir_url( __FILE__ ) . 'svelte/dist/add-button.js', array( 'jquery' ), $this->version, true );
		}

		if ( is_singular() ) {
			wp_register_script( $this->plugin_name . '-collection-list', plugin_dir_url( __FILE__ ) . 'svelte/dist/collection-list.js', array( 'jquery' ), $this->version, true );
		}

		if ( is_singular() ) {
			wp_register_script( $this->plugin_name . '-authentication-form', plugin_dir_url( __FILE__ ) . 'svelte/dist/user-authentication.js', array( 'jquery' ), $this->version, true );
		}

		if ( is_singular() ) {
			$data                             = [];
			$data['post']['user_id']          = get_current_user_id();
			$data['post']['item_id']          = get_the_ID();
			$data['post']['item_title']       = get_the_title();
			$data['post']['item_description'] = wp_strip_all_tags( get_the_excerpt(), true );
			$data['post']['item_url']         = get_the_permalink();
			$data['post']['item_author']      = get_the_author_meta( 'display_name', get_post()->post_author );
			$data['post']['item_category']    = implode( ', ', wp_list_pluck( get_the_category(), 'name' ) );
			$data['post']['item_thumbnail']   = get_the_post_thumbnail_url( null, 'medium' );
			$data['post']['item_published']   = get_the_date( 'Y-m-d H:i:s' );
			$data['post']['item_modified']    = get_the_modified_date( 'Y-m-d H:i:s' );

			wp_add_inline_script(
				$this->plugin_name,
				'window.uc = window.uc || {}; uc.wpData = ' . wp_json_encode( $data )
			);
		}

	}

	/**
	 * Adds an empty div to out footer to acts as an
	 * anchor for Svelte
	 *
	 * @since 1.0.0
	 *
	 * @return void
	 */
	public function svelte_anchor(): void {

		$post_types = apply_filters( 'user_collections_post_types', array( 'post', 'rpr_recipe', 'rcno_review' ) ); // @TODO: Set this as an option

		if ( ! is_singular( $post_types ) ) {
			return;
		}

		wp_enqueue_script( $this->plugin_name . '-add-button' );
		echo '<div id="user-collections-add"></div>';
	}

}
