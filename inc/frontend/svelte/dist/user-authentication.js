var userAuthentication = (function () {
    'use strict';

    function noop() { }
    function assign(tar, src) {
        // @ts-ignore
        for (const k in src)
            tar[k] = src[k];
        return tar;
    }
    function add_location(element, file, line, column, char) {
        element.__svelte_meta = {
            loc: { file, line, column, char }
        };
    }
    function run(fn) {
        return fn();
    }
    function blank_object() {
        return Object.create(null);
    }
    function run_all(fns) {
        fns.forEach(run);
    }
    function is_function(thing) {
        return typeof thing === 'function';
    }
    function safe_not_equal(a, b) {
        return a != a ? b == b : a !== b || ((a && typeof a === 'object') || typeof a === 'function');
    }
    let src_url_equal_anchor;
    function src_url_equal(element_src, url) {
        if (!src_url_equal_anchor) {
            src_url_equal_anchor = document.createElement('a');
        }
        src_url_equal_anchor.href = url;
        return element_src === src_url_equal_anchor.href;
    }
    function is_empty(obj) {
        return Object.keys(obj).length === 0;
    }
    function append(target, node) {
        target.appendChild(node);
    }
    function insert(target, node, anchor) {
        target.insertBefore(node, anchor || null);
    }
    function detach(node) {
        node.parentNode.removeChild(node);
    }
    function element(name) {
        return document.createElement(name);
    }
    function text(data) {
        return document.createTextNode(data);
    }
    function space() {
        return text(' ');
    }
    function empty() {
        return text('');
    }
    function listen(node, event, handler, options) {
        node.addEventListener(event, handler, options);
        return () => node.removeEventListener(event, handler, options);
    }
    function prevent_default(fn) {
        return function (event) {
            event.preventDefault();
            // @ts-ignore
            return fn.call(this, event);
        };
    }
    function attr(node, attribute, value) {
        if (value == null)
            node.removeAttribute(attribute);
        else if (node.getAttribute(attribute) !== value)
            node.setAttribute(attribute, value);
    }
    function children(element) {
        return Array.from(element.childNodes);
    }
    function set_input_value(input, value) {
        input.value = value == null ? '' : value;
    }
    function custom_event(type, detail, { bubbles = false, cancelable = false } = {}) {
        const e = document.createEvent('CustomEvent');
        e.initCustomEvent(type, bubbles, cancelable, detail);
        return e;
    }

    let current_component;
    function set_current_component(component) {
        current_component = component;
    }
    function get_current_component() {
        if (!current_component)
            throw new Error('Function called outside component initialization');
        return current_component;
    }
    function onMount(fn) {
        get_current_component().$$.on_mount.push(fn);
    }
    function createEventDispatcher() {
        const component = get_current_component();
        return (type, detail, { cancelable = false } = {}) => {
            const callbacks = component.$$.callbacks[type];
            if (callbacks) {
                // TODO are there situations where events could be dispatched
                // in a server (non-DOM) environment?
                const event = custom_event(type, detail, { cancelable });
                callbacks.slice().forEach(fn => {
                    fn.call(component, event);
                });
                return !event.defaultPrevented;
            }
            return true;
        };
    }

    const dirty_components = [];
    const binding_callbacks = [];
    const render_callbacks = [];
    const flush_callbacks = [];
    const resolved_promise = Promise.resolve();
    let update_scheduled = false;
    function schedule_update() {
        if (!update_scheduled) {
            update_scheduled = true;
            resolved_promise.then(flush);
        }
    }
    function add_render_callback(fn) {
        render_callbacks.push(fn);
    }
    // flush() calls callbacks in this order:
    // 1. All beforeUpdate callbacks, in order: parents before children
    // 2. All bind:this callbacks, in reverse order: children before parents.
    // 3. All afterUpdate callbacks, in order: parents before children. EXCEPT
    //    for afterUpdates called during the initial onMount, which are called in
    //    reverse order: children before parents.
    // Since callbacks might update component values, which could trigger another
    // call to flush(), the following steps guard against this:
    // 1. During beforeUpdate, any updated components will be added to the
    //    dirty_components array and will cause a reentrant call to flush(). Because
    //    the flush index is kept outside the function, the reentrant call will pick
    //    up where the earlier call left off and go through all dirty components. The
    //    current_component value is saved and restored so that the reentrant call will
    //    not interfere with the "parent" flush() call.
    // 2. bind:this callbacks cannot trigger new flush() calls.
    // 3. During afterUpdate, any updated components will NOT have their afterUpdate
    //    callback called a second time; the seen_callbacks set, outside the flush()
    //    function, guarantees this behavior.
    const seen_callbacks = new Set();
    let flushidx = 0; // Do *not* move this inside the flush() function
    function flush() {
        const saved_component = current_component;
        do {
            // first, call beforeUpdate functions
            // and update components
            while (flushidx < dirty_components.length) {
                const component = dirty_components[flushidx];
                flushidx++;
                set_current_component(component);
                update(component.$$);
            }
            set_current_component(null);
            dirty_components.length = 0;
            flushidx = 0;
            while (binding_callbacks.length)
                binding_callbacks.pop()();
            // then, once components are updated, call
            // afterUpdate functions. This may cause
            // subsequent updates...
            for (let i = 0; i < render_callbacks.length; i += 1) {
                const callback = render_callbacks[i];
                if (!seen_callbacks.has(callback)) {
                    // ...so guard against infinite loops
                    seen_callbacks.add(callback);
                    callback();
                }
            }
            render_callbacks.length = 0;
        } while (dirty_components.length);
        while (flush_callbacks.length) {
            flush_callbacks.pop()();
        }
        update_scheduled = false;
        seen_callbacks.clear();
        set_current_component(saved_component);
    }
    function update($$) {
        if ($$.fragment !== null) {
            $$.update();
            run_all($$.before_update);
            const dirty = $$.dirty;
            $$.dirty = [-1];
            $$.fragment && $$.fragment.p($$.ctx, dirty);
            $$.after_update.forEach(add_render_callback);
        }
    }
    const outroing = new Set();
    let outros;
    function group_outros() {
        outros = {
            r: 0,
            c: [],
            p: outros // parent group
        };
    }
    function check_outros() {
        if (!outros.r) {
            run_all(outros.c);
        }
        outros = outros.p;
    }
    function transition_in(block, local) {
        if (block && block.i) {
            outroing.delete(block);
            block.i(local);
        }
    }
    function transition_out(block, local, detach, callback) {
        if (block && block.o) {
            if (outroing.has(block))
                return;
            outroing.add(block);
            outros.c.push(() => {
                outroing.delete(block);
                if (callback) {
                    if (detach)
                        block.d(1);
                    callback();
                }
            });
            block.o(local);
        }
    }

    function get_spread_update(levels, updates) {
        const update = {};
        const to_null_out = {};
        const accounted_for = { $$scope: 1 };
        let i = levels.length;
        while (i--) {
            const o = levels[i];
            const n = updates[i];
            if (n) {
                for (const key in o) {
                    if (!(key in n))
                        to_null_out[key] = 1;
                }
                for (const key in n) {
                    if (!accounted_for[key]) {
                        update[key] = n[key];
                        accounted_for[key] = 1;
                    }
                }
                levels[i] = n;
            }
            else {
                for (const key in o) {
                    accounted_for[key] = 1;
                }
            }
        }
        for (const key in to_null_out) {
            if (!(key in update))
                update[key] = undefined;
        }
        return update;
    }
    function get_spread_object(spread_props) {
        return typeof spread_props === 'object' && spread_props !== null ? spread_props : {};
    }
    function create_component(block) {
        block && block.c();
    }
    function mount_component(component, target, anchor, customElement) {
        const { fragment, on_mount, on_destroy, after_update } = component.$$;
        fragment && fragment.m(target, anchor);
        if (!customElement) {
            // onMount happens before the initial afterUpdate
            add_render_callback(() => {
                const new_on_destroy = on_mount.map(run).filter(is_function);
                if (on_destroy) {
                    on_destroy.push(...new_on_destroy);
                }
                else {
                    // Edge case - component was destroyed immediately,
                    // most likely as a result of a binding initialising
                    run_all(new_on_destroy);
                }
                component.$$.on_mount = [];
            });
        }
        after_update.forEach(add_render_callback);
    }
    function destroy_component(component, detaching) {
        const $$ = component.$$;
        if ($$.fragment !== null) {
            run_all($$.on_destroy);
            $$.fragment && $$.fragment.d(detaching);
            // TODO null out other refs, including component.$$ (but need to
            // preserve final state?)
            $$.on_destroy = $$.fragment = null;
            $$.ctx = [];
        }
    }
    function make_dirty(component, i) {
        if (component.$$.dirty[0] === -1) {
            dirty_components.push(component);
            schedule_update();
            component.$$.dirty.fill(0);
        }
        component.$$.dirty[(i / 31) | 0] |= (1 << (i % 31));
    }
    function init(component, options, instance, create_fragment, not_equal, props, append_styles, dirty = [-1]) {
        const parent_component = current_component;
        set_current_component(component);
        const $$ = component.$$ = {
            fragment: null,
            ctx: null,
            // state
            props,
            update: noop,
            not_equal,
            bound: blank_object(),
            // lifecycle
            on_mount: [],
            on_destroy: [],
            on_disconnect: [],
            before_update: [],
            after_update: [],
            context: new Map(options.context || (parent_component ? parent_component.$$.context : [])),
            // everything else
            callbacks: blank_object(),
            dirty,
            skip_bound: false,
            root: options.target || parent_component.$$.root
        };
        append_styles && append_styles($$.root);
        let ready = false;
        $$.ctx = instance
            ? instance(component, options.props || {}, (i, ret, ...rest) => {
                const value = rest.length ? rest[0] : ret;
                if ($$.ctx && not_equal($$.ctx[i], $$.ctx[i] = value)) {
                    if (!$$.skip_bound && $$.bound[i])
                        $$.bound[i](value);
                    if (ready)
                        make_dirty(component, i);
                }
                return ret;
            })
            : [];
        $$.update();
        ready = true;
        run_all($$.before_update);
        // `false` as a special case of no DOM component
        $$.fragment = create_fragment ? create_fragment($$.ctx) : false;
        if (options.target) {
            if (options.hydrate) {
                const nodes = children(options.target);
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment && $$.fragment.l(nodes);
                nodes.forEach(detach);
            }
            else {
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment && $$.fragment.c();
            }
            if (options.intro)
                transition_in(component.$$.fragment);
            mount_component(component, options.target, options.anchor, options.customElement);
            flush();
        }
        set_current_component(parent_component);
    }
    /**
     * Base class for Svelte components. Used when dev=false.
     */
    class SvelteComponent {
        $destroy() {
            destroy_component(this, 1);
            this.$destroy = noop;
        }
        $on(type, callback) {
            const callbacks = (this.$$.callbacks[type] || (this.$$.callbacks[type] = []));
            callbacks.push(callback);
            return () => {
                const index = callbacks.indexOf(callback);
                if (index !== -1)
                    callbacks.splice(index, 1);
            };
        }
        $set($$props) {
            if (this.$$set && !is_empty($$props)) {
                this.$$.skip_bound = true;
                this.$$set($$props);
                this.$$.skip_bound = false;
            }
        }
    }

    function dispatch_dev(type, detail) {
        document.dispatchEvent(custom_event(type, Object.assign({ version: '3.48.0' }, detail), { bubbles: true }));
    }
    function append_dev(target, node) {
        dispatch_dev('SvelteDOMInsert', { target, node });
        append(target, node);
    }
    function insert_dev(target, node, anchor) {
        dispatch_dev('SvelteDOMInsert', { target, node, anchor });
        insert(target, node, anchor);
    }
    function detach_dev(node) {
        dispatch_dev('SvelteDOMRemove', { node });
        detach(node);
    }
    function listen_dev(node, event, handler, options, has_prevent_default, has_stop_propagation) {
        const modifiers = options === true ? ['capture'] : options ? Array.from(Object.keys(options)) : [];
        if (has_prevent_default)
            modifiers.push('preventDefault');
        if (has_stop_propagation)
            modifiers.push('stopPropagation');
        dispatch_dev('SvelteDOMAddEventListener', { node, event, handler, modifiers });
        const dispose = listen(node, event, handler, options);
        return () => {
            dispatch_dev('SvelteDOMRemoveEventListener', { node, event, handler, modifiers });
            dispose();
        };
    }
    function attr_dev(node, attribute, value) {
        attr(node, attribute, value);
        if (value == null)
            dispatch_dev('SvelteDOMRemoveAttribute', { node, attribute });
        else
            dispatch_dev('SvelteDOMSetAttribute', { node, attribute, value });
    }
    function set_data_dev(text, data) {
        data = '' + data;
        if (text.wholeText === data)
            return;
        dispatch_dev('SvelteDOMSetData', { node: text, data });
        text.data = data;
    }
    function validate_slots(name, slot, keys) {
        for (const slot_key of Object.keys(slot)) {
            if (!~keys.indexOf(slot_key)) {
                console.warn(`<${name}> received an unexpected slot "${slot_key}".`);
            }
        }
    }
    /**
     * Base class for Svelte components with some minor dev-enhancements. Used when dev=true.
     */
    class SvelteComponentDev extends SvelteComponent {
        constructor(options) {
            if (!options || (!options.target && !options.$$inline)) {
                throw new Error("'target' is a required option");
            }
            super();
        }
        $destroy() {
            super.$destroy();
            this.$destroy = () => {
                console.warn('Component was already destroyed'); // eslint-disable-line no-console
            };
        }
        $capture_state() { }
        $inject_state() { }
    }

    /* src\AuthenticationForm\ErrorMessage.svelte generated by Svelte v3.48.0 */

    const file = "src\\AuthenticationForm\\ErrorMessage.svelte";

    // (20:0) {#if error}
    function create_if_block(ctx) {
    	let div;
    	let p;
    	let t;

    	const block = {
    		c: function create() {
    			div = element("div");
    			p = element("p");
    			t = text(/*error*/ ctx[0]);
    			attr_dev(p, "class", "svelte-2kqrj");
    			add_location(p, file, 21, 8, 322);
    			attr_dev(div, "class", "error-message svelte-2kqrj");
    			add_location(div, file, 20, 1, 286);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			append_dev(div, p);
    			append_dev(p, t);
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*error*/ 1) set_data_dev(t, /*error*/ ctx[0]);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block.name,
    		type: "if",
    		source: "(20:0) {#if error}",
    		ctx
    	});

    	return block;
    }

    function create_fragment(ctx) {
    	let if_block_anchor;
    	let if_block = /*error*/ ctx[0] && create_if_block(ctx);

    	const block = {
    		c: function create() {
    			if (if_block) if_block.c();
    			if_block_anchor = empty();
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			if (if_block) if_block.m(target, anchor);
    			insert_dev(target, if_block_anchor, anchor);
    		},
    		p: function update(ctx, [dirty]) {
    			if (/*error*/ ctx[0]) {
    				if (if_block) {
    					if_block.p(ctx, dirty);
    				} else {
    					if_block = create_if_block(ctx);
    					if_block.c();
    					if_block.m(if_block_anchor.parentNode, if_block_anchor);
    				}
    			} else if (if_block) {
    				if_block.d(1);
    				if_block = null;
    			}
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (if_block) if_block.d(detaching);
    			if (detaching) detach_dev(if_block_anchor);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance($$self, $$props, $$invalidate) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('ErrorMessage', slots, []);
    	let { error } = $$props;
    	const writable_props = ['error'];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<ErrorMessage> was created with unknown prop '${key}'`);
    	});

    	$$self.$$set = $$props => {
    		if ('error' in $$props) $$invalidate(0, error = $$props.error);
    	};

    	$$self.$capture_state = () => ({ error });

    	$$self.$inject_state = $$props => {
    		if ('error' in $$props) $$invalidate(0, error = $$props.error);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [error];
    }

    class ErrorMessage extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance, create_fragment, safe_not_equal, { error: 0 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "ErrorMessage",
    			options,
    			id: create_fragment.name
    		});

    		const { ctx } = this.$$;
    		const props = options.props || {};

    		if (/*error*/ ctx[0] === undefined && !('error' in props)) {
    			console.warn("<ErrorMessage> was created without expected prop 'error'");
    		}
    	}

    	get error() {
    		throw new Error("<ErrorMessage>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set error(value) {
    		throw new Error("<ErrorMessage>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src\AuthenticationForm\SignupForm.svelte generated by Svelte v3.48.0 */
    const file$1 = "src\\AuthenticationForm\\SignupForm.svelte";

    // (79:0) {:else}
    function create_else_block(ctx) {
    	let div;
    	let h2;
    	let t1;
    	let p;

    	const block = {
    		c: function create() {
    			div = element("div");
    			h2 = element("h2");
    			h2.textContent = "We have sent you and email";
    			t1 = space();
    			p = element("p");
    			p.textContent = "Please follow the instructions in the email you have just received from us to finish creating your account.";
    			add_location(h2, file$1, 80, 2, 2001);
    			add_location(p, file$1, 81, 2, 2040);
    			attr_dev(div, "class", "uc-signup-message");
    			add_location(div, file$1, 79, 1, 1966);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			append_dev(div, h2);
    			append_dev(div, t1);
    			append_dev(div, p);
    		},
    		p: noop,
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_else_block.name,
    		type: "else",
    		source: "(79:0) {:else}",
    		ctx
    	});

    	return block;
    }

    // (57:0) {#if form}
    function create_if_block$1(ctx) {
    	let div5;
    	let errormessage;
    	let t0;
    	let form_1;
    	let div0;
    	let label0;
    	let t2;
    	let input0;
    	let t3;
    	let div1;
    	let label1;
    	let t5;
    	let input1;
    	let t6;
    	let div4;
    	let input2;
    	let t7;
    	let div2;
    	let t8;
    	let div3;
    	let button;
    	let t9;
    	let button_class_value;
    	let current;
    	let mounted;
    	let dispose;

    	errormessage = new ErrorMessage({
    			props: { error: /*errorMsg*/ ctx[3] },
    			$$inline: true
    		});

    	const block = {
    		c: function create() {
    			div5 = element("div");
    			create_component(errormessage.$$.fragment);
    			t0 = space();
    			form_1 = element("form");
    			div0 = element("div");
    			label0 = element("label");
    			label0.textContent = "Username";
    			t2 = space();
    			input0 = element("input");
    			t3 = space();
    			div1 = element("div");
    			label1 = element("label");
    			label1.textContent = "Email address";
    			t5 = space();
    			input1 = element("input");
    			t6 = space();
    			div4 = element("div");
    			input2 = element("input");
    			t7 = space();
    			div2 = element("div");
    			t8 = space();
    			div3 = element("div");
    			button = element("button");
    			t9 = text("Register");
    			attr_dev(label0, "for", "username");
    			add_location(label0, file$1, 62, 4, 1348);
    			attr_dev(input0, "type", "text");
    			attr_dev(input0, "id", "username");
    			attr_dev(input0, "class", "svelte-18pa6sc");
    			add_location(input0, file$1, 63, 4, 1392);
    			attr_dev(div0, "class", "username-container svelte-18pa6sc");
    			add_location(div0, file$1, 61, 3, 1310);
    			attr_dev(label1, "for", "email");
    			add_location(label1, file$1, 66, 4, 1505);
    			attr_dev(input1, "type", "email");
    			attr_dev(input1, "id", "email");
    			attr_dev(input1, "class", "svelte-18pa6sc");
    			add_location(input1, file$1, 67, 4, 1551);
    			attr_dev(div1, "class", "email-container svelte-18pa6sc");
    			add_location(div1, file$1, 65, 3, 1470);
    			attr_dev(input2, "type", "hidden");
    			attr_dev(input2, "id", "website");
    			add_location(input2, file$1, 70, 4, 1663);
    			attr_dev(div2, "class", "g-recaptcha");
    			add_location(div2, file$1, 71, 4, 1729);
    			attr_dev(button, "class", button_class_value = "button submit " + (/*disabled*/ ctx[2] ? 'disabled' : 'enabled') + " svelte-18pa6sc");
    			add_location(button, file$1, 73, 5, 1791);
    			attr_dev(div3, "class", "col1");
    			add_location(div3, file$1, 72, 4, 1766);
    			attr_dev(div4, "class", "submit-container svelte-18pa6sc");
    			add_location(div4, file$1, 69, 3, 1627);
    			attr_dev(form_1, "action", "/");
    			add_location(form_1, file$1, 60, 2, 1288);
    			attr_dev(div5, "class", "uc-signup-form svelte-18pa6sc");
    			add_location(div5, file$1, 57, 1, 1215);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div5, anchor);
    			mount_component(errormessage, div5, null);
    			append_dev(div5, t0);
    			append_dev(div5, form_1);
    			append_dev(form_1, div0);
    			append_dev(div0, label0);
    			append_dev(div0, t2);
    			append_dev(div0, input0);
    			set_input_value(input0, /*data*/ ctx[0].user_login);
    			append_dev(form_1, t3);
    			append_dev(form_1, div1);
    			append_dev(div1, label1);
    			append_dev(div1, t5);
    			append_dev(div1, input1);
    			set_input_value(input1, /*data*/ ctx[0].user_email);
    			append_dev(form_1, t6);
    			append_dev(form_1, div4);
    			append_dev(div4, input2);
    			set_input_value(input2, /*data*/ ctx[0].website);
    			append_dev(div4, t7);
    			append_dev(div4, div2);
    			append_dev(div4, t8);
    			append_dev(div4, div3);
    			append_dev(div3, button);
    			append_dev(button, t9);
    			current = true;

    			if (!mounted) {
    				dispose = [
    					listen_dev(input0, "input", /*input0_input_handler*/ ctx[5]),
    					listen_dev(input1, "input", /*input1_input_handler*/ ctx[6]),
    					listen_dev(input2, "input", /*input2_input_handler*/ ctx[7]),
    					listen_dev(button, "click", prevent_default(/*verifyForm*/ ctx[4]), false, true, false)
    				];

    				mounted = true;
    			}
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*data*/ 1 && input0.value !== /*data*/ ctx[0].user_login) {
    				set_input_value(input0, /*data*/ ctx[0].user_login);
    			}

    			if (dirty & /*data*/ 1 && input1.value !== /*data*/ ctx[0].user_email) {
    				set_input_value(input1, /*data*/ ctx[0].user_email);
    			}

    			if (dirty & /*data*/ 1) {
    				set_input_value(input2, /*data*/ ctx[0].website);
    			}

    			if (!current || dirty & /*disabled*/ 4 && button_class_value !== (button_class_value = "button submit " + (/*disabled*/ ctx[2] ? 'disabled' : 'enabled') + " svelte-18pa6sc")) {
    				attr_dev(button, "class", button_class_value);
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(errormessage.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(errormessage.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div5);
    			destroy_component(errormessage);
    			mounted = false;
    			run_all(dispose);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$1.name,
    		type: "if",
    		source: "(57:0) {#if form}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$1(ctx) {
    	let current_block_type_index;
    	let if_block;
    	let if_block_anchor;
    	let current;
    	const if_block_creators = [create_if_block$1, create_else_block];
    	const if_blocks = [];

    	function select_block_type(ctx, dirty) {
    		if (/*form*/ ctx[1]) return 0;
    		return 1;
    	}

    	current_block_type_index = select_block_type(ctx);
    	if_block = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);

    	const block = {
    		c: function create() {
    			if_block.c();
    			if_block_anchor = empty();
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			if_blocks[current_block_type_index].m(target, anchor);
    			insert_dev(target, if_block_anchor, anchor);
    			current = true;
    		},
    		p: function update(ctx, [dirty]) {
    			let previous_block_index = current_block_type_index;
    			current_block_type_index = select_block_type(ctx);

    			if (current_block_type_index === previous_block_index) {
    				if_blocks[current_block_type_index].p(ctx, dirty);
    			} else {
    				group_outros();

    				transition_out(if_blocks[previous_block_index], 1, 1, () => {
    					if_blocks[previous_block_index] = null;
    				});

    				check_outros();
    				if_block = if_blocks[current_block_type_index];

    				if (!if_block) {
    					if_block = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);
    					if_block.c();
    				} else {
    					if_block.p(ctx, dirty);
    				}

    				transition_in(if_block, 1);
    				if_block.m(if_block_anchor.parentNode, if_block_anchor);
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(if_block);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(if_block);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if_blocks[current_block_type_index].d(detaching);
    			if (detaching) detach_dev(if_block_anchor);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$1.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$1($$self, $$props, $$invalidate) {
    	let disabled;
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('SignupForm', slots, []);
    	let form = true;
    	let errorMsg = false;

    	let data = {
    		user_login: '',
    		user_email: '',
    		website: ''
    	};

    	function submitForm(token) {
    		if (data.website) return;

    		if (data.user_login && data.user_email) {
    			$$invalidate(0, data.token = token, data);

    			fetch(`/wp-json/uc/v1/authentication/register`, {
    				method: 'POST',
    				headers: {
    					'Content-Type': 'application/json',
    					'X-WP-Nonce': window?.svelteData?.rest_nonce
    				},
    				body: JSON.stringify(data)
    			}).then(async res => {
    				const { data: _data } = await res.json();

    				if (_data.signed_up) {
    					$$invalidate(0, data.user_login = '', data);
    					$$invalidate(0, data.user_email = '', data);
    					$$invalidate(1, form = false);
    				}
    			});
    		}
    	}

    	function verifyForm() {
    		if (typeof window.grecaptcha !== 'undefined') {
    			window.grecaptcha.ready(function () {
    				window.grecaptcha.execute(window.ucOptions.recaptcha_site_key, { action: 'uc_signup' }).then(function (token) {
    					submitForm(token);
    				});
    			});
    		} else {
    			// reCaptcha is disabled
    			submitForm(null);
    		}
    	}

    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<SignupForm> was created with unknown prop '${key}'`);
    	});

    	function input0_input_handler() {
    		data.user_login = this.value;
    		$$invalidate(0, data);
    	}

    	function input1_input_handler() {
    		data.user_email = this.value;
    		$$invalidate(0, data);
    	}

    	function input2_input_handler() {
    		data.website = this.value;
    		$$invalidate(0, data);
    	}

    	$$self.$capture_state = () => ({
    		ErrorMessage,
    		form,
    		errorMsg,
    		data,
    		submitForm,
    		verifyForm,
    		disabled
    	});

    	$$self.$inject_state = $$props => {
    		if ('form' in $$props) $$invalidate(1, form = $$props.form);
    		if ('errorMsg' in $$props) $$invalidate(3, errorMsg = $$props.errorMsg);
    		if ('data' in $$props) $$invalidate(0, data = $$props.data);
    		if ('disabled' in $$props) $$invalidate(2, disabled = $$props.disabled);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	$$self.$$.update = () => {
    		if ($$self.$$.dirty & /*data*/ 1) {
    			 $$invalidate(2, disabled = !(data.user_login && data.user_email));
    		}
    	};

    	return [
    		data,
    		form,
    		disabled,
    		errorMsg,
    		verifyForm,
    		input0_input_handler,
    		input1_input_handler,
    		input2_input_handler
    	];
    }

    class SignupForm extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$1, create_fragment$1, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "SignupForm",
    			options,
    			id: create_fragment$1.name
    		});
    	}
    }

    /* src\AuthenticationForm\LoginForm.svelte generated by Svelte v3.48.0 */
    const file$2 = "src\\AuthenticationForm\\LoginForm.svelte";

    function create_fragment$2(ctx) {
    	let div5;
    	let errormessage;
    	let t0;
    	let form;
    	let div0;
    	let label0;
    	let t2;
    	let input0;
    	let t3;
    	let div1;
    	let label1;
    	let t5;
    	let input1;
    	let t6;
    	let div4;
    	let div2;
    	let button;
    	let t7;
    	let button_class_value;
    	let t8;
    	let input2;
    	let t9;
    	let input3;
    	let t10;
    	let label2;
    	let t12;
    	let div3;
    	let span;
    	let current;
    	let mounted;
    	let dispose;

    	errormessage = new ErrorMessage({
    			props: { error: /*errorMsg*/ ctx[3] },
    			$$inline: true
    		});

    	const block = {
    		c: function create() {
    			div5 = element("div");
    			create_component(errormessage.$$.fragment);
    			t0 = space();
    			form = element("form");
    			div0 = element("div");
    			label0 = element("label");
    			label0.textContent = "Username";
    			t2 = space();
    			input0 = element("input");
    			t3 = space();
    			div1 = element("div");
    			label1 = element("label");
    			label1.textContent = "Password";
    			t5 = space();
    			input1 = element("input");
    			t6 = space();
    			div4 = element("div");
    			div2 = element("div");
    			button = element("button");
    			t7 = text("Login");
    			t8 = space();
    			input2 = element("input");
    			t9 = space();
    			input3 = element("input");
    			t10 = space();
    			label2 = element("label");
    			label2.textContent = "Remember me";
    			t12 = space();
    			div3 = element("div");
    			span = element("span");
    			span.textContent = "Forgot my password";
    			attr_dev(label0, "for", "_username");
    			add_location(label0, file$2, 63, 3, 1446);
    			attr_dev(input0, "type", "text");
    			attr_dev(input0, "id", "_username");
    			attr_dev(input0, "class", "svelte-kfsufd");
    			add_location(input0, file$2, 64, 3, 1490);
    			attr_dev(div0, "class", "username-container svelte-kfsufd");
    			add_location(div0, file$2, 62, 2, 1409);
    			attr_dev(label1, "for", "_password");
    			add_location(label1, file$2, 67, 3, 1604);
    			attr_dev(input1, "type", "password");
    			attr_dev(input1, "id", "_password");
    			attr_dev(input1, "class", "svelte-kfsufd");
    			add_location(input1, file$2, 68, 3, 1648);
    			attr_dev(div1, "class", "password-container svelte-kfsufd");
    			add_location(div1, file$2, 66, 2, 1567);
    			attr_dev(button, "class", button_class_value = "button submit " + (/*disabled*/ ctx[1] ? 'disabled' : 'enabled') + " svelte-kfsufd");
    			add_location(button, file$2, 72, 4, 1791);
    			attr_dev(input2, "type", "hidden");
    			attr_dev(input2, "id", "_website");
    			add_location(input2, file$2, 73, 4, 1914);
    			attr_dev(input3, "type", "checkbox");
    			attr_dev(input3, "id", "_remember");
    			add_location(input3, file$2, 74, 4, 1981);
    			attr_dev(label2, "for", "_remember");
    			add_location(label2, file$2, 75, 4, 2056);
    			attr_dev(div2, "class", "col1");
    			add_location(div2, file$2, 71, 3, 1767);
    			attr_dev(span, "class", "forgot svelte-kfsufd");
    			add_location(span, file$2, 78, 4, 2138);
    			attr_dev(div3, "class", "col2");
    			add_location(div3, file$2, 77, 3, 2114);
    			attr_dev(div4, "class", "submit-container svelte-kfsufd");
    			add_location(div4, file$2, 70, 2, 1732);
    			attr_dev(form, "action", "/");
    			add_location(form, file$2, 61, 1, 1388);
    			attr_dev(div5, "class", "uc-login-form svelte-kfsufd");
    			add_location(div5, file$2, 58, 0, 1318);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div5, anchor);
    			mount_component(errormessage, div5, null);
    			append_dev(div5, t0);
    			append_dev(div5, form);
    			append_dev(form, div0);
    			append_dev(div0, label0);
    			append_dev(div0, t2);
    			append_dev(div0, input0);
    			set_input_value(input0, /*data*/ ctx[0].user_login);
    			append_dev(form, t3);
    			append_dev(form, div1);
    			append_dev(div1, label1);
    			append_dev(div1, t5);
    			append_dev(div1, input1);
    			set_input_value(input1, /*data*/ ctx[0].user_password);
    			append_dev(form, t6);
    			append_dev(form, div4);
    			append_dev(div4, div2);
    			append_dev(div2, button);
    			append_dev(button, t7);
    			append_dev(div2, t8);
    			append_dev(div2, input2);
    			set_input_value(input2, /*data*/ ctx[0].website);
    			append_dev(div2, t9);
    			append_dev(div2, input3);
    			input3.checked = /*data*/ ctx[0].remember;
    			append_dev(div2, t10);
    			append_dev(div2, label2);
    			append_dev(div4, t12);
    			append_dev(div4, div3);
    			append_dev(div3, span);
    			current = true;

    			if (!mounted) {
    				dispose = [
    					listen_dev(input0, "input", /*input0_input_handler*/ ctx[5]),
    					listen_dev(input1, "input", /*input1_input_handler*/ ctx[6]),
    					listen_dev(button, "click", prevent_default(/*verifyForm*/ ctx[4]), false, true, false),
    					listen_dev(input2, "input", /*input2_input_handler*/ ctx[7]),
    					listen_dev(input3, "change", /*input3_change_handler*/ ctx[8]),
    					listen_dev(span, "click", /*click_handler*/ ctx[9], false, false, false)
    				];

    				mounted = true;
    			}
    		},
    		p: function update(ctx, [dirty]) {
    			if (dirty & /*data*/ 1 && input0.value !== /*data*/ ctx[0].user_login) {
    				set_input_value(input0, /*data*/ ctx[0].user_login);
    			}

    			if (dirty & /*data*/ 1 && input1.value !== /*data*/ ctx[0].user_password) {
    				set_input_value(input1, /*data*/ ctx[0].user_password);
    			}

    			if (!current || dirty & /*disabled*/ 2 && button_class_value !== (button_class_value = "button submit " + (/*disabled*/ ctx[1] ? 'disabled' : 'enabled') + " svelte-kfsufd")) {
    				attr_dev(button, "class", button_class_value);
    			}

    			if (dirty & /*data*/ 1) {
    				set_input_value(input2, /*data*/ ctx[0].website);
    			}

    			if (dirty & /*data*/ 1) {
    				input3.checked = /*data*/ ctx[0].remember;
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(errormessage.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(errormessage.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div5);
    			destroy_component(errormessage);
    			mounted = false;
    			run_all(dispose);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$2.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$2($$self, $$props, $$invalidate) {
    	let disabled;
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('LoginForm', slots, []);
    	const dispatch = createEventDispatcher();
    	let errorMsg = false;

    	let data = {
    		user_login: '',
    		user_password: '',
    		website: '',
    		remember: false
    	};

    	function submitForm(token) {
    		if (data.website) return;

    		if (data.user_login && data.user_password) {
    			$$invalidate(0, data.token = token, data);

    			fetch(`/wp-json/uc/v1/authentication/login`, {
    				method: 'POST',
    				headers: {
    					'Content-Type': 'application/json',
    					'X-WP-Nonce': window?.svelteData?.rest_nonce
    				},
    				body: JSON.stringify(data)
    			}).then(async res => {
    				const { data: _data } = await res.json();

    				if (_data.logged_in) {
    					$$invalidate(0, data.user_login = '', data);
    					$$invalidate(0, data.user_password = '', data);
    					window.location.reload(true);
    				}
    			});
    		}
    	}

    	function verifyForm() {
    		if (typeof window.grecaptcha !== 'undefined') {
    			window.grecaptcha.ready(function () {
    				window.grecaptcha.execute(window.ucOptions.recaptcha_site_key, { action: 'uc_login' }).then(function (token) {
    					submitForm(token);
    				});
    			});
    		} else {
    			// reCaptcha is disabled
    			submitForm(null);
    		}
    	}

    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<LoginForm> was created with unknown prop '${key}'`);
    	});

    	function input0_input_handler() {
    		data.user_login = this.value;
    		$$invalidate(0, data);
    	}

    	function input1_input_handler() {
    		data.user_password = this.value;
    		$$invalidate(0, data);
    	}

    	function input2_input_handler() {
    		data.website = this.value;
    		$$invalidate(0, data);
    	}

    	function input3_change_handler() {
    		data.remember = this.checked;
    		$$invalidate(0, data);
    	}

    	const click_handler = () => dispatch('forgot');

    	$$self.$capture_state = () => ({
    		createEventDispatcher,
    		ErrorMessage,
    		dispatch,
    		errorMsg,
    		data,
    		submitForm,
    		verifyForm,
    		disabled
    	});

    	$$self.$inject_state = $$props => {
    		if ('errorMsg' in $$props) $$invalidate(3, errorMsg = $$props.errorMsg);
    		if ('data' in $$props) $$invalidate(0, data = $$props.data);
    		if ('disabled' in $$props) $$invalidate(1, disabled = $$props.disabled);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	$$self.$$.update = () => {
    		if ($$self.$$.dirty & /*data*/ 1) {
    			 $$invalidate(1, disabled = !(data.user_login && data.user_password));
    		}
    	};

    	return [
    		data,
    		disabled,
    		dispatch,
    		errorMsg,
    		verifyForm,
    		input0_input_handler,
    		input1_input_handler,
    		input2_input_handler,
    		input3_change_handler,
    		click_handler
    	];
    }

    class LoginForm extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$2, create_fragment$2, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "LoginForm",
    			options,
    			id: create_fragment$2.name
    		});
    	}
    }

    /* src\AuthenticationForm\ForgotForm.svelte generated by Svelte v3.48.0 */
    const file$3 = "src\\AuthenticationForm\\ForgotForm.svelte";

    function create_fragment$3(ctx) {
    	let div1;
    	let div0;
    	let h3;
    	let t1;
    	let div6;
    	let errormessage;
    	let t2;
    	let form;
    	let div2;
    	let label;
    	let t4;
    	let input0;
    	let t5;
    	let div5;
    	let input1;
    	let t6;
    	let div3;
    	let t7;
    	let div4;
    	let button0;
    	let t8;
    	let button0_class_value;
    	let t9;
    	let button1;
    	let current;
    	let mounted;
    	let dispose;

    	errormessage = new ErrorMessage({
    			props: { error: /*errorMsg*/ ctx[3] },
    			$$inline: true
    		});

    	const block = {
    		c: function create() {
    			div1 = element("div");
    			div0 = element("div");
    			h3 = element("h3");
    			h3.textContent = "I Forgot My Password";
    			t1 = space();
    			div6 = element("div");
    			create_component(errormessage.$$.fragment);
    			t2 = space();
    			form = element("form");
    			div2 = element("div");
    			label = element("label");
    			label.textContent = "Email address";
    			t4 = space();
    			input0 = element("input");
    			t5 = space();
    			div5 = element("div");
    			input1 = element("input");
    			t6 = space();
    			div3 = element("div");
    			t7 = space();
    			div4 = element("div");
    			button0 = element("button");
    			t8 = text("Send");
    			t9 = space();
    			button1 = element("button");
    			button1.textContent = "Cancel";
    			attr_dev(h3, "class", "svelte-18da6a4");
    			add_location(h3, file$3, 58, 2, 1300);
    			attr_dev(div0, "class", "form-signup");
    			add_location(div0, file$3, 57, 1, 1271);
    			attr_dev(div1, "class", "form-selection svelte-18da6a4");
    			add_location(div1, file$3, 56, 0, 1240);
    			attr_dev(label, "for", "user_login");
    			add_location(label, file$3, 65, 3, 1472);
    			attr_dev(input0, "type", "email");
    			attr_dev(input0, "id", "user_login");
    			attr_dev(input0, "name", "user_login");
    			attr_dev(input0, "class", "svelte-18da6a4");
    			add_location(input0, file$3, 66, 3, 1522);
    			attr_dev(div2, "class", "email-container svelte-18da6a4");
    			add_location(div2, file$3, 64, 2, 1438);
    			attr_dev(input1, "type", "hidden");
    			attr_dev(input1, "id", "website");
    			add_location(input1, file$3, 69, 3, 1654);
    			attr_dev(div3, "class", "g-recaptcha");
    			add_location(div3, file$3, 70, 3, 1719);
    			attr_dev(button0, "class", button0_class_value = "submit " + (/*disabled*/ ctx[1] ? 'disabled' : 'enabled') + " svelte-18da6a4");
    			add_location(button0, file$3, 72, 4, 1779);
    			attr_dev(button1, "class", "cancel svelte-18da6a4");
    			add_location(button1, file$3, 73, 4, 1896);
    			attr_dev(div4, "class", "col1");
    			add_location(div4, file$3, 71, 3, 1755);
    			attr_dev(div5, "class", "submit-container svelte-18da6a4");
    			add_location(div5, file$3, 68, 2, 1619);
    			attr_dev(form, "action", "/");
    			add_location(form, file$3, 63, 1, 1417);
    			attr_dev(div6, "class", "uc-forgot-form svelte-18da6a4");
    			add_location(div6, file$3, 61, 0, 1348);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div1, anchor);
    			append_dev(div1, div0);
    			append_dev(div0, h3);
    			insert_dev(target, t1, anchor);
    			insert_dev(target, div6, anchor);
    			mount_component(errormessage, div6, null);
    			append_dev(div6, t2);
    			append_dev(div6, form);
    			append_dev(form, div2);
    			append_dev(div2, label);
    			append_dev(div2, t4);
    			append_dev(div2, input0);
    			set_input_value(input0, /*data*/ ctx[0].user_login);
    			append_dev(form, t5);
    			append_dev(form, div5);
    			append_dev(div5, input1);
    			set_input_value(input1, /*data*/ ctx[0].website);
    			append_dev(div5, t6);
    			append_dev(div5, div3);
    			append_dev(div5, t7);
    			append_dev(div5, div4);
    			append_dev(div4, button0);
    			append_dev(button0, t8);
    			append_dev(div4, t9);
    			append_dev(div4, button1);
    			current = true;

    			if (!mounted) {
    				dispose = [
    					listen_dev(input0, "input", /*input0_input_handler*/ ctx[5]),
    					listen_dev(input1, "input", /*input1_input_handler*/ ctx[6]),
    					listen_dev(button0, "click", prevent_default(/*verifyForm*/ ctx[4]), false, true, false),
    					listen_dev(button1, "click", prevent_default(/*click_handler*/ ctx[7]), false, true, false)
    				];

    				mounted = true;
    			}
    		},
    		p: function update(ctx, [dirty]) {
    			if (dirty & /*data*/ 1 && input0.value !== /*data*/ ctx[0].user_login) {
    				set_input_value(input0, /*data*/ ctx[0].user_login);
    			}

    			if (dirty & /*data*/ 1) {
    				set_input_value(input1, /*data*/ ctx[0].website);
    			}

    			if (!current || dirty & /*disabled*/ 2 && button0_class_value !== (button0_class_value = "submit " + (/*disabled*/ ctx[1] ? 'disabled' : 'enabled') + " svelte-18da6a4")) {
    				attr_dev(button0, "class", button0_class_value);
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(errormessage.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(errormessage.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div1);
    			if (detaching) detach_dev(t1);
    			if (detaching) detach_dev(div6);
    			destroy_component(errormessage);
    			mounted = false;
    			run_all(dispose);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$3.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$3($$self, $$props, $$invalidate) {
    	let disabled;
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('ForgotForm', slots, []);
    	const dispatch = createEventDispatcher();
    	let errorMsg = false;

    	let data = {
    		user_login: '',
    		website: '',
    		forgot_nonce: window.svelteData.forgot_nonce,
    		action: 'forgot_form'
    	};

    	function submitForm(token) {
    		if (data.website) return;

    		if (data.user_login) {
    			$$invalidate(0, data.token = token, data);

    			fetch(`/wp-json/uc/v1/authentication/forgot`, {
    				method: 'POST',
    				headers: {
    					'Content-Type': 'application/json',
    					'X-WP-Nonce': window?.svelteData?.rest_nonce
    				},
    				body: JSON.stringify(data)
    			}).then(async res => {
    				const { data: _data } = await res.json();

    				if (_data.email_sent) {
    					$$invalidate(0, data.user_login = '', data);
    				}
    			});
    		}
    	}

    	function verifyForm() {
    		if (typeof window.grecaptcha !== 'undefined') {
    			window.grecaptcha.ready(function () {
    				window.grecaptcha.execute(window.ucOptions.recaptcha_site_key, { action: 'uc_forgot' }).then(function (token) {
    					submitForm(token);
    				});
    			});
    		} else {
    			// reCaptcha is disabled
    			submitForm(null);
    		}
    	}

    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<ForgotForm> was created with unknown prop '${key}'`);
    	});

    	function input0_input_handler() {
    		data.user_login = this.value;
    		$$invalidate(0, data);
    	}

    	function input1_input_handler() {
    		data.website = this.value;
    		$$invalidate(0, data);
    	}

    	const click_handler = () => dispatch('cancel');

    	$$self.$capture_state = () => ({
    		createEventDispatcher,
    		ErrorMessage,
    		dispatch,
    		errorMsg,
    		data,
    		submitForm,
    		verifyForm,
    		disabled
    	});

    	$$self.$inject_state = $$props => {
    		if ('errorMsg' in $$props) $$invalidate(3, errorMsg = $$props.errorMsg);
    		if ('data' in $$props) $$invalidate(0, data = $$props.data);
    		if ('disabled' in $$props) $$invalidate(1, disabled = $$props.disabled);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	$$self.$$.update = () => {
    		if ($$self.$$.dirty & /*data*/ 1) {
    			 $$invalidate(1, disabled = !data.user_login);
    		}
    	};

    	return [
    		data,
    		disabled,
    		dispatch,
    		errorMsg,
    		verifyForm,
    		input0_input_handler,
    		input1_input_handler,
    		click_handler
    	];
    }

    class ForgotForm extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$3, create_fragment$3, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "ForgotForm",
    			options,
    			id: create_fragment$3.name
    		});
    	}
    }

    /* src\AuthenticationForm\ResetForm.svelte generated by Svelte v3.48.0 */
    const file$4 = "src\\AuthenticationForm\\ResetForm.svelte";

    // (76:1) {:else}
    function create_else_block$1(ctx) {
    	let div;
    	let h3;

    	const block = {
    		c: function create() {
    			div = element("div");
    			h3 = element("h3");
    			h3.textContent = "Register My Password";
    			attr_dev(h3, "class", "svelte-1dfeqlc");
    			add_location(h3, file$4, 77, 3, 1766);
    			attr_dev(div, "class", "form-title");
    			add_location(div, file$4, 76, 2, 1737);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			append_dev(div, h3);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_else_block$1.name,
    		type: "else",
    		source: "(76:1) {:else}",
    		ctx
    	});

    	return block;
    }

    // (72:1) {#if intent === 'reset'}
    function create_if_block$2(ctx) {
    	let div;
    	let h3;

    	const block = {
    		c: function create() {
    			div = element("div");
    			h3 = element("h3");
    			h3.textContent = "Reset My Password";
    			attr_dev(h3, "class", "svelte-1dfeqlc");
    			add_location(h3, file$4, 73, 3, 1687);
    			attr_dev(div, "class", "form-title");
    			add_location(div, file$4, 72, 2, 1658);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			append_dev(div, h3);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$2.name,
    		type: "if",
    		source: "(72:1) {#if intent === 'reset'}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$4(ctx) {
    	let div0;
    	let t0;
    	let div6;
    	let errormessage;
    	let t1;
    	let form;
    	let div1;
    	let label0;
    	let t3;
    	let input0;
    	let t4;
    	let div2;
    	let label1;
    	let t6;
    	let input1;
    	let t7;
    	let div5;
    	let input2;
    	let t8;
    	let div3;
    	let t9;
    	let div4;
    	let button0;

    	let t10_value = (/*intent*/ ctx[0] === 'reset'
    	? 'Reset Password'
    	: 'Create New Password') + "";

    	let t10;
    	let button0_class_value;
    	let t11;
    	let button1;
    	let current;
    	let mounted;
    	let dispose;

    	function select_block_type(ctx, dirty) {
    		if (/*intent*/ ctx[0] === 'reset') return create_if_block$2;
    		return create_else_block$1;
    	}

    	let current_block_type = select_block_type(ctx);
    	let if_block = current_block_type(ctx);

    	errormessage = new ErrorMessage({
    			props: { error: /*errorMsg*/ ctx[3] },
    			$$inline: true
    		});

    	const block = {
    		c: function create() {
    			div0 = element("div");
    			if_block.c();
    			t0 = space();
    			div6 = element("div");
    			create_component(errormessage.$$.fragment);
    			t1 = space();
    			form = element("form");
    			div1 = element("div");
    			label0 = element("label");
    			label0.textContent = "Password";
    			t3 = space();
    			input0 = element("input");
    			t4 = space();
    			div2 = element("div");
    			label1 = element("label");
    			label1.textContent = "Retype Your Password";
    			t6 = space();
    			input1 = element("input");
    			t7 = space();
    			div5 = element("div");
    			input2 = element("input");
    			t8 = space();
    			div3 = element("div");
    			t9 = space();
    			div4 = element("div");
    			button0 = element("button");
    			t10 = text(t10_value);
    			t11 = space();
    			button1 = element("button");
    			button1.textContent = "Cancel";
    			attr_dev(div0, "class", "form-selection svelte-1dfeqlc");
    			add_location(div0, file$4, 70, 0, 1599);
    			attr_dev(label0, "for", "user_pass_1");
    			add_location(label0, file$4, 86, 3, 1951);
    			attr_dev(input0, "type", "password");
    			attr_dev(input0, "id", "user_pass_1");
    			attr_dev(input0, "name", "user_pass_1");
    			attr_dev(input0, "class", "svelte-1dfeqlc");
    			add_location(input0, file$4, 87, 3, 1997);
    			attr_dev(div1, "class", "password-container svelte-1dfeqlc");
    			add_location(div1, file$4, 85, 2, 1914);
    			attr_dev(label1, "for", "user_pass_2");
    			add_location(label1, file$4, 90, 3, 2137);
    			attr_dev(input1, "type", "password");
    			attr_dev(input1, "id", "user_pass_2");
    			attr_dev(input1, "name", "user_pass_2");
    			attr_dev(input1, "class", "svelte-1dfeqlc");
    			add_location(input1, file$4, 91, 3, 2195);
    			attr_dev(div2, "class", "password-container svelte-1dfeqlc");
    			add_location(div2, file$4, 89, 2, 2100);
    			attr_dev(input2, "type", "hidden");
    			attr_dev(input2, "id", "website");
    			add_location(input2, file$4, 94, 3, 2333);
    			attr_dev(div3, "class", "g-recaptcha");
    			add_location(div3, file$4, 95, 3, 2398);
    			attr_dev(button0, "class", button0_class_value = "submit " + (/*disabled*/ ctx[2] ? 'disabled' : 'enabled') + " svelte-1dfeqlc");
    			add_location(button0, file$4, 97, 4, 2458);
    			attr_dev(button1, "class", "cancel svelte-1dfeqlc");
    			add_location(button1, file$4, 100, 4, 2647);
    			attr_dev(div4, "class", "col1");
    			add_location(div4, file$4, 96, 3, 2434);
    			attr_dev(div5, "class", "submit-container svelte-1dfeqlc");
    			add_location(div5, file$4, 93, 2, 2298);
    			attr_dev(form, "action", "/");
    			add_location(form, file$4, 84, 1, 1893);
    			attr_dev(div6, "class", "uc-reset-form svelte-1dfeqlc");
    			add_location(div6, file$4, 82, 0, 1825);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div0, anchor);
    			if_block.m(div0, null);
    			insert_dev(target, t0, anchor);
    			insert_dev(target, div6, anchor);
    			mount_component(errormessage, div6, null);
    			append_dev(div6, t1);
    			append_dev(div6, form);
    			append_dev(form, div1);
    			append_dev(div1, label0);
    			append_dev(div1, t3);
    			append_dev(div1, input0);
    			set_input_value(input0, /*data*/ ctx[1].user_pass_1);
    			append_dev(form, t4);
    			append_dev(form, div2);
    			append_dev(div2, label1);
    			append_dev(div2, t6);
    			append_dev(div2, input1);
    			set_input_value(input1, /*data*/ ctx[1].user_pass_2);
    			append_dev(form, t7);
    			append_dev(form, div5);
    			append_dev(div5, input2);
    			set_input_value(input2, /*data*/ ctx[1].website);
    			append_dev(div5, t8);
    			append_dev(div5, div3);
    			append_dev(div5, t9);
    			append_dev(div5, div4);
    			append_dev(div4, button0);
    			append_dev(button0, t10);
    			append_dev(div4, t11);
    			append_dev(div4, button1);
    			current = true;

    			if (!mounted) {
    				dispose = [
    					listen_dev(input0, "input", /*input0_input_handler*/ ctx[8]),
    					listen_dev(input1, "input", /*input1_input_handler*/ ctx[9]),
    					listen_dev(input2, "input", /*input2_input_handler*/ ctx[10]),
    					listen_dev(button0, "click", prevent_default(/*verifyForm*/ ctx[4]), false, true, false),
    					listen_dev(button1, "click", prevent_default(/*cancelForm*/ ctx[5]), false, true, false)
    				];

    				mounted = true;
    			}
    		},
    		p: function update(ctx, [dirty]) {
    			if (current_block_type !== (current_block_type = select_block_type(ctx))) {
    				if_block.d(1);
    				if_block = current_block_type(ctx);

    				if (if_block) {
    					if_block.c();
    					if_block.m(div0, null);
    				}
    			}

    			if (dirty & /*data*/ 2 && input0.value !== /*data*/ ctx[1].user_pass_1) {
    				set_input_value(input0, /*data*/ ctx[1].user_pass_1);
    			}

    			if (dirty & /*data*/ 2 && input1.value !== /*data*/ ctx[1].user_pass_2) {
    				set_input_value(input1, /*data*/ ctx[1].user_pass_2);
    			}

    			if (dirty & /*data*/ 2) {
    				set_input_value(input2, /*data*/ ctx[1].website);
    			}

    			if ((!current || dirty & /*intent*/ 1) && t10_value !== (t10_value = (/*intent*/ ctx[0] === 'reset'
    			? 'Reset Password'
    			: 'Create New Password') + "")) set_data_dev(t10, t10_value);

    			if (!current || dirty & /*disabled*/ 4 && button0_class_value !== (button0_class_value = "submit " + (/*disabled*/ ctx[2] ? 'disabled' : 'enabled') + " svelte-1dfeqlc")) {
    				attr_dev(button0, "class", button0_class_value);
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(errormessage.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(errormessage.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div0);
    			if_block.d();
    			if (detaching) detach_dev(t0);
    			if (detaching) detach_dev(div6);
    			destroy_component(errormessage);
    			mounted = false;
    			run_all(dispose);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$4.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$4($$self, $$props, $$invalidate) {
    	let disabled;
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('ResetForm', slots, []);
    	const dispatch = createEventDispatcher();
    	let { intent } = $$props;
    	let { key } = $$props;
    	let { login } = $$props;
    	let errorMsg = false;

    	let data = {
    		user_pass_1: '',
    		user_pass_2: '',
    		website: '',
    		reset_nonce: window.svelteData.reset_nonce,
    		action: 'reset_form'
    	};

    	function submitForm(token) {
    		if (data.website) return;

    		if (!disabled) {
    			$$invalidate(1, data.token = token, data);
    			$$invalidate(1, data.intent = intent, data);
    			$$invalidate(1, data.key = key, data);
    			$$invalidate(1, data.login = login, data);

    			fetch(`/wp-json/uc/v1/authentication/reset`, {
    				method: 'POST',
    				headers: {
    					'Content-Type': 'application/json',
    					'X-WP-Nonce': window?.svelteData?.rest_nonce
    				},
    				body: JSON.stringify(data)
    			}).then(async res => {
    				const { data } = await res.json();

    				if (data.password_reset) {
    					window.location.replace(window.location.href.split('?')[0]);
    				}
    			});
    		}
    	}

    	function verifyForm() {
    		if (typeof window.grecaptcha !== 'undefined') {
    			window.grecaptcha.ready(function () {
    				window.grecaptcha.execute(window.ucOptions.recaptcha_site_key, { action: 'uc_reset' }).then(function (token) {
    					submitForm(token);
    				});
    			});
    		} else {
    			// reCaptcha is disabled
    			submitForm(null);
    		}
    	}

    	function cancelForm() {
    		dispatch('cancel');
    		window.location.replace(window.location.href.split('?')[0]);
    	}

    	const writable_props = ['intent', 'key', 'login'];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<ResetForm> was created with unknown prop '${key}'`);
    	});

    	function input0_input_handler() {
    		data.user_pass_1 = this.value;
    		$$invalidate(1, data);
    	}

    	function input1_input_handler() {
    		data.user_pass_2 = this.value;
    		$$invalidate(1, data);
    	}

    	function input2_input_handler() {
    		data.website = this.value;
    		$$invalidate(1, data);
    	}

    	$$self.$$set = $$props => {
    		if ('intent' in $$props) $$invalidate(0, intent = $$props.intent);
    		if ('key' in $$props) $$invalidate(6, key = $$props.key);
    		if ('login' in $$props) $$invalidate(7, login = $$props.login);
    	};

    	$$self.$capture_state = () => ({
    		createEventDispatcher,
    		ErrorMessage,
    		dispatch,
    		intent,
    		key,
    		login,
    		errorMsg,
    		data,
    		submitForm,
    		verifyForm,
    		cancelForm,
    		disabled
    	});

    	$$self.$inject_state = $$props => {
    		if ('intent' in $$props) $$invalidate(0, intent = $$props.intent);
    		if ('key' in $$props) $$invalidate(6, key = $$props.key);
    		if ('login' in $$props) $$invalidate(7, login = $$props.login);
    		if ('errorMsg' in $$props) $$invalidate(3, errorMsg = $$props.errorMsg);
    		if ('data' in $$props) $$invalidate(1, data = $$props.data);
    		if ('disabled' in $$props) $$invalidate(2, disabled = $$props.disabled);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	$$self.$$.update = () => {
    		if ($$self.$$.dirty & /*data*/ 2) {
    			 $$invalidate(2, disabled = !(data.user_pass_1 && data.user_pass_2 && data.user_pass_1 === data.user_pass_2));
    		}
    	};

    	return [
    		intent,
    		data,
    		disabled,
    		errorMsg,
    		verifyForm,
    		cancelForm,
    		key,
    		login,
    		input0_input_handler,
    		input1_input_handler,
    		input2_input_handler
    	];
    }

    class ResetForm extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$4, create_fragment$4, safe_not_equal, { intent: 0, key: 6, login: 7 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "ResetForm",
    			options,
    			id: create_fragment$4.name
    		});

    		const { ctx } = this.$$;
    		const props = options.props || {};

    		if (/*intent*/ ctx[0] === undefined && !('intent' in props)) {
    			console.warn("<ResetForm> was created without expected prop 'intent'");
    		}

    		if (/*key*/ ctx[6] === undefined && !('key' in props)) {
    			console.warn("<ResetForm> was created without expected prop 'key'");
    		}

    		if (/*login*/ ctx[7] === undefined && !('login' in props)) {
    			console.warn("<ResetForm> was created without expected prop 'login'");
    		}
    	}

    	get intent() {
    		throw new Error("<ResetForm>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set intent(value) {
    		throw new Error("<ResetForm>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get key() {
    		throw new Error("<ResetForm>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set key(value) {
    		throw new Error("<ResetForm>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get login() {
    		throw new Error("<ResetForm>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set login(value) {
    		throw new Error("<ResetForm>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src\AuthenticationForm\Disclaimer.svelte generated by Svelte v3.48.0 */

    const file$5 = "src\\AuthenticationForm\\Disclaimer.svelte";

    function create_fragment$5(ctx) {
    	let p;
    	let t0;
    	let a0;
    	let t2;
    	let a1;
    	let t4;

    	const block = {
    		c: function create() {
    			p = element("p");
    			t0 = text("This form is protected by reCAPTCHA and the Google\n\t");
    			a0 = element("a");
    			a0.textContent = "Privacy Policy";
    			t2 = text(" and\n\t");
    			a1 = element("a");
    			a1.textContent = "Terms of Service";
    			t4 = text(" apply.");
    			attr_dev(a0, "href", "https://policies.google.com/privacy");
    			add_location(a0, file$5, 10, 1, 210);
    			attr_dev(a1, "href", "https://policies.google.com/terms");
    			add_location(a1, file$5, 11, 1, 280);
    			attr_dev(p, "class", "g-recaptcha-disclaimer svelte-56q5c8");
    			add_location(p, file$5, 9, 0, 124);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, p, anchor);
    			append_dev(p, t0);
    			append_dev(p, a0);
    			append_dev(p, t2);
    			append_dev(p, a1);
    			append_dev(p, t4);
    		},
    		p: noop,
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(p);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$5.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$5($$self, $$props) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('Disclaimer', slots, []);
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<Disclaimer> was created with unknown prop '${key}'`);
    	});

    	return [];
    }

    class Disclaimer extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$5, create_fragment$5, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Disclaimer",
    			options,
    			id: create_fragment$5.name
    		});
    	}
    }

    /* src\AuthenticationForm\App.svelte generated by Svelte v3.48.0 */
    const file$6 = "src\\AuthenticationForm\\App.svelte";

    // (29:1) {#if window.ucOptions.enable_recaptcha}
    function create_if_block_5(ctx) {
    	let script;
    	let script_src_value;

    	const block = {
    		c: function create() {
    			script = element("script");
    			if (!src_url_equal(script.src, script_src_value = "https://www.google.com/recaptcha/api.js?render=" + window.ucOptions.recaptcha_site_key)) attr_dev(script, "src", script_src_value);
    			script.async = true;
    			script.defer = true;
    			add_location(script, file$6, 29, 2, 794);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, script, anchor);
    		},
    		p: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(script);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block_5.name,
    		type: "if",
    		source: "(29:1) {#if window.ucOptions.enable_recaptcha}",
    		ctx
    	});

    	return block;
    }

    // (39:1) {:else}
    function create_else_block$2(ctx) {
    	let div2;
    	let div0;
    	let h30;
    	let div0_class_value;
    	let t1;
    	let div1;
    	let h31;
    	let div1_class_value;
    	let t3;
    	let t4;
    	let if_block1_anchor;
    	let current;
    	let mounted;
    	let dispose;
    	let if_block0 = /*form*/ ctx[2] === 'login' && create_if_block_4(ctx);
    	let if_block1 = /*form*/ ctx[2] === 'signup' && create_if_block_3(ctx);

    	const block = {
    		c: function create() {
    			div2 = element("div");
    			div0 = element("div");
    			h30 = element("h3");
    			h30.textContent = "Login";
    			t1 = space();
    			div1 = element("div");
    			h31 = element("h3");
    			h31.textContent = "Signup";
    			t3 = space();
    			if (if_block0) if_block0.c();
    			t4 = space();
    			if (if_block1) if_block1.c();
    			if_block1_anchor = empty();
    			attr_dev(h30, "class", "svelte-1uytr5h");
    			add_location(h30, file$6, 41, 4, 1373);
    			attr_dev(div0, "class", div0_class_value = "form-login " + (/*form*/ ctx[2] === 'login' ? 'selected' : '') + " svelte-1uytr5h");
    			add_location(div0, file$6, 40, 3, 1272);
    			attr_dev(h31, "class", "svelte-1uytr5h");
    			add_location(h31, file$6, 44, 4, 1507);
    			attr_dev(div1, "class", div1_class_value = "form-signup " + (/*form*/ ctx[2] === 'signup' ? 'selected' : '') + " svelte-1uytr5h");
    			add_location(div1, file$6, 43, 3, 1403);
    			attr_dev(div2, "class", "form-selection svelte-1uytr5h");
    			add_location(div2, file$6, 39, 2, 1239);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div2, anchor);
    			append_dev(div2, div0);
    			append_dev(div0, h30);
    			append_dev(div2, t1);
    			append_dev(div2, div1);
    			append_dev(div1, h31);
    			insert_dev(target, t3, anchor);
    			if (if_block0) if_block0.m(target, anchor);
    			insert_dev(target, t4, anchor);
    			if (if_block1) if_block1.m(target, anchor);
    			insert_dev(target, if_block1_anchor, anchor);
    			current = true;

    			if (!mounted) {
    				dispose = [
    					listen_dev(div0, "click", /*click_handler*/ ctx[7], false, false, false),
    					listen_dev(div1, "click", /*click_handler_1*/ ctx[8], false, false, false)
    				];

    				mounted = true;
    			}
    		},
    		p: function update(ctx, dirty) {
    			if (!current || dirty & /*form*/ 4 && div0_class_value !== (div0_class_value = "form-login " + (/*form*/ ctx[2] === 'login' ? 'selected' : '') + " svelte-1uytr5h")) {
    				attr_dev(div0, "class", div0_class_value);
    			}

    			if (!current || dirty & /*form*/ 4 && div1_class_value !== (div1_class_value = "form-signup " + (/*form*/ ctx[2] === 'signup' ? 'selected' : '') + " svelte-1uytr5h")) {
    				attr_dev(div1, "class", div1_class_value);
    			}

    			if (/*form*/ ctx[2] === 'login') {
    				if (if_block0) {
    					if_block0.p(ctx, dirty);

    					if (dirty & /*form*/ 4) {
    						transition_in(if_block0, 1);
    					}
    				} else {
    					if_block0 = create_if_block_4(ctx);
    					if_block0.c();
    					transition_in(if_block0, 1);
    					if_block0.m(t4.parentNode, t4);
    				}
    			} else if (if_block0) {
    				group_outros();

    				transition_out(if_block0, 1, 1, () => {
    					if_block0 = null;
    				});

    				check_outros();
    			}

    			if (/*form*/ ctx[2] === 'signup') {
    				if (if_block1) {
    					if (dirty & /*form*/ 4) {
    						transition_in(if_block1, 1);
    					}
    				} else {
    					if_block1 = create_if_block_3(ctx);
    					if_block1.c();
    					transition_in(if_block1, 1);
    					if_block1.m(if_block1_anchor.parentNode, if_block1_anchor);
    				}
    			} else if (if_block1) {
    				group_outros();

    				transition_out(if_block1, 1, 1, () => {
    					if_block1 = null;
    				});

    				check_outros();
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(if_block0);
    			transition_in(if_block1);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(if_block0);
    			transition_out(if_block1);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div2);
    			if (detaching) detach_dev(t3);
    			if (if_block0) if_block0.d(detaching);
    			if (detaching) detach_dev(t4);
    			if (if_block1) if_block1.d(detaching);
    			if (detaching) detach_dev(if_block1_anchor);
    			mounted = false;
    			run_all(dispose);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_else_block$2.name,
    		type: "else",
    		source: "(39:1) {:else}",
    		ctx
    	});

    	return block;
    }

    // (37:49) 
    function create_if_block_2(ctx) {
    	let resetform;
    	let current;
    	const resetform_spread_levels = [/*data*/ ctx[3]];
    	let resetform_props = {};

    	for (let i = 0; i < resetform_spread_levels.length; i += 1) {
    		resetform_props = assign(resetform_props, resetform_spread_levels[i]);
    	}

    	resetform = new ResetForm({ props: resetform_props, $$inline: true });
    	resetform.$on("cancel", /*cancel_handler_1*/ ctx[5]);
    	resetform.$on("error", /*error_handler*/ ctx[6]);

    	const block = {
    		c: function create() {
    			create_component(resetform.$$.fragment);
    		},
    		m: function mount(target, anchor) {
    			mount_component(resetform, target, anchor);
    			current = true;
    		},
    		p: function update(ctx, dirty) {
    			const resetform_changes = (dirty & /*data*/ 8)
    			? get_spread_update(resetform_spread_levels, [get_spread_object(/*data*/ ctx[3])])
    			: {};

    			resetform.$set(resetform_changes);
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(resetform.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(resetform.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_component(resetform, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block_2.name,
    		type: "if",
    		source: "(37:49) ",
    		ctx
    	});

    	return block;
    }

    // (35:1) {#if forgotPassword}
    function create_if_block_1(ctx) {
    	let forgotform;
    	let current;
    	forgotform = new ForgotForm({ $$inline: true });
    	forgotform.$on("cancel", /*cancel_handler*/ ctx[4]);

    	const block = {
    		c: function create() {
    			create_component(forgotform.$$.fragment);
    		},
    		m: function mount(target, anchor) {
    			mount_component(forgotform, target, anchor);
    			current = true;
    		},
    		p: noop,
    		i: function intro(local) {
    			if (current) return;
    			transition_in(forgotform.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(forgotform.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_component(forgotform, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block_1.name,
    		type: "if",
    		source: "(35:1) {#if forgotPassword}",
    		ctx
    	});

    	return block;
    }

    // (48:2) {#if form === 'login'}
    function create_if_block_4(ctx) {
    	let loginform;
    	let current;
    	loginform = new LoginForm({ $$inline: true });
    	loginform.$on("forgot", /*forgot_handler*/ ctx[9]);

    	const block = {
    		c: function create() {
    			create_component(loginform.$$.fragment);
    		},
    		m: function mount(target, anchor) {
    			mount_component(loginform, target, anchor);
    			current = true;
    		},
    		p: noop,
    		i: function intro(local) {
    			if (current) return;
    			transition_in(loginform.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(loginform.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_component(loginform, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block_4.name,
    		type: "if",
    		source: "(48:2) {#if form === 'login'}",
    		ctx
    	});

    	return block;
    }

    // (51:2) {#if form === 'signup'}
    function create_if_block_3(ctx) {
    	let signupform;
    	let current;
    	signupform = new SignupForm({ $$inline: true });

    	const block = {
    		c: function create() {
    			create_component(signupform.$$.fragment);
    		},
    		m: function mount(target, anchor) {
    			mount_component(signupform, target, anchor);
    			current = true;
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(signupform.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(signupform.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_component(signupform, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block_3.name,
    		type: "if",
    		source: "(51:2) {#if form === 'signup'}",
    		ctx
    	});

    	return block;
    }

    // (56:0) {#if window.ucOptions.enable_recaptcha}
    function create_if_block$3(ctx) {
    	let disclaimer;
    	let current;
    	disclaimer = new Disclaimer({ $$inline: true });

    	const block = {
    		c: function create() {
    			create_component(disclaimer.$$.fragment);
    		},
    		m: function mount(target, anchor) {
    			mount_component(disclaimer, target, anchor);
    			current = true;
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(disclaimer.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(disclaimer.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_component(disclaimer, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$3.name,
    		type: "if",
    		source: "(56:0) {#if window.ucOptions.enable_recaptcha}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$6(ctx) {
    	let if_block0_anchor;
    	let t0;
    	let div;
    	let current_block_type_index;
    	let if_block1;
    	let t1;
    	let if_block2_anchor;
    	let current;
    	let if_block0 = window.ucOptions.enable_recaptcha && create_if_block_5(ctx);
    	const if_block_creators = [create_if_block_1, create_if_block_2, create_else_block$2];
    	const if_blocks = [];

    	function select_block_type(ctx, dirty) {
    		if (/*forgotPassword*/ ctx[1]) return 0;
    		if (/*data*/ ctx[3].intent && 'reset' === /*data*/ ctx[3].intent) return 1;
    		return 2;
    	}

    	current_block_type_index = select_block_type(ctx);
    	if_block1 = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);
    	let if_block2 = window.ucOptions.enable_recaptcha && create_if_block$3(ctx);

    	const block = {
    		c: function create() {
    			if (if_block0) if_block0.c();
    			if_block0_anchor = empty();
    			t0 = space();
    			div = element("div");
    			if_block1.c();
    			t1 = space();
    			if (if_block2) if_block2.c();
    			if_block2_anchor = empty();
    			attr_dev(div, "class", "user-authentication svelte-1uytr5h");
    			add_location(div, file$6, 33, 0, 942);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			if (if_block0) if_block0.m(document.head, null);
    			append_dev(document.head, if_block0_anchor);
    			insert_dev(target, t0, anchor);
    			insert_dev(target, div, anchor);
    			if_blocks[current_block_type_index].m(div, null);
    			insert_dev(target, t1, anchor);
    			if (if_block2) if_block2.m(target, anchor);
    			insert_dev(target, if_block2_anchor, anchor);
    			current = true;
    		},
    		p: function update(ctx, [dirty]) {
    			if (window.ucOptions.enable_recaptcha) if_block0.p(ctx, dirty);
    			let previous_block_index = current_block_type_index;
    			current_block_type_index = select_block_type(ctx);

    			if (current_block_type_index === previous_block_index) {
    				if_blocks[current_block_type_index].p(ctx, dirty);
    			} else {
    				group_outros();

    				transition_out(if_blocks[previous_block_index], 1, 1, () => {
    					if_blocks[previous_block_index] = null;
    				});

    				check_outros();
    				if_block1 = if_blocks[current_block_type_index];

    				if (!if_block1) {
    					if_block1 = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);
    					if_block1.c();
    				} else {
    					if_block1.p(ctx, dirty);
    				}

    				transition_in(if_block1, 1);
    				if_block1.m(div, null);
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(if_block1);
    			transition_in(if_block2);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(if_block1);
    			transition_out(if_block2);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (if_block0) if_block0.d(detaching);
    			detach_dev(if_block0_anchor);
    			if (detaching) detach_dev(t0);
    			if (detaching) detach_dev(div);
    			if_blocks[current_block_type_index].d();
    			if (detaching) detach_dev(t1);
    			if (if_block2) if_block2.d(detaching);
    			if (detaching) detach_dev(if_block2_anchor);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$6.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$6($$self, $$props, $$invalidate) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('App', slots, []);
    	let error = false;
    	let forgotPassword = false;
    	let form = 'login';
    	const data = { intent: null, key: null, login: null };

    	onMount(() => {
    		const queryString = window.location.search;
    		const urlParams = new URLSearchParams(queryString);
    		$$invalidate(3, data.intent = urlParams.get('intent'), data);
    		$$invalidate(3, data.key = urlParams.get('key'), data);
    		$$invalidate(3, data.login = urlParams.get('login'), data);
    	});

    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<App> was created with unknown prop '${key}'`);
    	});

    	const cancel_handler = () => $$invalidate(1, forgotPassword = false);
    	const cancel_handler_1 = () => $$invalidate(3, data.intent = false, data);
    	const error_handler = e => $$invalidate(0, error = e.detail.data.error);
    	const click_handler = () => $$invalidate(2, form = 'login');
    	const click_handler_1 = () => $$invalidate(2, form = 'signup');
    	const forgot_handler = () => $$invalidate(1, forgotPassword = true);

    	$$self.$capture_state = () => ({
    		onMount,
    		SignupForm,
    		LoginForm,
    		ForgotForm,
    		ResetForm,
    		ErrorMessage,
    		Disclaimer,
    		error,
    		forgotPassword,
    		form,
    		data
    	});

    	$$self.$inject_state = $$props => {
    		if ('error' in $$props) $$invalidate(0, error = $$props.error);
    		if ('forgotPassword' in $$props) $$invalidate(1, forgotPassword = $$props.forgotPassword);
    		if ('form' in $$props) $$invalidate(2, form = $$props.form);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [
    		error,
    		forgotPassword,
    		form,
    		data,
    		cancel_handler,
    		cancel_handler_1,
    		error_handler,
    		click_handler,
    		click_handler_1,
    		forgot_handler
    	];
    }

    class App extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$6, create_fragment$6, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "App",
    			options,
    			id: create_fragment$6.name
    		});
    	}
    }

    let anchor = document.getElementById('collections-authentication');

    if ( anchor ) {
      const userAuthentication = new App({
        target: anchor,
      });
    }

    return userAuthentication;

}());
//# sourceMappingURL=user-authentication.js.map
