import App from './AuthenticationForm/App.svelte';

let anchor = document.getElementById('collections-authentication');

if ( anchor ) {
  const userAuthentication = new App({
    target: anchor,
  });
}

export default userAuthentication;
