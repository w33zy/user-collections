import App from './AddButton/App.svelte';

let anchor = document.getElementById('user-collections-add');

if ( anchor ) {
	const addButton = new App({
		target: anchor,
	});
}

export default addButton;
