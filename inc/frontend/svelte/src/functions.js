import { storage, collections, label, saved, lsName } from './store'
import { get } from 'svelte/store'

/**
 * Generates a random string
 *
 * @param {number} n
 * @returns {string}
 */
export const random = function(n) {
  return [...Array(n)].map(() => Math.random().toString(36)[2]).join('')
};

/**
 * Checks to see if the post ID is in a specific collection
 *
 * @param {object} collection
 * @param {string} subcollectionID
 * @param {number} postID
 * @returns {boolean}
 */
export function isPostInCollection( collection, subcollectionID, postID ) {

  if (collection.hasOwnProperty(subcollectionID)) {
    for (const post of collection[subcollectionID]) {
      if (post && post.item_id === postID) {
        return true
      }
    }
  }
  
  return false
}

/**
 * Checks to see if the post ID is in any collection
 *
 * @param {object} collections
 * @param {number} postID
 * @returns {boolean}
 */
export function postInAnyCollection( collections, postID ) {

  for ( const key of Object.keys( collections.titles ) ) {
    if ( collections.hasOwnProperty( key ) ) {
      for ( const post of collections[key] ) {
        if ( post && post.item_id === postID ) {
          return true
        }
      }
    }
  }
  
  return false
}

/**
 * Sets the label and checkbox
 *
 * @param {object} collections
 * @param {number} itemID
 */
export function updateLabelAndCheckbox( collections, itemID ) {
  if ( postInAnyCollection( collections, itemID ) ) {
    return {
      text: 'Saved to Collection',
      saved: true
    }
  }

  return {
    text: 'Save to Collection',
    saved: false
  }
}

/**
 * Updates a buttons label based on if post ID
 * is in collection
 *
 * @param {object} collection
 * @param {number} postID
 */
export function updateLabel(collection, postID) {
  if (postInAnyCollection(collection, postID)) {
    label.update(_label => 'Saved to Collection');
    saved.update(_saved => true);
  } else {
    label.update(_update => 'Add to Collection');
    saved.update(_saved => false);
  }
};

/**
 * Get the current post's metadata
 *
 * @param {number} postID
 * @returns {{}|{datePublished: string, image: string, dateCollected: string, name: string, description: string, postID: number, url: string}}
 */
export function getCurrentPostMeta(postID) {
  let item;
  let allScripts = Array.from(document.querySelectorAll('script[type="application/ld+json"]'));

  for (let i = 0; i < allScripts.length; ++i) {
    let data = allScripts[i].innerText ? JSON.parse(allScripts[i].innerText) : {};

    if (data.hasOwnProperty('@type') && data['@type'] === 'Recipe') {
      item = filterInput(data, postID);
      return item;
    }

    if (data.hasOwnProperty('@type') && data['@type'] === 'Review') {
      item = filterInput(data, postID);
      return item;
    }
  }

  item = {
    name: getMetaContent('og:title') || document.title,
    postID: postID,
    url: getMetaContent('og:url') || window.location.href,
    description: getMetaContent('og:description'),
    datePublished: getMetaContent('article:published_time'),
    image: getMetaContent('og:image'),
    dateCollected: (new Date()).toISOString(),
  };

  return item;
};

/**
 * Removes a post from a specific collection
 *
 * @param {number} postID
 * @param {string} collectionID
 */
export function removeFromCollection( collections, postID, collectionID ) {

  collections.update( ( collections ) => {
    collections[ collectionID ] = collections[ collectionID ].filter( ( post ) => post.postID !== postID )

    return collections
  })
}

export function getMetaContent( name ) {
  let value = '';
  const item = document.head.querySelector("[property='" + name + "']");

  if ( item ) {
    value = item.content
  }

  return value
}

export function removePost( postID, collectionID, userID ) {

  jQuery.ajax({
    method: 'POST',
    url: window.svelteData.ajax_url,
    data: {
      action: 'save_post_to_collection',
      security_nonce: window.svelteData.nonce,
      post_ID: postID,
      user_ID: userID,
      collection_ID: collectionID,
      operation: 'remove',
    },
  })
  .done(function(res) {
    collections.update((_collection) => {
      _collection[collectionID] = _collection[collectionID].filter((post) => post.postID !== postID);
      _collection.hash = res.data.hash;
      return _collection;
    });
    storage.set(lsName, get(collections));
  })
  .fail(function(res) {
      console.log(res.data);
  });
};

export function isEmpty( val ) {
  if ( !val ) {
    return true
  }

  if ( Array.isArray( val ) ) {
    return val.length === 0
  }

  return Object.getOwnPropertyNames( val ).length === 0
}

export function getObjKeyByValue( object, value ) {
  return Object.keys( object ).find( key => object[ key ] === value )
}

export function filterObj(obj, predicate) {
  return Object.keys(obj).filter(function (key) {
    return predicate(obj[key]);
  }).reduce(function (res, key) {
    return res[key] = obj[key], res;
  }, {});
};

/**
 * Returns a count of all posts in a collection
 *
 * @param {object} collections
 * @returns {number}
 */
export function countPosts( collections ) {
  let count = 0

  if ( !collections.hasOwnProperty( 'titles' ) ) return 0

  for ( let key of Object.keys( collections.titles ) ) {
    count += collections[ key ].length
  }

  return count
}

/**
 * @param {any} input
 * @param {number} postID
 */
function filterInput(input, postID) {
  const output = {};
  const keys = ['name', 'description', 'datePublished', 'dateModified', 'image',
    'aggregateRating', 'recipeCategory', 'recipeCuisine', 'itemReviewed', 'thumbnailUrl'];

  for (let key of keys) {
    output[key] = input[key];
  }

  if (typeof output.name === 'undefined') {
    output.name = output.itemReviewed.name || '';
  }

  if (typeof output.image === 'undefined') {
    output.image = output.thumbnailUrl || '';
  }

  output.postID = postID;
  output.url    = window.location.href;
  output.dateCollected = (new Date()).toISOString();

  return output;
}

/**
 * String all HTML tags
 *
 * @param html
 * @returns {string}
 */
function stripTags(html) {
  const doc = new DOMParser().parseFromString(html, 'text/html');
  return doc.body.textContent || '';
}

function flattenObject(obj) {
  let toReturn = {};

  for (let i in obj) {
    if (!obj.hasOwnProperty(i)) continue;

    if ((typeof obj[i]) == 'object' && obj[i] !== null) {
      let flatObject = flattenObject(obj[i]);
      for (let x in flatObject) {
        if (!flatObject.hasOwnProperty(x)) continue;
        toReturn[i + '.' + x] = flatObject[x];
      }
    } else {
      toReturn[i] = obj[i];
    }
  }
  return toReturn;
}

/**
 * Strips the '\' from strings
 *
 * @param {string} str
 * @returns {string}
 */
export const stripSlashes = function (str) {
  return (str + '')
  .replace(/\\(.?)/g, function (s, n1) {
    switch (n1) {
      case '\\':
        return '\\';
      case '0':
        return '\u0000';
      case '':
        return '';
      default:
        return n1
    }
  })
};

export function setCookie(name,value,days) {
	let expires = ''
	if (days) {
		const date = new Date()
		date.setTime(date.getTime() + (days*24*60*60*1000))
		expires = "; expires=" + date.toUTCString()
	}
	document.cookie = name + "=" + (value || "")  + expires + "; path=/"
}

export function getCookie(name) {
	const nameEQ = name + '='
	const ca = document.cookie.split( ';' )
	for( let i=0; i < ca.length; i++) {
		let c = ca[ i ]
		while (c.charAt(0)===' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length,c.length)
	}
	return null;
}

export function eraseCookie(name) {
	document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;'
}
