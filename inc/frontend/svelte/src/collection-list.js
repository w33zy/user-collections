import App from './CollectionList/App.svelte';

let anchor = document.getElementById('collections-container');

if ( anchor ) {
	const collectionList = new App({
		target: anchor,
	});
}

export default collectionList;
