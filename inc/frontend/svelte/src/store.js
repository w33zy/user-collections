import { writable } from 'svelte/store'
import Stoor from 'stoor';
import localforage from 'localforage';

export const storage    = new Stoor({namespace: 'wzy'});
export const localForage = localforage.createInstance({
    name: 'User Collections',
    storeName: `user_collection_${window.svelteData.user_ID || 0}`
});

export const collections = writable( { layout: 'grid', titles: {} } )
export const label = writable( 'Add to Collection' )
export const saved = writable(false)
export const hearts = writable(false)
export const manage = writable(false)
export const sortOrder = writable('collected_date')
export const count = writable(0)
export const errMsg = writable(false)
export const lsName = `user_collection_${window.svelteData.user_ID || 0}`;
