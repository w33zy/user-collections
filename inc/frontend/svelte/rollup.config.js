import svelte from 'rollup-plugin-svelte';
import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import {terser} from 'rollup-plugin-terser';

const production = !process.env.ROLLUP_WATCH;

export default [
    {
        input: 'src/add-button.js',
        output: {
            sourcemap: true,
            format: 'iife',
            name: 'addButton',
            file: 'dist/add-button.js',
        },
        plugins: [
            svelte({
                dev: !production,
                css: css => {
                    css.write('add-button.css');
                },
            }),
            resolve({
                browser: true,
                dedupe: importee => importee === 'svelte' || importee.startsWith('svelte/'),
            }),
            commonjs(),
            production && terser(),
        ],
        watch: {
            clearScreen: false,
        },
    },

    {
        input: 'src/collection-list.js',
        output: {
            sourcemap: true,
            format: 'iife',
            name: 'collectionList',
            file: 'dist/collection-list.js',
        },
        plugins: [
            svelte({
                dev: !production,
                css: css => {
                    css.write('collection-list.css');
                },
            }),
            resolve({
                browser: true,
                dedupe: importee => importee === 'svelte' || importee.startsWith('svelte/'),
            }),
            commonjs(
                {
                    namedExports: {
                        'node_modules/lodash/lodash.js': [
                            'unionBy',
                            'union',
                            'reduce',
                            'find',
                            'forEach',
                            'includes',
                            'deburr',
                            'trim'
                        ]
                    }
                }
            ),
            production && terser(),
        ],
        watch: {
            clearScreen: false,
        },
    },

    {
        input: 'src/user-authentication.js',
        output: {
            sourcemap: true,
            format: 'iife',
            name: 'userAuthentication',
            file: 'dist/user-authentication.js',
        },
        plugins: [
            svelte({
                dev: !production,
                css: css => {
                    css.write('user-authentication.css');
                },
            }),
            resolve({
                browser: true,
                dedupe: importee => importee === 'svelte' || importee.startsWith('svelte/'),
            }),
            commonjs(),
            production && terser(),
        ],
        watch: {
            clearScreen: false,
        },
    }

];
