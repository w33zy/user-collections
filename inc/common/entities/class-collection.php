<?php

namespace User_Collections\Inc\Common\Entities;

/**
 * Creates our `collection` entity
 *
 * @link       http://example.com
 *
 * @since      1.0.0
 *
 * @author    Your Name or Your Company
 */
class Collection {

	/**
	 * The ID of this collection
	 *
	 * @since 1.0.0
	 *
	 * @var  string $ID
	 */
	public $ID;

	/**
	 * The titles of each user sub-collection
	 *
	 * @since 1.0.0
	 *
	 * @var  array $titles
	 */
	public $titles;

	/**
	 * A count of the number of stored posts
	 * in each sub-collection
	 *
	 * @since 1.0.0
	 *
	 * @var  int $count
	 */
	public $count;

	/**
	 * The hash of this collection
	 *
	 * @since 1.0.0
	 *
	 * @var  string $hash MD5 hash
	 */
	public $hash;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {

		$this->ID        = substr( md5( mt_rand() ), 0, 16 );
		$this->titles    = array();
		$this->count     = 0;
		$this->hash      = '';
	}
}
