<?php

namespace User_Collections\Inc\Common\Entities;

/**
 * Stores information on user collection per post
 *
 * @since      1.0.0
 */
class Metadata {

	/**
	 * Array of user's that have collected this post
	 *
	 * @since 1.0.0
	 *
	 * @var  array $collected_by An array of user IDs that have added the post to a collection
	 */
	public $collected_by = array();

	/**
	 * A count of the number of times post has been added to a collection
	 *
	 * @since 1.0.0
	 *
	 * @var  int $collection_count
	 */
	public $collection_count = 0;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {}
}
