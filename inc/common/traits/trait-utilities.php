<?php

namespace User_Collections\Inc\Common\Traits;

/**
 * Trait Utilities.
 *
 * @since 1.0.0
 *
 * @package User_Collections\Inc\Common\Traits
 */
trait Utilities {

	/**
	 * Array map for multidimensional arrays
	 *
	 * A utility function to  recursively apply a function to each value of a
	 * multidimensional array.
	 *
	 * @param string $function Name of global function.
	 * @param array  $arr      Array to work on.
	 *
	 * @return array
	 */
	public function array_walker( $function, array $arr ) {

		$result = array();
		foreach ( $arr as $key => $val ) {
			$result[ $key ] = ( is_array( $val ) ? $this->array_walker( $function, $val ) : $function( $val ) );
		}

		return $result;
	}

	/**
	 * Prints the HTML data attribute
	 *
	 * @param string $attribute
	 * @param int|string $data
	 *
	 * @return string
	 */
	public function data_attribute( $attribute, $data ) {

		if ( $attribute && $data ) {
			return sprintf( 'data-%s="%s"', $attribute, $data );
		}

		return '';
	}

	/**
	 * Creates an array of published pages with page slug as key
	 * and page title as value
	 *
	 * @since 1.0.0
	 *
	 * @return array
	 */
	public function list_wp_pages() {
		$result = array();
		$pages  = get_pages(
			array(
				'sort_order' => 'DESC',
				'sort_column' => 'post_modified_gmt',
			)
		);

		if ( $pages ) {
			foreach ( $pages as $page ) {
				$result[ $page->post_name ] = $page->post_title;
			}
		}

		return $result;
	}

	/**
	 * Check to see if post is in at least 1 of the user's collections
	 *
	 * @since 1.0.0
	 *
	 * @param int $post_ID
	 * @param int $user_ID
	 *
	 * @return bool
	 */
	public function post_in_user_collection( $post_ID, $user_ID ) {

		$collection = get_user_meta( $user_ID, 'uc_collection', true );

		foreach ( $this->filter_collection( $collection ) as $subcollection ) {
			if ( is_array( $subcollection ) ) {
				foreach ( $subcollection as $item ) {
					if ( $item['postID'] === $post_ID ) {
						return true;
					}
				}
			}
		}

		return false;
	}

	/**
	 * Counts the amount of times a post appears
	 * in a user collection
	 *
	 * @since 1.0.0
	 *
	 * @param int $post_ID
	 * @param int $user_ID
	 *
	 * @return int
	 */
	public function post_in_collection_count( $post_ID, $user_ID ) {

		$count      = 0;
		$collection = get_user_meta( $user_ID, 'uc_collection', true );

		foreach ( $this->filter_collection( $collection ) as $subcollection ) {
			if ( is_array( $subcollection ) ) {
				foreach ( $subcollection as $item ) {
					if ( $item['postID'] === $post_ID ) {
						++ $count;
					}
				}
			}
		}

		return $count;
	}

	/**
	 * Filters the collection object to return only our user's collection
	 *
	 * @since 1.0.0
	 *
	 * @param object $collection
	 *
	 * @return array
	 */
	public function filter_collection( $collection ) {

		if ( is_object( $collection ) ) {

			return array_filter( get_object_vars( $collection ), static function ( $item ) {
				return ! in_array( $item, array( 'titles', 'count', 'ID'), true );
			}, ARRAY_FILTER_USE_KEY );
		}

		return array();
	}

	public function get_user_collection( $user_ID ) {

		return get_user_meta( $user_ID, 'uc_collection', true );
	}

	public function process_post( $post_arr ) {

		$post = null;

		if ( is_array( $post_arr ) && array_key_exists( '@type', $post_arr ) && 'Review' === $post_arr['@type'] ) {
			$keys = [ 'postID', 'description', 'itemReviewed', 'url', 'reviewRating' ];

			foreach ( $keys as $key ) {
				if ( 'itemReviewed' === $key ) {
					$post[] = $post_arr[$key]['name'];
				}
				$post[] = $post_arr[ $key ];
			}
		}
	}

}
