<?php

namespace User_Collections\Inc\Common\Tasks;

use User_Collections\Inc\Common\Abstracts\Async_Task;

class Update_Post_Meta extends Async_Task {

	/**
	 * Action to use to trigger this task
	 *
	 * @var string
	 */
	protected $action = 'user_collection_update';

	/**
	 * Prepare any data to be passed to the asynchronous postback
	 *
	 * The array this function receives will be a numerically keyed array from
	 * func_get_args(). It is expected that you will return an associative array
	 * so that the $_POST values used in the asynchronous call will make sense.
	 *
	 * The array you send back may or may not have anything to do with the data
	 * passed into this method. It all depends on the implementation details and
	 * what data is needed in the asynchronous postback.
	 *
	 * Do not set values for 'action' or '_nonce', as those will get overwritten
	 * later in launch().
	 *
	 * @param array $data The raw data received by the launch method
	 *
	 * @throws \Exception If the postback should not occur for any reason
	 *
	 * @return array The prepared data
	 */
	protected function prepare_data( $data ) {
		return array(
			'post_id' => $data[0],
			'user_id' => $data[1],
			'task'    => $data[2],
		);
	}

	/**
	 * Run the do_action function for the asynchronous postback.
	 *
	 * This method needs to fetch and sanitize any and all data from the $_POST
	 * superglobal and provide them to the do_action call.
	 *
	 * The action should be constructed as "wp_async_task_$this->action"
	 */
	protected function run_action() {
		if ( ! empty( $_POST[ 'post_id' ] ) ) {
			do_action( "wp_async_$this->action", (int) $_POST[ 'post_id' ], (int) $_POST[ 'user_id' ], $_POST[ 'task' ] );
		}
	}
}
