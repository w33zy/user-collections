<?php

namespace User_Collections\Inc\Core;

use User_Collections as NS;
use User_Collections\Inc\Common\Entities\Metadata;
use User_Collections\Inc\Common\Tasks\Update_Post_Meta;

/**
 *
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    User_Collections
 * @subpackage User_Collections/inc
 */

/**
 * The get_option functionality of the plugin.
 *
 *
 * @package    User_Collections
 * @subpackage User_Collections/inc
 * @author     Your Name <email@example.com>
 */


class Tasks {

	use NS\Inc\Common\Traits\Utilities;

	/**
	 * The ID of this plugin.
	 *
	 * @since 1.0.0
	 *
	 * @access private
	 * @var    string $plugin_name The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since  1.0.0
	 *
	 * @access private
	 * @var    string $version The current version of this plugin.
	 */
	private $version;

	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 *
	 * @param string $plugin_name The name of the plugin.
	 * @param string $version The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version     = $version;
	}

	/**
	 * Register our async tasks
	 *
	 * @since 1.0.0
	 *
	 * @throws \Exception
	 *
	 * @return void
	 */
	public function register_tasks() {

		new Update_Post_Meta();
	}

	/**
	 * Update the post meta containing a list of user IDs
	 * that hve added the post to a collection
	 *
	 * @since 1.0.0
	 *
	 * @param int    $post_id  The post ID we are updating
	 * @param int    $user_id  The user ID
	 * @param string $task     The task we are performing
	 *
	 * @return void
	 */
	public function process_update_post_meta( $post_id, $user_id, $task ) {

		$meta = get_post_meta( $post_id, 'uc_post_meta', true ) ?: new Metadata();

		if ( ( 'add' === $task ) && ! in_array( $user_id, $meta->collected_by, true ) ) {
			$meta->collected_by[] = $user_id;
		}

		// If post is in at least 1 of the user's collections, don't remove their ID
		if ( 'remove' === $task && ! $this->post_in_user_collection( $post_id, $user_id ) && in_array( $user_id, $meta->collected_by, true ) ) {
			$meta->collected_by = array_filter( $meta->collected_by, static function ( $i ) use ( $user_id ) {
				return $i !== $user_id;
			} );
		}

		if ( 'add' === $task ) {
			++ $meta->collection_count;
		}

		if ( ( 'remove' === $task ) && $meta->collection_count > 0 ) {
			-- $meta->collection_count;
		}

		if ( 'anonymous' === $task ) {
			++ $meta->collection_count;
		}

		update_post_meta( $post_id, 'uc_post_meta', $meta );
    }

	/**
	 * Update the post meta containing a list of user IDs
	 * that hve added the post to a collection
	 *
	 * @since 1.0.0
	 *
	 * @param int    $post_id  The post ID we are updating
	 * @param int    $user_id  The user ID
	 * @param string $task     The task we are performing
	 *
	 * @return void
	 */
	public function process_collection_delete_post_meta( $post_id, $user_id, $task ) {
		// TODO: Deleting an entire collection on the frontend should also update post meta counts
	}

}
