<?php

namespace User_Collections\Inc\Core;

/**
 * Fired during plugin activation
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @author     Your Name or Your Company
 **/
class Activator {

	/**
	 * Our custom role capabilities.
	 *
	 * @var array
	 */
	public static $collector_roles = array(
		'read'              => true,
		'create_collection' => true,
		'create_item'       => true,
		'edit_collection'   => true,
		'edit_item'         => true,
		'delete_collection' => true,
		'delete_item'       => true,
	);

	/**
	 * Short Description.
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate(): void {

		$min_php = '7.1';

		// Check PHP Version and deactivate & die if it doesn't meet minimum requirements.
		if ( version_compare( PHP_VERSION, $min_php, '<' ) ) {
			deactivate_plugins( plugin_basename( __FILE__ ) );
			wp_die( 'This plugin requires a minimum PHP Version of ' . $min_php );
		}

		self::setup_custom_tables();
		self::setup_custom_roles();

	}

	/**
	 * Add our custom roles on plugin activation.
	 *
	 * @return void
	 */
	public static function setup_custom_roles(): void {
		add_role(
			'collector',
			__( 'Collector', 'user-collections' ),
			self::$collector_roles
		);
	}

	/**
	 * Create our custom tables in the WordPress database
	 *
	 * @since  1.0.0
	 *
	 * @return void
	 */
	private static function setup_custom_tables(): void {
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

		global $wpdb;
		$wpdb->hide_errors();

		$collate = '';

		if ( $wpdb->has_cap( 'collation' ) ) {
			$collate = $wpdb->get_charset_collate();
		}

		/* Create collections table */
		$collections_table = $wpdb->prefix . 'uc_collections';
		if ( $wpdb->get_var( "SHOW TABLES LIKE '$collections_table'" ) !== $collections_table ) {
			$default_title = __( 'Favorites', 'user-collections' );
			$collections = "CREATE TABLE $collections_table (
                        ID INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, 
                        collection_key VARCHAR(255) NOT NULL, 
                        collection_title VARCHAR(255) DEFAULT '$default_title' NOT NULL, 
    					collection_count INT(11) UNSIGNED DEFAULT 0 NOT NULL, 
                        collection_public TINYINT(1) DEFAULT 1,
    					collection_user INT(11) UNSIGNED DEFAULT 0 NOT NULL, 
    					collection_created datetime DEFAULT CURRENT_TIMESTAMP NOT NULL, 
    					collection_updated datetime DEFAULT CURRENT_TIMESTAMP NOT NULL, 
                        PRIMARY KEY  (ID) 
                        ) $collate;";
			dbDelta( $collections );
		}

		/* Create items table */
		$items_table = $wpdb->prefix . 'uc_items';
		if ( $wpdb->get_var( "SHOW TABLES LIKE '$items_table'" ) !== $items_table ) {
			$items = "CREATE TABLE $items_table (
                        ID INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, 
                        item_id INT(11) NOT NULL, 
                        item_title VARCHAR(255), 
                        item_description TEXT, 
                        item_url VARCHAR(255) NOT NULL, 
                        item_thumbnail VARCHAR(255), 
                        item_author VARCHAR(255), 
                        item_category VARCHAR(255), 
                        item_published datetime DEFAULT '0000-00-00 00:00:00', 
                        item_modified datetime DEFAULT '0000-00-00 00:00:00', 
                        item_collected datetime DEFAULT CURRENT_TIMESTAMP NOT NULL, 
                        item_public TINYINT(1) DEFAULT 1,
                        item_key VARCHAR(255) NOT NULL, 
                        collection_id INT(11) UNSIGNED NOT NULL,
                        FOREIGN KEY (collection_id) REFERENCES `$collections_table`(`ID`) ON DELETE CASCADE, 
                        PRIMARY KEY  (ID)                          
                        ) $collate;";
			dbDelta( $items );
		}

	}

}
