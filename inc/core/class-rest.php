<?php

namespace User_Collections\Inc\Core;

/**
 *
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    User_Collections
 * @subpackage User_Collections/inc
 */

/**
 * The get_option functionality of the plugin.
 *
 *
 * @package    User_Collections
 * @subpackage User_Collections/inc
 * @author     Your Name <email@example.com>
 */


class Rest {

	/**
	 * The ID of this plugin.
	 *
	 * @since 1.0.0
	 *
	 * @access private
	 * @var    string $plugin_name The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since  1.0.0
	 *
	 * @access private
	 * @var    string $version The current version of this plugin.
	 */
	private $version;

	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 *
	 * @param string $plugin_name The name of the plugin.
	 * @param string $version The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version     = $version;
	}

	public function register_rest_routes() {

		register_rest_route(
			'user-collections/v1',
			'/collector/(?P<id>\d+)/(?P<collection>[a-zA-Z0-9,]+)',
			array(
				'methods' => 'GET',
				'callback' => array( $this, 'get_user_collection' ),
				'args' => array(
					'id' => array(
						'validate_callback' => function($param, $request, $key) {
							return is_numeric( $param );
						}
					),
				),
				'permission_callback' => function() {
					return true; //current_user_can( 'edit_others_posts' );
				}
			)
		);
	}

	/**
	 * @param \WP_REST_Request $request
	 *
	 * @return array
	 */
	public function get_user_collection( \WP_REST_Request $request ) {

		$params      = $request->get_params();
		$user_ID     = (int) $params['id'];
		$collections = explode( ',', $params['collection'] );
		$results     = [];

		$current_user = get_current_user_id();
		$user_logged_in = is_user_logged_in();

		foreach	( $collections as $collection) {
			$result    = get_user_meta( $user_ID, 'uc_collection', true );
			$results[] = $result;
		}

		return $results;
	}
}
