<?php

namespace User_Collections\Inc\Core;

/**
 *
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    User_Collections
 * @subpackage User_Collections/includes
 */

/**
 * The get_option functionality of the plugin.
 *
 *
 * @package    User_Collections
 * @subpackage User_Collections/includes
 * @author     Your Name <email@example.com>
 */


class Options {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	protected static $plugin_name = 'user_collections';

	/**
	 * Get an option
	 *
	 * Looks to see if the specified setting exists, returns default if not.
	 *
	 * @param      $key
	 * @param bool $default
	 *
	 * @return    mixed    $value    Value saved / $default if key if not exist
	 * @since    1.0.0
	 */
	public static function get_option( $key, $default = false ) {

		if ( empty( $key ) ) {
			return $default;
		}

		$plugin_options = get_option( self::$plugin_name . '_settings', array() );
		$value          = isset( $plugin_options[ $key ] ) ? $plugin_options[ $key ] : $default;

		return $value;
	}

	/**
	 * Update an option
	 *
	 * Updates the specified option.
	 * This is for developers to update options outside the settings page.
	 *
	 * WARNING: Hooks and filters will be triggered!!
	 *
	 * @TODO  : Trigger hooks & filters, pull requests welcomed
	 *
	 * @param $key
	 * @param $value
	 *
	 * @return true if the option was saved or false if not
	 * @since 1.0.0
	 */
	public static function update_option( $key, $value ) {

		if ( empty( $key ) ) {
			return false;
		}

		// Load the options
		$plugin_options = get_option( self::$plugin_name . '_settings', array() );

		// Update the specified value in the array
		$plugin_options[ $key ] = $value;

		// Save the options back to the DB
		return update_option( self::$plugin_name . '_settings', $plugin_options );
	}

	/**
	 * Delete an option
	 *
	 * Deletes the specified option.
	 * This is for developers to delete options outside the settings page.
	 *
	 * WARNING: Hooks and filters will be triggered!!
	 *
	 * @TODO  : Trigger hooks & filters, pull requests welcomed
	 *
	 * @param $key
	 *
	 * @return true if the option was deleted or false if not
	 * @since 1.0.0
	 */
	public static function delete_option( $key ) {

		if ( empty( $key ) ) {
			return false;
		}

		// Load the options
		$plugin_options = get_option( self::$plugin_name . '_settings', array() );

		// Delete the specified key
		unset( $plugin_options[$key] );

		// Save the options back to the DB
		return update_option( self::$plugin_name . '_settings', $plugin_options );
	}
}
