<?php

namespace User_Collections\Inc\Core;

/**
 * Fired during plugin deactivation
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @author     wzyedia
 **/
class Deactivator {

	/**
	 * Methods to run during plugin deactivation.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate(): void {
		self::remove_custom_roles();
	}

	/**
	 * Remove our custom roles.
	 *
	 * @since 1.0.0
	 *
	 * @return void
	 */
	public static function remove_custom_roles(): void {
		remove_role( 'collector' );
	}
}
