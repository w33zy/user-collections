<?php

namespace User_Collections\Inc\Core;

use User_Collections as NS;
use User_Collections\Inc\Admin;
use User_Collections\Inc\Frontend;
use User_Collections\Inc\Admin\Settings\Callbacks;
use User_Collections\Inc\Admin\Settings\Sanitization;
use User_Collections\Inc\Admin\Settings\Settings;
use User_Collections\Inc\Admin\Settings\Metaboxes;
use User_Collections\Inc\Rest\Authentication;
use User_Collections\Inc\Rest\Collections;

/**
 * The core plugin class.
 * Defines internationalization, admin-specific hooks, and public-facing site hooks.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @author     Your Name or Your Company
 */
class Init {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @var      Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_base_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_basename;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The name of the plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * The text domain of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $plugin_text_domain;

	/**
	 * Initialize and define the core functionality of the plugin.
	 */
	public function __construct() {

		$this->plugin_name        = NS\PLUGIN_NAME;
		$this->version            = NS\PLUGIN_VERSION;
		$this->plugin_basename    = NS\PLUGIN_BASENAME;
		$this->plugin_text_domain = NS\PLUGIN_TEXT_DOMAIN;

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();
		$this->define_task_hooks();
		$this->define_rest_hooks();
		$this->define_shortcode_hooks();
	}

	/**
	 * Loads the following required dependencies for this plugin.
	 *
	 * - Loader - Orchestrates the hooks of the plugin.
	 * - Internationalization_I18n - Defines internationalization functionality.
	 * - Admin - Defines all hooks for the admin area.
	 * - Frontend - Defines all hooks for the public side of the site.
	 *
	 * @access    private
	 */
	private function load_dependencies(): void {
		$this->loader = new Loader();
	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Internationalization_I18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @access    private
	 */
	private function set_locale(): void {

		$plugin_i18n = new Internationalization( $this->plugin_text_domain );

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );
	}

	/**
	 * Register all the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @access    private
	 */
	private function define_admin_hooks(): void {

		$plugin_admin = new Admin\Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_init', $plugin_admin, 'restrict_admin_access' );
		$this->loader->add_action( 'login_redirect', $plugin_admin, 'collector_redirect', 10, 3 );

		//$this->loader->add_action( 'query_vars', $plugin_admin, 'collection_query' );

		$this->loader->add_filter( 'manage_users_columns', $plugin_admin, 'new_modify_user_table' );
		$this->loader->add_filter( 'manage_users_custom_column', $plugin_admin, 'new_modify_user_table_row', 10, 3 );

		// Built the settings page.
		$callbacks    = new Callbacks( $this->plugin_name );
		$sanitization = new Sanitization( $this->plugin_name );
		$settings     = new Settings( $this->get_plugin_name(), $callbacks, $sanitization );
		$metaboxes    = new Metaboxes( $this->get_plugin_name() );

		// Add plugin settings page to WP menus.
		$this->loader->add_action( 'admin_menu', $plugin_admin, 'add_plugin_admin_menu' );

		// Plugin action links.
		$this->loader->add_filter( 'plugin_action_links_' . $this->plugin_basename, $plugin_admin, 'add_action_links' );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		$this->loader->add_action( 'admin_init' , $settings, 'register_settings' );
		$this->loader->add_action( 'load-toplevel_page_' . $this->get_plugin_name() , $metaboxes, 'add_meta_boxes' );
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @access    private
	 */
	private function define_public_hooks(): void {

		$plugin_public = new Frontend\Frontend( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
		$this->loader->add_action( 'wp_footer', $plugin_public, 'svelte_anchor' );
	}

	public function define_task_hooks(  ): void {

		$tasks = new Tasks( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'plugins_loaded', $tasks, 'register_tasks' );
		$this->loader->add_action( 'wp_async_user_collection_update', $tasks, 'process_update_post_meta', 10, 3 );
	}

	private function define_rest_hooks(): void {

		$this->loader->add_action( 'rest_api_init', new Collections, 'register_routes' );
		$this->loader->add_action( 'rest_api_init', new Authentication, 'register_routes' );
	}

	/**
	 * Register all of the hooks related to the shortcodes functionality
	 * of the plugin.
	 *
	 * @access    private
	 */
	private function define_shortcode_hooks(): void {

		$shortcodes = new Shortcodes( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_shortcode( 'user-collections', $shortcodes->collection_list, 'the_shortcode' );

		$this->loader->add_action( 'wp_ajax_nopriv_save_post_to_collection', $shortcodes->collections, 'save_post_to_collection' );
		$this->loader->add_action( 'wp_ajax_save_post_to_collection', $shortcodes->collections, 'save_post_to_collection' );

		$this->loader->add_action( 'wp_ajax_nopriv_get_collection', $shortcodes->collections, 'get_collection' );
		$this->loader->add_action( 'wp_ajax_get_collection', $shortcodes->collections, 'get_collection' );

		$this->loader->add_action( 'wp_ajax_nopriv_delete_collection', $shortcodes->collections, 'delete_collection' );
		$this->loader->add_action( 'wp_ajax_delete_collection', $shortcodes->collections, 'delete_collection' );

		$this->loader->add_action( 'wp_ajax_nopriv_create_collection', $shortcodes->collections, 'create_collection' );
		$this->loader->add_action( 'wp_ajax_create_collection', $shortcodes->collections, 'create_collection' );

		$this->loader->add_action( 'wp_ajax_nopriv_delete_post_from_collection', $shortcodes->collections, 'delete_post_from_collection' );
		$this->loader->add_action( 'wp_ajax_delete_post_from_collection', $shortcodes->collections, 'delete_post_from_collection' );

		$this->loader->add_action( 'wp_ajax_nopriv_signup_form', $shortcodes->collections, 'signup_form' );
		$this->loader->add_action( 'wp_ajax_nopriv_login_form', $shortcodes->collections, 'login_form' );
		$this->loader->add_action( 'wp_ajax_nopriv_forgot_form', $shortcodes->collections, 'forgot_form' );
		$this->loader->add_action( 'wp_ajax_nopriv_reset_form', $shortcodes->collections, 'reset_form' );

	}

	/**
	 * Run the loader to execute all the hooks with WordPress.
	 */
	public function run(): void {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 */
	public function get_plugin_name(): string {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @return    Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader(): Loader {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version(): string {
		return $this->version;
	}

	/**
	 * Retrieve the text domain of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The text domain of the plugin.
	 */
	public function get_plugin_text_domain(): string {
		return $this->plugin_text_domain;
	}

}
