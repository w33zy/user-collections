<?php

namespace User_Collections\Inc\Core;

use User_Collections\Inc\Admin\Collections;
use User_Collections\Inc\Frontend\Shortcodes\Collection_List;
use User_Collections\Inc\Frontend\Shortcodes\Login_Form;

/**
 *
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    User_Collections
 * @subpackage User_Collections/inc
 */

/**
 * The get_option functionality of the plugin.
 *
 *
 * @package    User_Collections
 * @subpackage User_Collections/inc
 * @author     Your Name <email@example.com>
 */


class Shortcodes {

	/**
	 * The ID of this plugin.
	 *
	 * @since 1.0.0
	 *
	 * @access private
	 * @var    string $plugin_name The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since  1.0.0
	 *
	 * @access private
	 * @var    string $version The current version of this plugin.
	 */
	private $version;

	/**
	 * An instance of the Shortcodes\CollectionList class
	 *
	 * @since  1.0.0
	 *
	 * @access public
	 * @var    Collection_List $collection_list The recipe shortcode class
	 */
	public $collection_list;

	/**
	 * An instance of the Admin\Collections class
	 *
	 * @since  1.0.0
	 *
	 * @access public
	 * @var    Collections $collections The recipe shortcode class
	 */
	public $collections;

	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 *
	 * @param string $plugin_name The name of the plugin.
	 * @param string $version The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version     = $version;

		$this->collection_list = new Collection_List( $this->plugin_name, $this->version );
		$this->collections     = new Collections( $this->plugin_name, $this->version );
	}
}
