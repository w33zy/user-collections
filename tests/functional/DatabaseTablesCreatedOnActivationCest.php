<?php

class DatabaseTablesCreatedOnActivationCest {

    protected $uc_collections_table = 'wp_uc_collections';
    protected $uc_items_table       = 'wp_uc_items';

    public function _before( AcceptanceTester $I ) {

      // Activate our plugin
      $I->loginAsAdmin();
      $I->amOnPluginsPage();
      $I->activatePlugin( 'user-collections' );
    }

    public function pluginTablesExists( AcceptanceTester $I ) {

      // Check that the `*_uc_collections` table exists
      $uc_collections_table = $I->grabPrefixedTableNameFor( 'uc_collections' );
      $I->seeTableInDatabase( $uc_collections_table );

      // Check that the `*_uc_items` table exists
      $uc_items_table = $I->grabPrefixedTableNameFor( 'uc_items' );
      $I->seeTableInDatabase( $uc_items_table );
  }
}
