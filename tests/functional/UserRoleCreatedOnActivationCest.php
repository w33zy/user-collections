<?php

class UserRoleCreatedOnActivationCest {

    public function _before( AcceptanceTester $I ) {

      // Activate our plugin
      $I->loginAsAdmin();
      $I->amOnPluginsPage();
      $I->activatePlugin( 'user-collections' );
    }

    public function collectorUserRoleExists( AcceptanceTester $I ) {

      // Check that the `collector` user role exists
      $user_roles = $I->grabOptionFromDatabase( 'wp_user_roles' );
      // assertArrayHasKey( 'collector', $user_roles );
  }
}
