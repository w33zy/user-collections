<?php

namespace wpunit\authentication;

use Codeception\TestCase\WPTestCase;
use User_Collections\Inc\Core\Activator;
use User_Collections\Inc\Core\Deactivator;
use User_Collections\Inc\Rest\Authentication;

/**
 * Handles testing of the user reset endpoint.
 *
 * @since 1.0.0
 *
 * @covers \User_Collections\Inc\Rest\Authentication
 */
class ResetUserTest extends WPTestCase {

	/**
	 * The user ID of the user to reset.
	 *
	 * @var int
	 */
	protected static $user_id;

	/**
	 * The request object.
	 *
	 * @var \WP_Rest_Request
	 */
	protected static $request;

	/**
	 * Handles user authentication via the REST API
	 *
	 * @var \User_Collections\Inc\Rest\Authentication
	 */
	protected static $authentication;

	/**
	 * The password reset link parsed into an array.
	 *
	 * @var array
	 */
	protected static $reset_key;

	/**
	 * The username of the user to reset.
	 *
	 * @var string
	 */
	protected static $user_login;

	/**
	 * The email address of the user to reset.
	 *
	 * @var string
	 */
	protected static $user_email;

	/**
	 * The password of the user to reset.
	 *
	 * @var string
	 */
	protected static $user_pass  = 'testPassword';

	/**
	 * Runs before the all tests are run.
	 *
	 * @param $factory
	 *
	 * @return void
	 */
	public static function wpSetUpBeforeClass( $factory ): void {
		Activator::setup_custom_roles();

		self::$user_login     = wp_generate_password( 6, false );
		self::$user_email     = self::$user_login . '@uc-tests.local';
		self::$authentication = new Authentication();
		self::$request        = new \WP_Rest_Request();
		self::$user_id        = $factory->user->create(
			[
				'user_login' => self::$user_login,
				'user_email' => self::$user_email,
				'user_pass'  => self::$user_pass,
				'role'       => 'collector'
			]
		);
		self::$reset_key     = self::get_password_reset_data( self::$user_login, self::$user_email );
	}

	/**
	 * Runs after the all tests are run.
	 *
	 * @return void
	 */
	public static function wpTearDownAfterClass(): void {
		Deactivator::remove_custom_roles();

		self::delete_user( self::$user_id );
		self::$user_id        = null;
		self::$request        = null;
		self::$authentication = null;
	}

	/**
	 * Send a request to get a password reset email
	 * via a user name.
	 *
	 * @since 1.0.0
	 *
	 * @covers \User_Collections\Inc\Rest\Authentication::reset_user()
	 *
	 * @uses \WP_REST_Response
	 */
	public function testGoodResetUserPasswordRequest(): void {
		self::$request->set_method( 'POST' );
		self::$request->set_route( '/uc/v1/authentication/reset' );
		self::$request->set_header( 'content-type', 'application/json' );
		self::$request->set_body_params(
			[
				'user_login' => self::$user_login,
				'new_pass_1' => 'testPassword1',
				'new_pass_2' => 'testPassword1',
				'key'        => self::$reset_key[ 'key' ],
			]
		);

		$response = self::$authentication->reset_user( self::$request );

		$this->assertEquals( self::$user_login, self::$reset_key[ 'login' ], 'User login does not match' );
		$this->assertInstanceOf( \WP_REST_Response::class, $response );
		$this->assertEquals( 200, $response->get_status() );
		$this->assertNotEmpty( $response->get_data()['data']['password_reset'] );
	}

	/**
	 * Send a request to get a password reset email
	 * via a user name.
	 *
	 * @since 1.0.0
	 *
	 * @covers \User_Collections\Inc\Rest\Authentication::reset_user()
	 *
	 * @uses \WP_REST_Response
	 */
	public function testOldUserPasswordEqualsNewUserPasswordRequest(): void {

		self::$request->set_method( 'POST' );
		self::$request->set_route( '/uc/v1/authentication/reset' );
		self::$request->set_header( 'content-type', 'application/json' );
		self::$request->set_body_params(
			[
				'user_login' => self::$user_login,
				'new_pass_1' => self::$user_pass,
				'new_pass_2' => self::$user_pass,
				'key'        => self::$reset_key[ 'key' ],
			]
		);

		$response = self::$authentication->reset_user( self::$request );

		$this->assertInstanceOf( \WP_REST_Response::class, $response );
		$this->assertEquals( 200, $response->get_status() );
		$this->assertNotEmpty( $response->get_data()['data']['password_reset'] );
	}

	/**
	 * Send a request to get a password reset email
	 * via a user name.
	 *
	 * @since 1.0.0
	 *
	 * @covers \User_Collections\Inc\Rest\Authentication::reset_user()
	 *
	 * @uses \WP_REST_Response
	 */
	public function testNotEqualUserPasswordRequest(): void {

		self::$request->set_method( 'POST' );
		self::$request->set_route( '/uc/v1/authentication/reset' );
		self::$request->set_header( 'content-type', 'application/json' );
		self::$request->set_body_params(
			[
				'user_login' => self::$user_login,
				'new_pass_1' => self::$user_pass,
				'new_pass_2' => 'testPassword1',
				'key'        => self::$reset_key[ 'key' ],
			]
		);

		$response = self::$authentication->reset_user( self::$request );

		$this->assertWPError( $response );
		$this->assertEquals( 'mismatch_error', $response->get_error_code() );
	}

	/**
	 * Send a request to get a password reset email
	 * via a user name.
	 *
	 * @since 1.0.0
	 *
	 * @covers \User_Collections\Inc\Rest\Authentication::reset_user()
	 *
	 * @uses \WP_REST_Response
	 */
	public function testMissingOrEmptyUserPasswordRequest(): void {

		self::$request->set_method( 'POST' );
		self::$request->set_route( '/uc/v1/authentication/reset' );
		self::$request->set_header( 'content-type', 'application/json' );
		self::$request->set_body_params(
			[
				'user_login' => self::$user_login,
				'new_pass_2' => 'testPassword1',
				'key'        => self::$reset_key[ 'key' ],
			]
		);

		$response = self::$authentication->reset_user( self::$request );

		$this->assertWPError( $response );
		$this->assertEquals( 'reset_error', $response->get_error_code() );
	}

	/**
	 * Send a request to get a password reset email
	 * via a user name.
	 *
	 * @since 1.0.0
	 *
	 * @covers \User_Collections\Inc\Rest\Authentication::reset_user()
	 *
	 * @uses \WP_REST_Response
	 */
	public function testUnequalPasswordResetKeyRequest(): void {

		$key = wp_generate_password( 12, false );

		self::$request->set_method( 'POST' );
		self::$request->set_route( '/uc/v1/authentication/reset' );
		self::$request->set_header( 'content-type', 'application/json' );
		self::$request->set_body_params(
			[
				'user_login' => self::$user_login,
				'new_pass_1' => self::$user_pass,
				'new_pass_2' => self::$user_pass,
				'key'        => $key,
			]
		);

		$response = self::$authentication->reset_user( self::$request );

		$this->assertNotEquals( self::$reset_key[ 'key' ], $key );
		$this->assertWPError( $response );
		$this->assertEquals( 'invalid_key', $response->get_error_code() );
	}

	/**
	 * Send a request to get a password reset email
	 * via a user name.
	 *
	 * @since 1.0.0
	 *
	 * @covers \User_Collections\Inc\Rest\Authentication::reset_user()
	 *
	 * @uses \WP_REST_Response
	 */
	public function testEmptyOrMissingResetKeyRequest(): void {

		self::$request->set_method( 'POST' );
		self::$request->set_route( '/uc/v1/authentication/reset' );
		self::$request->set_header( 'content-type', 'application/json' );
		self::$request->set_body_params(
			[
				'user_login' => self::$user_login,
				'new_pass_1' => self::$user_pass,
				'new_pass_2' => self::$user_pass,
			]
		);

		$response = self::$authentication->reset_user( self::$request );

		$this->assertWPError( $response );
		$this->assertEquals( 'invalid_key', $response->get_error_code() );
	}

	/**
	 * Grabs the values of our password reset URL.
	 *
	 * @since 1.0.0
	 *
	 * @param  string  $user_login
	 * @param  string  $user_email
	 * @param  string|null  $value
	 *
	 * @return string|array
	 */
	private static function get_password_reset_data( string $user_login, string $user_email, string $value = null ) {
		$qry = [];

		self::$request->set_method( 'POST' );
		self::$request->set_route( '/uc/v1/authentication/forgot' );
		self::$request->set_header( 'content-type', 'application/json' );
		self::$request->set_body_params(
			[
				'user_login' => $user_login ?: $user_email,
			]
		);

		$response = self::$authentication->forgot_user( self::$request );

		if ( is_wp_error( $response ) ) {
			return $qry;
		}

		$url = parse_url( $response->get_data()['data']['reset_url'] );

		if ( ! empty( $url[ 'query' ] ) ) {
			parse_str( $url[ 'query' ], $qry );
		}

		return ( $value && ! empty( $qry[ $value ] ) ) ? $qry[ $value ] : $qry;
	}

}