<?php

namespace wpunit\authentication;

use Codeception\TestCase\WPTestCase;
use User_Collections\Inc\Core\Activator;
use User_Collections\Inc\Core\Deactivator;
use User_Collections\Inc\Rest\Authentication;

/**
 * Handles testing of the forgot user password endpoint.
 *
 * @since 1.0.0
 *
 * @covers \User_Collections\Inc\Rest\Authentication
 */
class ForgotUserTest extends WPTestCase {

	protected static $user_id;
	protected static $request;
	protected static $authentication;
	protected static $user_login;
	protected static $user_email;
	protected static $user_pass  = 'testPassword';

	public static function wpSetUpBeforeClass( $factory ): void {
		Activator::setup_custom_roles();

		self::$user_login     = wp_generate_password( 6, false );
		self::$user_email     = self::$user_login . '@uc-tests.local';
		self::$authentication = new Authentication();
		self::$request        = new \WP_Rest_Request();
		self::$user_id        = $factory->user->create(
			[
				'user_login' => self::$user_login,
				'user_email' => self::$user_email,
				'user_pass'  => self::$user_pass,
				'role'       => 'collector'
			]
		);
	}

	public static function wpTearDownAfterClass(): void {
		Deactivator::remove_custom_roles();

		self::delete_user( self::$user_id );
		self::$user_id        = null;
		self::$request        = null;
		self::$authentication = null;
	}

	/**
	 * Send a request to get a password reset email
	 * via a user name.
	 *
	 * @since 1.0.0
	 *
	 * @covers \User_Collections\Inc\Rest\Authentication::forgot_user()
	 *
	 * @uses \WP_REST_Response
	 */
	public function testGoodForgotPasswordWithUserLoginRequest(): void {

		self::$request->set_method( 'POST' );
		self::$request->set_route( '/uc/v1/authentication/forgot' );
		self::$request->set_header( 'content-type', 'application/json' );
		self::$request->set_body_params(
			[
				'user_login'    => self::$user_login,
			]
		);

		$response = self::$authentication->forgot_user( self::$request );

		$this->assertInstanceOf( \WP_REST_Response::class, $response );
		$this->assertEquals( 200, $response->get_status() );
		$this->assertNotEmpty( $response->get_data()['data']['email_sent'] );
		$this->assertNotEmpty( $response->get_data()['data']['reset_url'] );
	}

	/**
	 * Send a request to get a password reset email
	 * via a user name.
	 *
	 * @since 1.0.0
	 *
	 * @covers \User_Collections\Inc\Rest\Authentication::forgot_user()
	 *
	 * @uses \WP_REST_Response
	 */
	public function testGoodForgotPasswordWithUserEmailRequest(): void {

		self::$request->set_method( 'POST' );
		self::$request->set_route( '/uc/v1/authentication/forgot' );
		self::$request->set_header( 'content-type', 'application/json' );
		self::$request->set_body_params(
			[
				'user_login'    => self::$user_email,
			]
		);

		$response = self::$authentication->forgot_user( self::$request );

		$this->assertInstanceOf( \WP_REST_Response::class, $response );
		$this->assertEquals( 200, $response->get_status() );
		$this->assertNotEmpty( $response->get_data()['data']['email_sent'] );
		$this->assertNotEmpty( $response->get_data()['data']['reset_url'] );
	}

	/**
	 * Send a good request to log in a new user.
	 *
	 * @since 1.0.0
	 *
	 * @covers \User_Collections\Inc\Rest\Authentication::forgot_user()
	 *
	 * @uses \WP_REST_Response
	 */
//	public function testForgotPasswordEmailSent(): void {
//
//		self::$request->set_method( 'POST' );
//		self::$request->set_route( '/uc/v1/authentication/login' );
//		self::$request->set_header( 'content-type', 'application/json' );
//		self::$request->set_body_params(
//			[
//				'user_login'    => self::$user_login,
//				'user_password' => self::$user_pass
//			]
//		);
//
//		self::$authentication->login_user( self::$request );
//
//		$this->assertEquals( self::$user_id, get_current_user_id() );
//		$this->assertTrue( user_can( get_current_user_id(), 'create_collection' ) );
//		$this->assertEquals( get_current_user_id(), self::$user_id );
//	}

	/**
	 * Send a request with a missing or blank user information
	 * to get a password reset email.
	 *
	 * @since 1.0.0
	 *
	 * @covers \User_Collections\Inc\Rest\Authentication::forgot_user()
	 *
	 * @uses \WP_REST_Response
	 */
	public function testMissingUserLoginRequest(): void {

		self::$request->set_method( 'POST' );
		self::$request->set_route( '/uc/v1/authentication/forgot' );
		self::$request->set_header( 'content-type', 'application/json' );
		self::$request->set_body_params( [] );

		$response = self::$authentication->forgot_user( self::$request );

		$this->assertWPError( $response );
		$this->assertEquals( 'login_error', $response->get_error_code() );
	}

	/**
	 * Send a request with an unknown user email
	 * to get a password reset email.
	 *
	 * @since 1.0.0
	 *
	 * @covers \User_Collections\Inc\Rest\Authentication::forgot_user()
	 *
	 * @uses \WP_REST_Response
	 */
	public function testUnknownUserEmailRequest(): void {

		self::$request->set_method( 'POST' );
		self::$request->set_route( '/uc/v1/authentication/forgot' );
		self::$request->set_header( 'content-type', 'application/json' );
		self::$request->set_body_params(
			[
				'user_login' => 'testUser02@uc-tests.local',
			]
		);

		$response = self::$authentication->forgot_user( self::$request );

		$this->assertWPError( $response );
		$this->assertEquals( 'unknown_user', $response->get_error_code() );
	}

	/**
	 * Send a request with an unknown user email
	 * to get a password reset email.
	 *
	 * @since 1.0.0
	 *
	 * @covers \User_Collections\Inc\Rest\Authentication::forgot_user()
	 *
	 * @uses \WP_REST_Response
	 */
	public function testUnknownUserLoginRequest(): void {

		self::$request->set_method( 'POST' );
		self::$request->set_route( '/uc/v1/authentication/forgot' );
		self::$request->set_header( 'content-type', 'application/json' );
		self::$request->set_body_params(
			[
				'user_login' => 'testUser02',
			]
		);

		$response = self::$authentication->forgot_user( self::$request );

		$this->assertWPError( $response );
		$this->assertEquals( 'unknown_user', $response->get_error_code() );
	}

}