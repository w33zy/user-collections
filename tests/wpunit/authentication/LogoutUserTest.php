<?php

namespace wpunit\authentication;

use Codeception\TestCase\WPTestCase;
use User_Collections\Inc\Core\Activator;
use User_Collections\Inc\Core\Deactivator;
use User_Collections\Inc\Rest\Authentication;

/**
 * Handles testing of the logout user endpoint.
 *
 * @since 1.0.0
 *
 * @covers \User_Collections\Inc\Rest\Authentication
 */
class LogoutUserTest extends WPTestCase {

	/**
	 * The user ID of the user to logout.
	 *
	 * @var int
	 */
	protected static $user_id;

	/**
	 * The request object.
	 *
	 * @var \WP_REST_Request
	 */
	protected static $request;

	/**
	 * Handles user authentication via the REST API
	 *
	 * @var \User_Collections\Inc\Rest\Authentication
	 */
	protected static $authentication;

	/**
	 * The username of the user to logout.
	 *
	 * @var string
	 */
	protected static $user_login;

	/**
	 * The email address of the user to logout.
	 *
	 * @var string
	 */
	protected static $user_email;

	/**
	 * The password of the user to logout.
	 *
	 * @var string
	 */
	protected static $user_pass  = 'testPassword';

	/**
	 * The response logging in out user.
	 *
	 * @var \WP_REST_Response
	 */
	protected static $login_response;

	/**
	 * Runs before the all tests are run.
	 *
	 * @param $factory
	 *
	 * @return void
	 */
	public static function wpSetUpBeforeClass( $factory ): void {
		Activator::setup_custom_roles();

		self::$user_login     = wp_generate_password( 6, false );
		self::$user_email     = self::$user_login . '@uc-tests.local';
		self::$authentication = new Authentication();
		self::$request        = new \WP_Rest_Request();
		self::$user_id        = $factory->user->create(
			[
				'user_login' => self::$user_login,
				'user_email' => self::$user_email,
				'user_pass'  => self::$user_pass,
				'role'       => 'collector'
			]
		);

		self::$request->set_method( 'POST' );
		self::$request->set_route( '/uc/v1/authentication/login' );
		self::$request->set_header( 'content-type', 'application/json' );
		self::$request->set_body_params(
			[
				'user_login'    => self::$user_login,
				'user_password' => self::$user_pass
			]
		);

		self::$login_response = self::$authentication->login_user( self::$request );
	}

	/**
	 * Runs after the all tests are run.
	 *
	 * @return void
	 */
	public static function wpTearDownAfterClass(): void {
		Deactivator::remove_custom_roles();

		self::delete_user( self::$user_id );
		self::$user_id        = null;
		self::$request        = null;
		self::$authentication = null;
		self::$login_response = null;
	}

	/**
	 * Send a request to logout the current user.
	 *
	 * @since 1.0.0
	 *
	 * @covers \User_Collections\Inc\Rest\Authentication::logout_user()
	 *
	 * @uses \WP_REST_Response
	 */
	public function testGoodUserLogoutRequest(): void {
		wp_set_current_user( self::$user_id );

		// Ensure wwe are logged in
		$this->assertInstanceOf( \WP_REST_Response::class, self::$login_response );
		$this->assertEquals( 200, self::$login_response->get_status() );
		$this->assertEquals( self::$user_id, wp_get_current_user()->ID );
		$this->assertEquals( self::$user_id, self::$login_response->get_data()['data']['user_id'] );
		$this->assertEquals( wp_get_current_user()->ID, self::$login_response->get_data()['data']['user_id'] );

		// Send a request to logout the user
		self::$request->set_method( 'DELETE' );
		self::$request->set_route( '/uc/v1/authentication/logout' );
		self::$request->set_header( 'content-type', 'application/json' );
		self::$request->set_body_params( []	);

		$response = self::$authentication->logout_user( self::$request );

		// Ensure we are logged out
		$this->assertInstanceOf( \WP_REST_Response::class, $response );
		$this->assertEquals( 200, $response->get_status() );
		$this->assertEquals( 0, wp_get_current_user()->ID );
	}


}