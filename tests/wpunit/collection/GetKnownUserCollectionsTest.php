<?php

namespace wpunit\collection;

use Codeception\TestCase\WPTestCase;
use User_Collections\Inc\Core\Activator;
use User_Collections\Inc\Core\Deactivator;
use User_Collections\Inc\Rest\Collections;

class GetKnownUserCollectionsTest extends WPTestCase {

	protected static $user_id;
	protected static $request;
	protected static $collections;

	public static function wpSetUpBeforeClass( $factory ): void {
		Activator::setup_custom_roles();

		self::$request     = new \WP_Rest_Request();
		self::$collections = new Collections();
		self::$user_id     = $factory->user->create( [ 'role' => 'collector' ] );
	}

	public static function wpTearDownAfterClass(): void {
		Deactivator::remove_custom_roles();

		self::delete_user( self::$user_id );
		self::$user_id = null;
		self::$request = null;

		wp_logout();
	}

	/**
	 * Send a good request to log in a new user.
	 *
	 * @since 1.0.0
	 *
	 * @group collectionsKnownUser
	 *
	 * @covers \User_Collections\Inc\Rest\Collections::get_collections()
	 *
	 * @uses \WP_REST_Response
	 */
	public function testGetCollectionsOfKnownUser(): void {
		wp_set_current_user( self::$user_id );

		self::$request->set_method( 'GET' );
		self::$request->set_route( '/uc/v1/collections' );

		$response = self::$collections->get_collections( self::$request );

		$this->assertInstanceOf( \WP_REST_Response::class, $response );
		$this->assertEquals( 200, $response->get_status() );
		$this->assertArrayHasKey( 'titles', $response->get_data()['data'] );
		$this->assertContains( 'Favorites', $response->get_data()['data']['titles'] );
		$this->assertArrayHasKey( 'counts', $response->get_data()['data'] );
		$this->assertEquals( 0, $response->get_data()['data']['counts'][ array_search( 'Favorites', $response->get_data()['data']['titles'], true ) ] );
	}

	/**
	 * Send a good request to log in a new user.
	 *
	 * @since 1.0.0
	 *
	 * @group collectionsKnownUser
	 *
	 * @covers \User_Collections\Inc\Rest\Collections::get_collections_permissions_check()
	 */
	public function testKnownUserGetCollectionsPermissionsCheck(): void {
		wp_set_current_user( self::$user_id );

		$this->assertTrue( wp_get_current_user()->has_cap( 'collector' ) );
		$this->assertTrue( current_user_can( 'create_collection' ) );
	}

}