<?php

namespace wpunit\collection;

use Codeception\TestCase\WPTestCase;
use User_Collections\Inc\Core\Activator;
use User_Collections\Inc\Core\Deactivator;
use User_Collections\Inc\Rest\Collections;

class DeleteCollectionTest extends WPTestCase {

	/**
	 * The user ID of the user to logout.
	 *
	 * @var int
	 */
	protected static $user_id;

	/**
	 * The request object.
	 *
	 * @var \WP_REST_Request
	 */
	protected static $request;

	/**
	 * Handles CRUD action of collections via the REST API
	 *
	 * @var \User_Collections\Inc\Rest\Collections
	 */
	protected static $collections;

	public static function wpSetUpBeforeClass( $factory ): void {
		Activator::setup_custom_roles();

		self::$request     = new \WP_Rest_Request();
		self::$collections = new Collections();
		self::$user_id     = $factory->user->create( [ 'role' => 'collector' ] );
	}

	public static function wpTearDownAfterClass(): void {
		Deactivator::remove_custom_roles();

		self::delete_user( self::$user_id );
		self::$user_id = null;
		self::$request = null;

		wp_logout();
	}

	/**
	 * Send a good request to delete a collection.
	 *
	 * @since 1.0.0
	 *
	 * @group createCollection
	 *
	 * @covers \User_Collections\Inc\Rest\Collections::delete_collection()
	 *
	 * @uses \WP_REST_Response
	 */
	public function testGoodDeleteCollectionRequest(): void {
		$title      = wp_generate_uuid4();
		$collection = self::create_collection_for_deletion( self::$collections, self::$user_id, $title );

		// Create a delete request.
		wp_set_current_user( self::$user_id );

		self::$request = new \WP_Rest_Request();
		self::$request->set_method( 'DELETE' );
		self::$request->set_route( '/uc/v1/collections' );
		self::$request->set_header( 'content-type', 'application/json' );
		self::$request->set_body_params(
			[
				'collection_key'  => $collection->collection_key,
				'collection_user' => self::$user_id,
			]
		);

		$response = self::$collections->delete_collection( self::$request );

		$this->assertInstanceOf( \WP_REST_Response::class, $response );
		$this->assertArrayHasKey( 'collection_key', $response->get_data()['data'] );
		$this->assertArrayHasKey( 'collections_deleted', $response->get_data()['data'] );
		$this->assertEquals( $collection->collection_key, $response->get_data()['data']['collection_key'] );
		$this->assertEquals( 1, $response->get_data()['data']['collections_deleted'] );
	}

	/**
	 * Test the values stored in the database are removed
	 *
	 * @since 1.0.0
	 *
	 * @group createCollection
	 *
	 * @covers \User_Collections\Inc\Rest\Collections::delete_collection()
	 *
	 * @uses \WP_REST_Response
	 */
//	public function testCollectionValuesRemovedFromDatabase(): void {
//		global $wpdb;
//		wp_set_current_user( self::$user_id );
//
//		$collection_title = wp_generate_uuid4();
//
//		self::$request->set_method( 'POST' );
//		self::$request->set_route( '/uc/v1/collections' );
//		self::$request->set_header( 'content-type', 'application/json' );
//		self::$request->set_body_params(
//			[
//				'collection_title' => $collection_title,
//			]
//		);
//
//		self::$collections->create_collection( self::$request );
//
//		$collection = $wpdb->get_row(
//			$wpdb->prepare(
//				"SELECT * FROM `{$wpdb->prefix}uc_collections` WHERE `collection_title` = %s", $collection_title
//			)
//		);
//
//		$this->assertEquals( $collection_title, $collection->collection_title );
//		$this->assertEquals( self::$user_id, $collection->collection_user );
//		$this->assertTrue( str_contains( $collection->collection_key, 'uc-' ) );
//		$this->assertEquals( 0, $collection->collection_count );
//		$this->assertNotEmpty( $collection->collection_created );
//		$this->assertNotEmpty( $collection->collection_updated );
//	}

	/**
	 * Send a good request to create a new collection.
	 *
	 * @since 1.0.0
	 *
	 * @group createCollection
	 *
	 * @covers \User_Collections\Inc\Rest\Collections::create_collection()
	 *
	 * @uses \WP_REST_Response
	 */
//	public function testMissingOrEmptyCollectionTitleRequest(): void {
//		wp_set_current_user( self::$user_id );
//
//		self::$request->set_method( 'POST' );
//		self::$request->set_route( '/uc/v1/collections' );
//		self::$request->set_header( 'content-type', 'application/json' );
//		self::$request->set_body_params(
//			[
//				'collection_title' => '',
//			]
//		);
//
//		$response = self::$collections->create_collection( self::$request );
//
//		$this->assertWPError( $response );
//		$this->assertEquals( 'collection_title_required', $response->get_error_code() );
//	}

	/**
	 * Create a test collection
	 *
	 * @since 1.0.0
	 *
	 * @param  \User_Collections\Inc\Rest\Collections  $collections
	 * @param  int                                     $user_id
	 * @param  string                                  $title
	 *
	 * @return \stdClass
	 */
	private static function create_collection_for_deletion( Collections $collections, int $user_id, string $title ): \stdClass {
		global $wpdb;
		wp_set_current_user( $user_id );

		$request = new \WP_Rest_Request();
		$request->set_method( 'POST' );
		$request->set_route( '/uc/v1/collections' );
		$request->set_header( 'content-type', 'application/json' );
		$request->set_body_params(
			[
				'collection_title' => $title,
			]
		);

		$collections->create_collection( $request );

		return $wpdb->get_row(
			$wpdb->prepare(
				"SELECT * FROM `{$wpdb->prefix}uc_collections` WHERE `collection_title` = %s", $title
			)
		);
	}
}