<?php

namespace wpunit\collection;

use Codeception\TestCase\WPTestCase;
use User_Collections\Inc\Core\Activator;
use User_Collections\Inc\Core\Deactivator;
use User_Collections\Inc\Rest\Collections;

class CreateCollectionTest extends WPTestCase {

	/**
	 * The user ID of the user to logout.
	 *
	 * @var int
	 */
	protected static $user_id;

	/**
	 * The request object.
	 *
	 * @var \WP_REST_Request
	 */
	protected static $request;

	/**
	 * Handles CRUD action of collections via the REST API
	 *
	 * @var \User_Collections\Inc\Rest\Collections
	 */
	protected static $collections;

	public static function wpSetUpBeforeClass( $factory ): void {
		Activator::setup_custom_roles();

		self::$request     = new \WP_Rest_Request();
		self::$collections = new Collections();
		self::$user_id     = $factory->user->create( [ 'role' => 'collector' ] );
	}

	public static function wpTearDownAfterClass(): void {
		Deactivator::remove_custom_roles();

		self::delete_user( self::$user_id );
		self::$user_id = null;
		self::$request = null;

		wp_logout();
	}

	/**
	 * Send a good request to create a new collection.
	 *
	 * @since 1.0.0
	 *
	 * @group createCollection
	 *
	 * @covers \User_Collections\Inc\Rest\Collections::create_collection()
	 *
	 * @uses \WP_REST_Response
	 */
	public function testGoodCreateCollectionRequest(): void {
		wp_set_current_user( self::$user_id );

		self::$request->set_method( 'POST' );
		self::$request->set_route( '/uc/v1/collections' );
		self::$request->set_header( 'content-type', 'application/json' );
		self::$request->set_body_params(
			[
				'collection_title' => 'Test Collection',
			]
		);

		$response = self::$collections->create_collection( self::$request );

		$this->assertInstanceOf( \WP_REST_Response::class, $response );
		$this->assertEquals( 201, $response->get_status() );
		$this->assertArrayHasKey( 'collection_title', $response->get_data()['data'] );
		$this->assertEquals( 'Test Collection', $response->get_data()['data']['collection_title'] );
		$this->assertArrayHasKey( 'collection_key', $response->get_data()['data'] );
	}

	/**
	 * Test the values stored in the database.
	 *
	 * @since 1.0.0
	 *
	 * @group createCollection
	 *
	 * @covers \User_Collections\Inc\Rest\Collections::create_collection()
	 *
	 * @uses \WP_REST_Response
	 */
	public function testCollectionValuesInsertedInDatabase(): void {
		global $wpdb;
		wp_set_current_user( self::$user_id );

		$collection_title = wp_generate_uuid4();

		self::$request->set_method( 'POST' );
		self::$request->set_route( '/uc/v1/collections' );
		self::$request->set_header( 'content-type', 'application/json' );
		self::$request->set_body_params(
			[
				'collection_title' => $collection_title,
			]
		);

		self::$collections->create_collection( self::$request );

		$collection = $wpdb->get_row(
			$wpdb->prepare(
				"SELECT * FROM `{$wpdb->prefix}uc_collections` WHERE `collection_title` = %s", $collection_title
			)
		);

		$this->assertEquals( $collection_title, $collection->collection_title );
		$this->assertEquals( self::$user_id, $collection->collection_user );
		$this->assertTrue( str_contains( $collection->collection_key, 'uc-' ) );
		$this->assertEquals( 0, $collection->collection_count );
		$this->assertNotEmpty( $collection->collection_created );
		$this->assertNotEmpty( $collection->collection_updated );
	}

	/**
	 * Send a good request to create a new collection.
	 *
	 * @since 1.0.0
	 *
	 * @group createCollection
	 *
	 * @covers \User_Collections\Inc\Rest\Collections::create_collection()
	 *
	 * @uses \WP_REST_Response
	 */
	public function testMissingOrEmptyCollectionTitleRequest(): void {
		wp_set_current_user( self::$user_id );

		self::$request->set_method( 'POST' );
		self::$request->set_route( '/uc/v1/collections' );
		self::$request->set_header( 'content-type', 'application/json' );
		self::$request->set_body_params(
			[
				'collection_title' => '',
			]
		);

		$response = self::$collections->create_collection( self::$request );

		$this->assertWPError( $response );
		$this->assertEquals( 'collection_title_required', $response->get_error_code() );
	}



}