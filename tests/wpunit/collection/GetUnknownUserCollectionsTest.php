<?php

namespace wpunit\collection;

use Codeception\TestCase\WPTestCase;
use User_Collections\Inc\Rest\Collections;

class GetUnknownUserCollectionsTest extends WPTestCase {

	protected static $request;
	protected static $collections;

	public static function wpSetUpBeforeClass(): void {
		self::$request     = new \WP_Rest_Request();
		self::$collections = new Collections();
	}

	public static function wpTearDownAfterClass(): void {
		self::$request     = null;
		self::$collections = null;
	}

	/**
	 * Send a good request to log in a new user.
	 *
	 * @since 1.0.0
	 *
	 * @covers \User_Collections\Inc\Rest\Collections::get_collections()
	 *
	 * @uses \WP_REST_Response
	 */
	public function testGetCollectionsOfUnknownUser(): void {

		self::$request->set_method( 'GET' );
		self::$request->set_route( '/uc/v1/collections' );

		$response = self::$collections->get_collections( self::$request );

		$this->assertInstanceOf( \WP_REST_Response::class, $response );
		$this->assertEquals( 200, $response->get_status() );
		$this->assertArrayHasKey( 'titles', $response->get_data()['data'] );
		$this->assertArrayHasKey( 'uc-favorites', $response->get_data()['data']['titles'] );
		$this->assertContains( 'Favorites', $response->get_data()['data']['titles'] );
		$this->assertArrayHasKey( 'uc-favorites', $response->get_data()['data'] );
		$this->assertArrayHasKey( 'counts', $response->get_data()['data'] );
		$this->assertEquals( 0, $response->get_data()['data']['counts'][ array_search( 'Favorites', $response->get_data()['data']['titles'], true ) ] );
	}

	/**
	 * Send a good request to log in a new user.
	 *
	 * @since 1.0.0
	 *
	 * @covers \User_Collections\Inc\Rest\Collections::get_collections_permissions_check()
	 */
	public function testUnknownUserGetCollectionsPermissionsCheck(): void {

		$this->assertFalse( wp_get_current_user()->has_cap( 'collector' ) );
	}

}