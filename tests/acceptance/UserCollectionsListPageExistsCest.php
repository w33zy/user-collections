<?php

namespace acceptance;

use AcceptanceTester;

class UserCollectionsListPageExistsCest {

	protected static $page_id;

	public function _before( AcceptanceTester $I ): void {
		// Activate our plugin
		$I->loginAsAdmin();
		$I->amOnPluginsPage();
		$I->activatePlugin( 'user-collections' );
		$I->seePluginActivated( 'user-collections' );

		// Add a page that contains the shortcode that will user collection list
		// or sign-in/signup form
		self::$page_id = $I->havePageInDatabase(
			[
				'post_title'   => 'Favorites',
				'post_name'    => 'favorites',
				'post_content' => '[user-collections]',
			]
		);

	}

	public function emptyCollectionListIsOnPage( AcceptanceTester $I ): void {

		$I->amOnPage( '/favorites' );
		$I->wait( 1 );
		$I->see( 'Favorites', 'h1' );
		$I->see( 'Favorites', 'h3' );
		$I->see( 'No posts saved to this collection', 'p' );
	}
}
